# 目录
## 一、项目简介
## 二、前端技术栈
## 三、后端技术栈
## 四、数据库技术栈
## 五、架构设计
## 六、核心系统
## 七、经典代码
## 八、经典BUG场景
## 九、总结
## 十、资源贡献     

-----
-----
-----
-----
-----
## 一、项目简介
## 二、前端技术栈
## 三、后端技术栈
## 四、数据库技术栈
## 五、架构设计
## 六、核心系统
## 七、经典代码
## 八、经典BUG场景
## 九、总结
## 十、资源贡献 
## 背景
这个项目，是我职业生涯中的大前端技术体系，主要囊括了大前端技术栈的开发总结、踩坑反思、技术升华与新技术实践。
## 涉及技术栈
1、开发基础版: HTML5、CSS3、JavaScript、JSON、AJAX；  
2、Vue全家桶系列: Vue.js、Vue-router、Vue-cli、Axios、Vuex；  
3、数据库: MySQL、MongoDB、Mongoose、Redis；  
4、Node.js系列: Node.js、Koa、、、、  
5、AI人工智能: TensorFlow、数据结构、各种算法、统计学知识  
6、运维系列: KeepAlived、Heartbeat  


# 项目文件目录
## /web
这是前端项目文件目录，由Vue系列搭建的前端系统，是一个完整的Vue单页面应用，主要功能包括IM系统、
## /api
这是为前端提供API接口访问的文件目录。
## /data
文件目录
## /example
文件目录
## /node_modules
文件目录
## /static
文件目录
## /frame
文件目录
## /item
文件目录
## /ysun
文件目录
## /main.js
Node.js系统启动文件配置
## /app.js
测试配置启动文件
## /plugin.js




[返回目录](#目录)