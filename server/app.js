//异常处理
process.on('uncaughtException', (err)=> {
  console.log(`
***************************************************************************************
********************${new Date()} ${(new Date()).getMilliseconds()}********************

  uncaughtException: ${err}
*****
*

  `);
}); 


const Koa = require('koa');
const Router = require('koa-router');

global.app = new Koa();
global.router = new Router();

//跨域配置 
const convert = require('koa-convert');
const cors = require('koa-cors');
app.use(cors());   

//通信配置
const koaBody = require('koa-body');
app.use(koaBody());


//配置静态资源目录  
const static = require('koa-static');
app.use(static('./'));

//路由配置
app.use(router.routes());



(async ()=> {
  //全局变量
  try {
    global.plug = require("./plugin.js"); 
    plug.run();
  } catch(e) { 
    console.log(e);   
  } 


  //全局变量
  try { 
    const apply = require("./application/init.js"); 
    apply.run();
  } catch(e) { 
    console.log(e);   
  } 
})(); 
 



/*--------------------------------------------------*/
app.listen(8031); 
 
console.log(`
············································································· 
············································································· 
···············${new Date()} ${(new Date()).getMilliseconds()}···············
PORT: 7901:80
Server is Running...
····· 
· 

`); 