"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @license
 * Copyright 2019 Google Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
var tf = require("../index");
var jasmine_util_1 = require("../jasmine_util");
var backend_1 = require("./backend");
jasmine_util_1.describeWithFlags('epsilon', jasmine_util_1.ALL_ENVS, function () {
    it('Epsilon is a function of float precision', function () {
        var epsilonValue = tf.backend().floatPrecision() === 32 ?
            backend_1.EPSILON_FLOAT32 :
            backend_1.EPSILON_FLOAT16;
        expect(tf.backend().epsilon()).toBe(epsilonValue);
    });
    it('abs(epsilon) > 0', function () {
        expect(tf.abs(tf.backend().epsilon()).arraySync()).toBeGreaterThan(0);
    });
});
//# sourceMappingURL=backend_test.js.map