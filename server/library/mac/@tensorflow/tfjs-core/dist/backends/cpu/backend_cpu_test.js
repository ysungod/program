"use strict";
/**
 * @license
 * Copyright 2019 Google LLC. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
Object.defineProperty(exports, "__esModule", { value: true });
var tf = require("../../index");
var jasmine_util_1 = require("../../jasmine_util");
var ops_1 = require("../../ops/ops");
var test_util_1 = require("../../test_util");
var backend_cpu_test_registry_1 = require("./backend_cpu_test_registry");
jasmine_util_1.describeWithFlags('backendCPU', backend_cpu_test_registry_1.CPU_ENVS, function () {
    var backend;
    beforeEach(function () {
        backend = tf.backend();
    });
    it('register empty string tensor', function () {
        var t = tf.Tensor.make([3], {}, 'string');
        expect(backend.readSync(t.dataId) == null).toBe(true);
    });
    it('register empty string tensor and write', function () {
        var t = tf.Tensor.make([3], {}, 'string');
        backend.write(t.dataId, ['c', 'a', 'b']);
        test_util_1.expectArraysEqual(backend.readSync(t.dataId), ['c', 'a', 'b']);
    });
    it('register string tensor with values', function () {
        var t = tf.Tensor.make([3], { values: ['a', 'b', 'c'] }, 'string');
        test_util_1.expectArraysEqual(backend.readSync(t.dataId), ['a', 'b', 'c']);
    });
    it('register string tensor with values and overwrite', function () {
        var t = tf.Tensor.make([3], { values: ['a', 'b', 'c'] }, 'string');
        backend.write(t.dataId, ['c', 'a', 'b']);
        test_util_1.expectArraysEqual(backend.readSync(t.dataId), ['c', 'a', 'b']);
    });
    it('register string tensor with values and mismatched shape', function () {
        expect(function () { return tf.tensor(['a', 'b', 'c'], [4], 'string'); }).toThrowError();
    });
});
jasmine_util_1.describeWithFlags('depthToSpace', backend_cpu_test_registry_1.CPU_ENVS, function () {
    it('throws when CPU backend used with data format NCHW', function () {
        var t = tf.tensor4d([1, 2, 3, 4], [1, 4, 1, 1]);
        var blockSize = 2;
        var dataFormat = 'NCHW';
        expect(function () { return tf.depthToSpace(t, blockSize, dataFormat); })
            .toThrowError("Only NHWC dataFormat supported on CPU for depthToSpace. Got " + dataFormat);
    });
});
jasmine_util_1.describeWithFlags('gatherND CPU', backend_cpu_test_registry_1.CPU_ENVS, function () {
    it('should throw error when index out of range', function () {
        var indices = ops_1.tensor2d([0, 2, 99], [3, 1], 'int32');
        var input = ops_1.tensor2d([100, 101, 102, 777, 778, 779, 10000, 10001, 10002], [3, 3], 'float32');
        expect(function () { return tf.gatherND(input, indices); }).toThrow();
    });
});
jasmine_util_1.describeWithFlags('scatterND CPU', backend_cpu_test_registry_1.CPU_ENVS, function () {
    it('should throw error when index out of range', function () {
        var indices = tf.tensor2d([0, 4, 99], [3, 1], 'int32');
        var updates = tf.tensor2d([100, 101, 102, 777, 778, 779, 10000, 10001, 10002], [3, 3], 'float32');
        var shape = [5, 3];
        expect(function () { return tf.scatterND(indices, updates, shape); }).toThrow();
    });
    it('should throw error when indices has wrong dimension', function () {
        var indices = tf.tensor2d([0, 4, 99], [3, 1], 'int32');
        var updates = tf.tensor2d([100, 101, 102, 777, 778, 779, 10000, 10001, 10002], [3, 3], 'float32');
        var shape = [2, 3];
        expect(function () { return tf.scatterND(indices, updates, shape); }).toThrow();
    });
});
jasmine_util_1.describeWithFlags('sparseToDense CPU', backend_cpu_test_registry_1.CPU_ENVS, function () {
    it('should throw error when index out of range', function () {
        var defaultValue = 2;
        var indices = tf.tensor1d([0, 2, 6], 'int32');
        var values = tf.tensor1d([100, 101, 102], 'int32');
        var shape = [6];
        expect(function () { return tf.sparseToDense(indices, values, shape, defaultValue); })
            .toThrow();
    });
});
jasmine_util_1.describeWithFlags('memory cpu', backend_cpu_test_registry_1.CPU_ENVS, function () {
    it('unreliable is true due to auto gc', function () {
        tf.tensor(1);
        var mem = tf.memory();
        expect(mem.numTensors).toBe(1);
        expect(mem.numDataBuffers).toBe(1);
        expect(mem.numBytes).toBe(4);
        expect(mem.unreliable).toBe(true);
        var expectedReason = 'The reported memory is an upper bound. Due to automatic garbage ' +
            'collection, the true allocated memory may be less.';
        expect(mem.reasons.indexOf(expectedReason) >= 0).toBe(true);
    });
    it('unreliable is true due to both auto gc and string tensors', function () {
        tf.tensor(1);
        tf.tensor('a');
        var mem = tf.memory();
        expect(mem.numTensors).toBe(2);
        expect(mem.numDataBuffers).toBe(2);
        expect(mem.numBytes).toBe(6);
        expect(mem.unreliable).toBe(true);
        var expectedReasonGC = 'The reported memory is an upper bound. Due to automatic garbage ' +
            'collection, the true allocated memory may be less.';
        expect(mem.reasons.indexOf(expectedReasonGC) >= 0).toBe(true);
        var expectedReasonString = 'Memory usage by string tensors is approximate ' +
            '(2 bytes per character)';
        expect(mem.reasons.indexOf(expectedReasonString) >= 0).toBe(true);
    });
});
//# sourceMappingURL=backend_cpu_test.js.map