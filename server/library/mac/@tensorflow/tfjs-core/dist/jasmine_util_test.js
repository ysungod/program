"use strict";
/**
 * @license
 * Copyright 2018 Google LLC. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
Object.defineProperty(exports, "__esModule", { value: true });
var environment_1 = require("./environment");
var tf = require("./index");
var jasmine_util_1 = require("./jasmine_util");
describe('jasmine_util.envSatisfiesConstraints', function () {
    it('ENV satisfies empty constraints', function () {
        var backendName = 'test-backend';
        var env = new environment_1.Environment({});
        env.setFlags({});
        var registeredBackends = ['test-backend1', 'test-backend2'];
        var constraints = {};
        expect(jasmine_util_1.envSatisfiesConstraints(env, backendName, registeredBackends, constraints))
            .toBe(true);
    });
    it('ENV satisfies matching flag constraints, no backend constraint', function () {
        var backendName = 'test-backend';
        var env = new environment_1.Environment({});
        env.setFlags({ 'TEST-FLAG': true });
        var registeredBackends = ['test-backend1', 'test-backend2'];
        var constraints = { flags: { 'TEST-FLAG': true } };
        expect(jasmine_util_1.envSatisfiesConstraints(env, backendName, registeredBackends, constraints))
            .toBe(true);
    });
    it('ENV satisfies matching flag and one backend constraint', function () {
        var backendName = 'test-backend';
        var env = new environment_1.Environment({});
        env.setFlags({ 'TEST-FLAG': true });
        var registeredBackends = ['test-backend1', 'test-backend2'];
        var constraints = { flags: { 'TEST-FLAG': true }, backends: backendName };
        expect(jasmine_util_1.envSatisfiesConstraints(env, backendName, registeredBackends, constraints))
            .toBe(true);
    });
    it('ENV satisfies matching flag and multiple backend constraints', function () {
        var backendName = 'test-backend';
        var env = new environment_1.Environment({});
        env.setFlags({ 'TEST-FLAG': true });
        var registeredBackends = ['test-backend1', 'test-backend2'];
        var constraints = {
            flags: { 'TEST-FLAG': true },
            backends: [backendName, 'other-backend']
        };
        expect(jasmine_util_1.envSatisfiesConstraints(env, backendName, registeredBackends, constraints))
            .toBe(true);
    });
    it('ENV does not satisfy mismatching flags constraints', function () {
        var backendName = 'test-backend';
        var env = new environment_1.Environment({});
        env.setFlags({ 'TEST-FLAG': false });
        var registeredBackends = ['test-backend1', 'test-backend2'];
        var constraints = { flags: { 'TEST-FLAG': true } };
        expect(jasmine_util_1.envSatisfiesConstraints(env, backendName, registeredBackends, constraints))
            .toBe(false);
    });
    it('ENV satisfies no flag constraint but not satisfy activebackend', function () {
        var backendName = 'test-backend';
        var env = new environment_1.Environment({});
        var registeredBackends = ['test-backend1', 'test-backend2'];
        var constraints = { activeBackend: 'test-backend2' };
        expect(jasmine_util_1.envSatisfiesConstraints(env, backendName, registeredBackends, constraints))
            .toBe(false);
    });
    it('ENV satisfies flags but does not satisfy active backend', function () {
        var backendName = 'test-backend';
        var env = new environment_1.Environment({});
        env.setFlags({ 'TEST-FLAG': true });
        var registeredBackends = ['test-backend1', 'test-backend2'];
        var constraints = {
            flags: { 'TEST-FLAG': true },
            activeBackend: 'test-backend2'
        };
        expect(jasmine_util_1.envSatisfiesConstraints(env, backendName, registeredBackends, constraints))
            .toBe(false);
    });
    it('ENV satisfies flags active backend, but not registered backends', function () {
        var backendName = 'test-backend';
        var env = new environment_1.Environment({});
        env.setFlags({ 'TEST-FLAG': true });
        var registeredBackends = ['test-backend1'];
        var constraints = {
            flags: { 'TEST-FLAG': true },
            activeBackend: 'test-backend1',
            registeredBackends: ['test-backend1', 'test-backend2']
        };
        expect(jasmine_util_1.envSatisfiesConstraints(env, backendName, registeredBackends, constraints))
            .toBe(false);
    });
});
describe('jasmine_util.parseKarmaFlags', function () {
    it('parse empty args', function () {
        var res = jasmine_util_1.parseKarmaFlags([]);
        expect(res).toBeNull();
    });
    it('--backend test-backend --flags {"IS_NODE": true}', function () {
        var backend = new jasmine_util_1.TestKernelBackend();
        tf.registerBackend('test-backend', function () { return backend; });
        var res = jasmine_util_1.parseKarmaFlags(['--backend', 'test-backend', '--flags', '{"IS_NODE": true}']);
        expect(res.name).toBe('test-backend');
        expect(res.backendName).toBe('test-backend');
        expect(res.flags).toEqual({ IS_NODE: true });
        tf.removeBackend('test-backend');
    });
    it('"--backend unknown" throws error', function () {
        expect(function () { return jasmine_util_1.parseKarmaFlags(['--backend', 'unknown']); }).toThrowError();
    });
    it('"--flags {}" throws error since --backend is missing', function () {
        expect(function () { return jasmine_util_1.parseKarmaFlags(['--flags', '{}']); }).toThrowError();
    });
    it('"--backend cpu --flags" throws error since features value is missing', function () {
        expect(function () { return jasmine_util_1.parseKarmaFlags(['--backend', 'cpu', '--flags']); })
            .toThrowError();
    });
    it('"--backend cpu --flags notJson" throws error', function () {
        expect(function () { return jasmine_util_1.parseKarmaFlags(['--backend', 'cpu', '--flags', 'notJson']); })
            .toThrowError();
    });
});
//# sourceMappingURL=jasmine_util_test.js.map