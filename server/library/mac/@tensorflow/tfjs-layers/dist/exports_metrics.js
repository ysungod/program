"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var losses = require("./losses");
var metrics = require("./metrics");
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'binaryAccuracy'
 * }
 */
function binaryAccuracy(yTrue, yPred) {
    return metrics.binaryAccuracy(yTrue, yPred);
}
exports.binaryAccuracy = binaryAccuracy;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'binaryCrossentropy'
 * }
 */
function binaryCrossentropy(yTrue, yPred) {
    return metrics.binaryCrossentropy(yTrue, yPred);
}
exports.binaryCrossentropy = binaryCrossentropy;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'sparseCategoricalAccuracy'
 * }
 */
function sparseCategoricalAccuracy(yTrue, yPred) {
    return metrics.sparseCategoricalAccuracy(yTrue, yPred);
}
exports.sparseCategoricalAccuracy = sparseCategoricalAccuracy;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'categoricalAccuracy'
 * }
 */
function categoricalAccuracy(yTrue, yPred) {
    return metrics.categoricalAccuracy(yTrue, yPred);
}
exports.categoricalAccuracy = categoricalAccuracy;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'categoricalCrossentropy'
 * }
 */
function categoricalCrossentropy(yTrue, yPred) {
    return metrics.categoricalCrossentropy(yTrue, yPred);
}
exports.categoricalCrossentropy = categoricalCrossentropy;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'precision'
 * }
 */
function precision(yTrue, yPred) {
    return metrics.precision(yTrue, yPred);
}
exports.precision = precision;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'recall'
 * }
 */
function recall(yTrue, yPred) {
    return metrics.recall(yTrue, yPred);
}
exports.recall = recall;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'cosineProximity'
 * }
 */
function cosineProximity(yTrue, yPred) {
    return losses.cosineProximity(yTrue, yPred);
}
exports.cosineProximity = cosineProximity;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'meanAbsoluteError'
 * }
 */
function meanAbsoluteError(yTrue, yPred) {
    return losses.meanAbsoluteError(yTrue, yPred);
}
exports.meanAbsoluteError = meanAbsoluteError;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'meanAbsolutePercentageError'
 * }
 */
function meanAbsolutePercentageError(yTrue, yPred) {
    return losses.meanAbsolutePercentageError(yTrue, yPred);
}
exports.meanAbsolutePercentageError = meanAbsolutePercentageError;
function MAPE(yTrue, yPred) {
    return losses.meanAbsolutePercentageError(yTrue, yPred);
}
exports.MAPE = MAPE;
function mape(yTrue, yPred) {
    return losses.meanAbsolutePercentageError(yTrue, yPred);
}
exports.mape = mape;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'meanSquaredError'
 * }
 */
function meanSquaredError(yTrue, yPred) {
    return losses.meanSquaredError(yTrue, yPred);
}
exports.meanSquaredError = meanSquaredError;
function MSE(yTrue, yPred) {
    return losses.meanSquaredError(yTrue, yPred);
}
exports.MSE = MSE;
function mse(yTrue, yPred) {
    return losses.meanSquaredError(yTrue, yPred);
}
exports.mse = mse;
//# sourceMappingURL=exports_metrics.js.map