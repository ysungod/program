/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * =============================================================================
 */
import { Constraint, MaxNormArgs, MinMaxNormArgs, UnitNormArgs } from './constraints';
/**
 * @doc {
 *   heading: 'Constraints',
 *   namespace: 'constraints',
 *   useDocsFrom: 'MaxNorm'
 * }
 */
export declare function maxNorm(args: MaxNormArgs): Constraint;
/**
 * @doc {
 *   heading: 'Constraints',
 *   namespace: 'constraints',
 *   useDocsFrom: 'UnitNorm'
 * }
 */
export declare function unitNorm(args: UnitNormArgs): Constraint;
/**
 * @doc {
 *   heading: 'Constraints',
 *   namespace: 'constraints',
 *   useDocsFrom: 'NonNeg'
 * }
 */
export declare function nonNeg(): Constraint;
/**
 * @doc {
 *   heading: 'Constraints',
 *   namespace: 'constraints',
 *   useDocsFrom: 'MinMaxNormConfig'
 * }
 */
export declare function minMaxNorm(config: MinMaxNormArgs): Constraint;
