import { L1Args, L1L2Args, L2Args, Regularizer } from './regularizers';
/**
 * @doc {
 *   heading: 'Regularizers',
 *   namespace: 'regularizers',
 *   useDocsFrom: 'L1L2'
 * }
 */
export declare function l1l2(config?: L1L2Args): Regularizer;
/**
 * @doc {
 *   heading: 'Regularizers',
 *   namespace: 'regularizers',
 *   useDocsFrom: 'L1L2'
 * }
 */
export declare function l1(config?: L1Args): Regularizer;
/**
 * @doc {
 *   heading: 'Regularizers',
 *   namespace: 'regularizers',
 *   useDocsFrom: 'L1L2'
 * }
 */
export declare function l2(config?: L2Args): Regularizer;
