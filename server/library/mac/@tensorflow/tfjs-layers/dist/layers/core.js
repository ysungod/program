"use strict";
/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * =============================================================================
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * TensorFlow.js Layers: Basic Layers.
 */
var tfjs_core_1 = require("@tensorflow/tfjs-core");
var activations_1 = require("../activations");
var K = require("../backend/tfjs_backend");
var constraints_1 = require("../constraints");
var topology_1 = require("../engine/topology");
var errors_1 = require("../errors");
var initializers_1 = require("../initializers");
var regularizers_1 = require("../regularizers");
var generic_utils_1 = require("../utils/generic_utils");
var math_utils_1 = require("../utils/math_utils");
var types_utils_1 = require("../utils/types_utils");
function mapActivationToFusedKernel(className) {
    if (className === 'relu') {
        return 'relu';
    }
    else if (className === 'linear') {
        return 'linear';
    }
    return null;
}
/**
 * Applies
 * [dropout](http://www.cs.toronto.edu/~rsalakhu/papers/srivastava14a.pdf) to
 * the input.
 *
 * Dropout consists in randomly setting a fraction `rate` of input units to 0 at
 * each update during training time, which helps prevent overfitting.
 */
var Dropout = /** @class */ (function (_super) {
    __extends(Dropout, _super);
    function Dropout(args) {
        var _this = _super.call(this, args) || this;
        _this.rate = Math.max(Math.min(args.rate, 1), 0);
        // So that the scalar doesn't get tidied up between executions.
        _this.noiseShape = args.noiseShape;
        _this.seed = args.seed;
        if (_this.seed != null) {
            throw new errors_1.NotImplementedError('Non-default seed is not implemented in Dropout layer yet: ' +
                _this.seed);
        }
        _this.supportsMasking = true;
        return _this;
    }
    Dropout.prototype.getNoiseShape = function (input) {
        if (this.noiseShape == null) {
            return this.noiseShape;
        }
        var inputShape = input.shape;
        var noiseShape = [];
        for (var i = 0; i < this.noiseShape.length; ++i) {
            noiseShape.push(this.noiseShape[i] == null ? inputShape[i] : this.noiseShape[i]);
        }
        return noiseShape;
    };
    Dropout.prototype.call = function (inputs, kwargs) {
        var _this = this;
        return tfjs_core_1.tidy(function () {
            _this.invokeCallHook(inputs, kwargs);
            var input = types_utils_1.getExactlyOneTensor(inputs);
            if (_this.noiseShape != null &&
                !tfjs_core_1.util.arraysEqual(input.shape, _this.noiseShape)) {
                throw new errors_1.NotImplementedError('Non-default noise shape is not implemented in Dropout ' +
                    'layer yet: ' + JSON.stringify(_this.noiseShape));
            }
            if (0 < _this.rate && _this.rate < 1) {
                var training = kwargs['training'] == null ? false : kwargs['training'];
                var noiseShape_1 = _this.getNoiseShape(input);
                var output = K.inTrainPhase(function () { return K.dropout(input, _this.rate, noiseShape_1, _this.seed); }, function () { return input; }, training);
                return output;
            }
            return inputs;
        });
    };
    Dropout.prototype.getConfig = function () {
        var config = {
            rate: this.rate,
            noiseShape: this.noiseShape,
            seed: this.seed,
        };
        var baseConfig = _super.prototype.getConfig.call(this);
        Object.assign(config, baseConfig);
        return config;
    };
    Dropout.prototype.dispose = function () {
        return _super.prototype.dispose.call(this);
    };
    /** @nocollapse */
    Dropout.className = 'Dropout';
    return Dropout;
}(topology_1.Layer));
exports.Dropout = Dropout;
tfjs_core_1.serialization.registerClass(Dropout);
/**
 * Creates a dense (fully connected) layer.
 *
 * This layer implements the operation:
 *   `output = activation(dot(input, kernel) + bias)`
 *
 * `activation` is the element-wise activation function
 *   passed as the `activation` argument.
 *
 * `kernel` is a weights matrix created by the layer.
 *
 * `bias` is a bias vector created by the layer (only applicable if `useBias`
 * is `true`).
 *
 * **Input shape:**
 *
 *   nD `tf.Tensor` with shape: `(batchSize, ..., inputDim)`.
 *
 *   The most common situation would be
 *   a 2D input with shape `(batchSize, inputDim)`.
 *
 * **Output shape:**
 *
 *   nD tensor with shape: `(batchSize, ..., units)`.
 *
 *   For instance, for a 2D input with shape `(batchSize, inputDim)`,
 *   the output would have shape `(batchSize, units)`.
 *
 * Note: if the input to the layer has a rank greater than 2, then it is
 * flattened prior to the initial dot product with the kernel.
 */
var Dense = /** @class */ (function (_super) {
    __extends(Dense, _super);
    function Dense(args) {
        var _this = _super.call(this, args) || this;
        // Default activation: Linear (none).
        _this.activation = null;
        _this.useBias = true;
        _this.kernel = null;
        _this.bias = null;
        _this.DEFAULT_KERNEL_INITIALIZER = 'glorotNormal';
        _this.DEFAULT_BIAS_INITIALIZER = 'zeros';
        if (args.batchInputShape == null && args.inputShape == null &&
            args.inputDim != null) {
            // This logic is copied from Layer's constructor, since we can't
            // do exactly what the Python constructor does for Dense().
            var batchSize = null;
            if (args.batchSize != null) {
                batchSize = args.batchSize;
            }
            _this.batchInputShape = [batchSize, args.inputDim];
        }
        _this.units = args.units;
        generic_utils_1.assertPositiveInteger(_this.units, 'units');
        _this.activation = activations_1.getActivation(args.activation);
        if (args.useBias != null) {
            _this.useBias = args.useBias;
        }
        _this.kernelInitializer = initializers_1.getInitializer(args.kernelInitializer || _this.DEFAULT_KERNEL_INITIALIZER);
        _this.biasInitializer =
            initializers_1.getInitializer(args.biasInitializer || _this.DEFAULT_BIAS_INITIALIZER);
        _this.kernelConstraint = constraints_1.getConstraint(args.kernelConstraint);
        _this.biasConstraint = constraints_1.getConstraint(args.biasConstraint);
        _this.kernelRegularizer = regularizers_1.getRegularizer(args.kernelRegularizer);
        _this.biasRegularizer = regularizers_1.getRegularizer(args.biasRegularizer);
        _this.activityRegularizer = regularizers_1.getRegularizer(args.activityRegularizer);
        _this.supportsMasking = true;
        _this.inputSpec = [{ minNDim: 2 }];
        return _this;
    }
    Dense.prototype.build = function (inputShape) {
        var _a;
        inputShape = types_utils_1.getExactlyOneShape(inputShape);
        var inputLastDim = inputShape[inputShape.length - 1];
        if (this.kernel == null) {
            this.kernel = this.addWeight('kernel', [inputLastDim, this.units], null, this.kernelInitializer, this.kernelRegularizer, true, this.kernelConstraint);
            if (this.useBias) {
                this.bias = this.addWeight('bias', [this.units], null, this.biasInitializer, this.biasRegularizer, true, this.biasConstraint);
            }
        }
        this.inputSpec = [{ minNDim: 2, axes: (_a = {}, _a[-1] = inputLastDim, _a) }];
        this.built = true;
    };
    Dense.prototype.computeOutputShape = function (inputShape) {
        inputShape = types_utils_1.getExactlyOneShape(inputShape);
        var outputShape = inputShape.slice();
        outputShape[outputShape.length - 1] = this.units;
        return outputShape;
    };
    Dense.prototype.call = function (inputs, kwargs) {
        var _this = this;
        return tfjs_core_1.tidy(function () {
            _this.invokeCallHook(inputs, kwargs);
            // Dense layer accepts only a single input.
            var input = types_utils_1.getExactlyOneTensor(inputs);
            var fusedActivationName = mapActivationToFusedKernel(_this.activation.getClassName());
            var output;
            if (fusedActivationName != null) {
                output = K.dot(input, _this.kernel.read(), fusedActivationName, _this.bias ? _this.bias.read() : null);
            }
            else {
                output = K.dot(input, _this.kernel.read());
                if (_this.bias != null) {
                    output = K.biasAdd(output, _this.bias.read());
                }
                if (_this.activation != null) {
                    output = _this.activation.apply(output);
                }
            }
            return output;
        });
    };
    Dense.prototype.getConfig = function () {
        var config = {
            units: this.units,
            activation: activations_1.serializeActivation(this.activation),
            useBias: this.useBias,
            kernelInitializer: initializers_1.serializeInitializer(this.kernelInitializer),
            biasInitializer: initializers_1.serializeInitializer(this.biasInitializer),
            kernelRegularizer: regularizers_1.serializeRegularizer(this.kernelRegularizer),
            biasRegularizer: regularizers_1.serializeRegularizer(this.biasRegularizer),
            activityRegularizer: regularizers_1.serializeRegularizer(this.activityRegularizer),
            kernelConstraint: constraints_1.serializeConstraint(this.kernelConstraint),
            biasConstraint: constraints_1.serializeConstraint(this.biasConstraint)
        };
        var baseConfig = _super.prototype.getConfig.call(this);
        Object.assign(config, baseConfig);
        return config;
    };
    /** @nocollapse */
    Dense.className = 'Dense';
    return Dense;
}(topology_1.Layer));
exports.Dense = Dense;
tfjs_core_1.serialization.registerClass(Dense);
/**
 * Flattens the input. Does not affect the batch size.
 *
 * A `Flatten` layer flattens each batch in its inputs to 1D (making the output
 * 2D).
 *
 * For example:
 *
 * ```js
 * const input = tf.input({shape: [4, 3]});
 * const flattenLayer = tf.layers.flatten();
 * // Inspect the inferred output shape of the flatten layer, which
 * // equals `[null, 12]`. The 2nd dimension is 4 * 3, i.e., the result of the
 * // flattening. (The 1st dimension is the undermined batch size.)
 * console.log(JSON.stringify(flattenLayer.apply(input).shape));
 * ```
 */
var Flatten = /** @class */ (function (_super) {
    __extends(Flatten, _super);
    function Flatten(args) {
        var _this = _super.call(this, args || {}) || this;
        _this.inputSpec = [{ minNDim: 3 }];
        return _this;
    }
    Flatten.prototype.computeOutputShape = function (inputShape) {
        inputShape = types_utils_1.getExactlyOneShape(inputShape);
        for (var _i = 0, _a = inputShape.slice(1); _i < _a.length; _i++) {
            var dim = _a[_i];
            if (dim == null) {
                throw new errors_1.ValueError("The shape of the input to \"Flatten\" is not fully defined " +
                    ("(got " + inputShape.slice(1) + "). Make sure to pass a complete ") +
                    "\"input_shape\" or \"batch_input_shape\" argument to the first " +
                    "layer in your model.");
            }
        }
        return [inputShape[0], math_utils_1.arrayProd(inputShape, 1)];
    };
    Flatten.prototype.call = function (inputs, kwargs) {
        var _this = this;
        return tfjs_core_1.tidy(function () {
            _this.invokeCallHook(inputs, kwargs);
            return K.batchFlatten(types_utils_1.getExactlyOneTensor(inputs));
        });
    };
    /** @nocollapse */
    Flatten.className = 'Flatten';
    return Flatten;
}(topology_1.Layer));
exports.Flatten = Flatten;
tfjs_core_1.serialization.registerClass(Flatten);
/**
 * Applies an activation function to an output.
 *
 * This layer applies element-wise activation function.  Other layers, notably
 * `dense` can also apply activation functions.  Use this isolated activation
 * function to extract the values before and after the
 * activation. For instance:
 *
 * ```js
 * const input = tf.input({shape: [5]});
 * const denseLayer = tf.layers.dense({units: 1});
 * const activationLayer = tf.layers.activation({activation: 'relu6'});
 *
 * // Obtain the output symbolic tensors by applying the layers in order.
 * const denseOutput = denseLayer.apply(input);
 * const activationOutput = activationLayer.apply(denseOutput);
 *
 * // Create the model based on the inputs.
 * const model = tf.LayersModel({
 *     inputs: input,
 *     outputs: [denseOutput, activationOutput]
 * });
 *
 * // Collect both outputs and print separately.
 * const [denseOut, activationOut] = model.predict(tf.randomNormal([6, 5]));
 * denseOut.print();
 * activationOut.print();
 * ```
 *
 */
var Activation = /** @class */ (function (_super) {
    __extends(Activation, _super);
    function Activation(args) {
        var _this = _super.call(this, args) || this;
        _this.supportsMasking = true;
        _this.activation = activations_1.getActivation(args.activation);
        return _this;
    }
    Activation.prototype.call = function (inputs, kwargs) {
        var _this = this;
        return tfjs_core_1.tidy(function () {
            _this.invokeCallHook(inputs, kwargs);
            var input = types_utils_1.getExactlyOneTensor(inputs);
            return _this.activation.apply(input);
        });
    };
    Activation.prototype.getConfig = function () {
        var config = { activation: activations_1.serializeActivation(this.activation) };
        var baseConfig = _super.prototype.getConfig.call(this);
        Object.assign(config, baseConfig);
        return config;
    };
    /** @nocollapse */
    Activation.className = 'Activation';
    return Activation;
}(topology_1.Layer));
exports.Activation = Activation;
tfjs_core_1.serialization.registerClass(Activation);
/**
 * Repeats the input n times in a new dimension.
 *
 * ```js
 *  const model = tf.sequential();
 *  model.add(tf.layers.repeatVector({n: 4, inputShape: [2]}));
 *  const x = tf.tensor2d([[10, 20]]);
 *  // Use the model to do inference on a data point the model hasn't see
 *  model.predict(x).print();
 *  // output shape is now [batch, 2, 4]
 * ```
 */
var RepeatVector = /** @class */ (function (_super) {
    __extends(RepeatVector, _super);
    function RepeatVector(args) {
        var _this = _super.call(this, args) || this;
        _this.n = args.n;
        _this.inputSpec = [{ ndim: 2 }];
        return _this;
    }
    RepeatVector.prototype.computeOutputShape = function (inputShape) {
        return [inputShape[0], this.n, inputShape[1]];
    };
    RepeatVector.prototype.call = function (inputs, kwargs) {
        var _this = this;
        return tfjs_core_1.tidy(function () {
            inputs = types_utils_1.getExactlyOneTensor(inputs);
            return K.repeat(inputs, _this.n);
        });
    };
    RepeatVector.prototype.getConfig = function () {
        var config = {
            n: this.n,
        };
        var baseConfig = _super.prototype.getConfig.call(this);
        Object.assign(config, baseConfig);
        return config;
    };
    /** @nocollapse */
    RepeatVector.className = 'RepeatVector';
    return RepeatVector;
}(topology_1.Layer));
exports.RepeatVector = RepeatVector;
tfjs_core_1.serialization.registerClass(RepeatVector);
/**
 * Reshapes an input to a certain shape.
 *
 * ```js
 * const input = tf.input({shape: [4, 3]});
 * const reshapeLayer = tf.layers.reshape({targetShape: [2, 6]});
 * // Inspect the inferred output shape of the Reshape layer, which
 * // equals `[null, 2, 6]`. (The 1st dimension is the undermined batch size.)
 * console.log(JSON.stringify(reshapeLayer.apply(input).shape));
 * ```
 *
 * Input shape:
 *   Arbitrary: although all dimensions in the input shape must be fixed.
 *     Use the ReshapeLayerConfig field `input_shape` when using this layer
 *     as the first layer in a model.
 *
 * Output shape:
 *   [batchSize, targetShape[0], targetShape[1], ...,
 *    targetShape[targetShape.length - 1]].
 */
var Reshape = /** @class */ (function (_super) {
    __extends(Reshape, _super);
    function Reshape(args) {
        var _this = _super.call(this, args) || this;
        _this.targetShape = args.targetShape;
        // Make sure that all unknown dimensions are represented as `null`.
        for (var i = 0; i < _this.targetShape.length; ++i) {
            if (_this.isUnknown(_this.targetShape[i])) {
                _this.targetShape[i] = null;
            }
        }
        return _this;
    }
    Reshape.prototype.isUnknown = function (dim) {
        return dim < 0 || dim == null;
    };
    /**
     * Finds and replaces a missing dimension in output shape.
     *
     * This is a near direct port of the internal Numpy function
     * `_fix_unknown_dimension` in `numpy/core/src/multiarray/shape.c`.
     *
     * @param inputShape: Original shape of array begin reshape.
     * @param outputShape: Target shape of the array, with at most a single
     * `null` or negative number, which indicates an underdetermined dimension
     * that should be derived from `inputShape` and the known dimensions of
     *   `outputShape`.
     * @returns: The output shape with `null` replaced with its computed value.
     * @throws: ValueError: If `inputShape` and `outputShape` do not match.
     */
    Reshape.prototype.fixUnknownDimension = function (inputShape, outputShape) {
        var errorMsg = 'Total size of new array must be unchanged.';
        var finalShape = outputShape.slice();
        var known = 1;
        var unknown = null;
        for (var i = 0; i < finalShape.length; ++i) {
            var dim = finalShape[i];
            if (this.isUnknown(dim)) {
                if (unknown === null) {
                    unknown = i;
                }
                else {
                    throw new errors_1.ValueError('Can only specifiy one unknown dimension.');
                }
            }
            else {
                known *= dim;
            }
        }
        var originalSize = math_utils_1.arrayProd(inputShape);
        if (unknown !== null) {
            if (known === 0 || originalSize % known !== 0) {
                throw new errors_1.ValueError(errorMsg);
            }
            finalShape[unknown] = originalSize / known;
        }
        else if (originalSize !== known) {
            throw new errors_1.ValueError(errorMsg);
        }
        return finalShape;
    };
    Reshape.prototype.computeOutputShape = function (inputShape) {
        var anyUnknownDims = false;
        for (var i = 0; i < inputShape.length; ++i) {
            if (this.isUnknown(inputShape[i])) {
                anyUnknownDims = true;
                break;
            }
        }
        if (anyUnknownDims) {
            return inputShape.slice(0, 1).concat(this.targetShape);
        }
        else {
            return inputShape.slice(0, 1).concat(this.fixUnknownDimension(inputShape.slice(1), this.targetShape));
        }
    };
    Reshape.prototype.call = function (inputs, kwargs) {
        var _this = this;
        return tfjs_core_1.tidy(function () {
            _this.invokeCallHook(inputs, kwargs);
            var input = types_utils_1.getExactlyOneTensor(inputs);
            var inputShape = input.shape;
            var outputShape = inputShape.slice(0, 1).concat(_this.fixUnknownDimension(inputShape.slice(1), _this.targetShape));
            return input.reshape(outputShape);
        });
    };
    Reshape.prototype.getConfig = function () {
        var config = {
            targetShape: this.targetShape,
        };
        var baseConfig = _super.prototype.getConfig.call(this);
        Object.assign(config, baseConfig);
        return config;
    };
    /** @nocollapse */
    Reshape.className = 'Reshape';
    return Reshape;
}(topology_1.Layer));
exports.Reshape = Reshape;
tfjs_core_1.serialization.registerClass(Reshape);
/**
 * Permutes the dimensions of the input according to a given pattern.
 *
 * Useful for, e.g., connecting RNNs and convnets together.
 *
 * Example:
 *
 * ```js
 * const model = tf.Sequential();
 * model.add(tf.layers.permute({
 *   dims: [2, 1],
 *   inputShape: [10, 64]
 * }));
 * console.log(model.outputShape);
 * // Now model's output shape is [null, 64, 10], where null is the
 * // unpermuted sample (batch) dimension.
 * ```
 *
 * Input shape:
 *   Arbitrary. Use the configuration field `inputShape` when using this
 *   layer as othe first layer in a model.
 *
 * Output shape:
 *   Same rank as the input shape, but with the dimensions re-ordered (i.e.,
 *   permuted) according to the `dims` configuration of this layer.
 */
var Permute = /** @class */ (function (_super) {
    __extends(Permute, _super);
    function Permute(args) {
        var _this = _super.call(this, args) || this;
        if (args.dims == null) {
            throw new Error('Required configuration field `dims` is missing during Permute ' +
                'constructor call.');
        }
        if (!Array.isArray(args.dims)) {
            throw new Error('Permute constructor requires `dims` to be an Array, but received ' +
                (args.dims + " instead."));
        }
        // Check the validity of the permutation indices.
        var expectedSortedIndices = math_utils_1.range(1, args.dims.length + 1);
        if (!tfjs_core_1.util.arraysEqual(args.dims.slice().sort(), expectedSortedIndices)) {
            throw new Error('Invalid permutation `dims`: ' + JSON.stringify(args.dims) +
                ' `dims` must contain consecutive integers starting from 1.');
        }
        _this.dims = args.dims;
        _this.dimsIncludingBatch = [0].concat(_this.dims);
        _this.inputSpec = [new topology_1.InputSpec({ ndim: _this.dims.length + 1 })];
        return _this;
    }
    Permute.prototype.computeOutputShape = function (inputShape) {
        inputShape = types_utils_1.getExactlyOneShape(inputShape);
        var outputShape = inputShape.slice();
        this.dims.forEach(function (dim, i) {
            outputShape[i + 1] = inputShape[dim];
        });
        return outputShape;
    };
    Permute.prototype.call = function (inputs, kwargs) {
        return tfjs_core_1.transpose(types_utils_1.getExactlyOneTensor(inputs), this.dimsIncludingBatch);
    };
    Permute.prototype.getConfig = function () {
        var config = {
            dims: this.dims,
        };
        var baseConfig = _super.prototype.getConfig.call(this);
        Object.assign(config, baseConfig);
        return config;
    };
    /** @nocollapse */
    Permute.className = 'Permute';
    return Permute;
}(topology_1.Layer));
exports.Permute = Permute;
tfjs_core_1.serialization.registerClass(Permute);
/**
 * Masks a sequence by using a mask value to skip timesteps.
 *
 * If all features for a given sample timestep are equal to `mask_value`,
 * then the sample timestep will be masked (skipped) in all downstream layers
 * (as long as they support masking).
 *
 * If any downstream layer does not support masking yet receives such
 * an input mask, an exception will be raised.
 *
 * # Arguments
 *     maskValue: Either None or mask value to skip.
 *
 * # Input shape
 *     Arbitrary. Use the keyword argument `input_shape`
 *     (tuple of integers, does not include the samples axis)
 *     when using this layer as the first layer in a model.
 *
 * # Output shape
 *     Same shape as input.
 */
var Masking = /** @class */ (function (_super) {
    __extends(Masking, _super);
    function Masking(args) {
        var _this = _super.call(this, args == null ? {} : args) || this;
        _this.supportsMasking = true;
        if (args != null) {
            _this.maskValue = args.maskValue == null ? 0 : args.maskValue;
        }
        else {
            _this.maskValue = 0;
        }
        return _this;
    }
    Masking.prototype.computeOutputShape = function (inputShape) {
        return inputShape;
    };
    Masking.prototype.getConfig = function () {
        var baseConfig = _super.prototype.getConfig.call(this);
        var config = { maskValue: this.maskValue };
        Object.assign(config, baseConfig);
        return config;
    };
    Masking.prototype.computeMask = function (inputs, mask) {
        var input = types_utils_1.getExactlyOneTensor(inputs);
        var axis = -1;
        return tfjs_core_1.any(tfjs_core_1.notEqual(input, this.maskValue), axis);
    };
    Masking.prototype.call = function (inputs, kwargs) {
        var _this = this;
        return tfjs_core_1.tidy(function () {
            _this.invokeCallHook(inputs, kwargs);
            var input = types_utils_1.getExactlyOneTensor(inputs);
            var axis = -1;
            var keepDims = true;
            var booleanMask = tfjs_core_1.any(tfjs_core_1.notEqual(input, _this.maskValue), axis, keepDims);
            var output = input.mul(booleanMask.asType(input.dtype));
            return output;
        });
    };
    Masking.className = 'Masking';
    return Masking;
}(topology_1.Layer));
exports.Masking = Masking;
tfjs_core_1.serialization.registerClass(Masking);
//# sourceMappingURL=core.js.map