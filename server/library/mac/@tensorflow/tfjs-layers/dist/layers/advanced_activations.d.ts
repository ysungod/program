/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * =============================================================================
 */
/**
 *  Advanced activation layers.
 */
import { serialization, Tensor } from '@tensorflow/tfjs-core';
import { Constraint } from '../constraints';
import { Layer, LayerArgs } from '../engine/topology';
import { Initializer, InitializerIdentifier } from '../initializers';
import { Shape } from '../keras_format/common';
import { Regularizer } from '../regularizers';
import { Kwargs } from '../types';
export declare interface ReLULayerArgs extends LayerArgs {
    /**
     * Float, the maximum output value.
     */
    maxValue?: number;
}
/**
 * Rectified Linear Unit activation function.
 *
 * Input shape:
 *   Arbitrary. Use the config field `inputShape` (Array of integers, does
 *   not include the sample axis) when using this layer as the first layer
 *   in a model.
 *
 * Output shape:
 *   Same shape as the input.
 */
export declare class ReLU extends Layer {
    /** @nocollapse */
    static className: string;
    maxValue: number;
    constructor(args?: ReLULayerArgs);
    call(inputs: Tensor | Tensor[], kwargs: Kwargs): Tensor | Tensor[];
    computeOutputShape(inputShape: Shape | Shape[]): Shape | Shape[];
    getConfig(): serialization.ConfigDict;
}
export declare interface LeakyReLULayerArgs extends LayerArgs {
    /**
     * Float `>= 0`. Negative slope coefficient. Defaults to `0.3`.
     */
    alpha?: number;
}
/**
 * Leaky version of a rectified linear unit.
 *
 * It allows a small gradient when the unit is not active:
 * `f(x) = alpha * x for x < 0.`
 * `f(x) = x for x >= 0.`
 *
 * Input shape:
 *   Arbitrary. Use the configuration `inputShape` when using this layer as the
 *   first layer in a model.
 *
 * Output shape:
 *   Same shape as the input.
 */
export declare class LeakyReLU extends Layer {
    /** @nocollapse */
    static className: string;
    readonly alpha: number;
    readonly DEFAULT_ALPHA = 0.3;
    constructor(args?: LeakyReLULayerArgs);
    call(inputs: Tensor | Tensor[], kwargs: Kwargs): Tensor | Tensor[];
    computeOutputShape(inputShape: Shape | Shape[]): Shape | Shape[];
    getConfig(): serialization.ConfigDict;
}
export declare interface PReLULayerArgs extends LayerArgs {
    /**
     * Initializer for the learnable alpha.
     */
    alphaInitializer?: Initializer | InitializerIdentifier;
    /**
     * Regularizer for the learnable alpha.
     */
    alphaRegularizer?: Regularizer;
    /**
     * Constraint for the learnable alpha.
     */
    alphaConstraint?: Constraint;
    /**
     * The axes along which to share learnable parameters for the activation
     * function. For example, if the incoming feature maps are from a 2D
     * convolution with output shape `[numExamples, height, width, channels]`,
     * and you wish to share parameters across space (height and width) so that
     * each filter channels has only one set of parameters, set
     * `shared_axes: [1, 2]`.
     */
    sharedAxes?: number | number[];
}
/**
 * Parameterized version of a leaky rectified linear unit.
 *
 * It follows
 * `f(x) = alpha * x for x < 0.`
 * `f(x) = x for x >= 0.`
 * wherein `alpha` is a trainable weight.
 *
 * Input shape:
 *   Arbitrary. Use the configuration `inputShape` when using this layer as the
 *   first layer in a model.
 *
 * Output shape:
 *   Same shape as the input.
 */
export declare class PReLU extends Layer {
    /** @nocollapse */
    static className: string;
    private readonly alphaInitializer;
    private readonly alphaRegularizer;
    private readonly alphaConstraint;
    private readonly sharedAxes;
    private alpha;
    readonly DEFAULT_ALPHA_INITIALIZER: InitializerIdentifier;
    constructor(args?: PReLULayerArgs);
    build(inputShape: Shape | Shape[]): void;
    call(inputs: Tensor | Tensor[], kwargs: Kwargs): Tensor | Tensor[];
    getConfig(): serialization.ConfigDict;
}
export declare interface ELULayerArgs extends LayerArgs {
    /**
     * Float `>= 0`. Negative slope coefficient. Defaults to `1.0`.
     */
    alpha?: number;
}
/**
 * Exponetial Linear Unit (ELU).
 *
 * It follows:
 * `f(x) =  alpha * (exp(x) - 1.) for x < 0`,
 * `f(x) = x for x >= 0`.
 *
 * Input shape:
 *   Arbitrary. Use the configuration `inputShape` when using this layer as the
 *   first layer in a model.
 *
 * Output shape:
 *   Same shape as the input.
 *
 * References:
 *   - [Fast and Accurate Deep Network Learning by Exponential Linear Units
 * (ELUs)](https://arxiv.org/abs/1511.07289v1)
 */
export declare class ELU extends Layer {
    /** @nocollapse */
    static className: string;
    readonly alpha: number;
    readonly DEFAULT_ALPHA = 1;
    constructor(args?: ELULayerArgs);
    call(inputs: Tensor | Tensor[], kwargs: Kwargs): Tensor | Tensor[];
    computeOutputShape(inputShape: Shape | Shape[]): Shape | Shape[];
    getConfig(): serialization.ConfigDict;
}
export declare interface ThresholdedReLULayerArgs extends LayerArgs {
    /**
     * Float >= 0. Threshold location of activation.
     */
    theta?: number;
}
/**
 * Thresholded Rectified Linear Unit.
 *
 * It follows:
 * `f(x) = x for x > theta`,
 * `f(x) = 0 otherwise`.
 *
 * Input shape:
 *   Arbitrary. Use the configuration `inputShape` when using this layer as the
 *   first layer in a model.
 *
 * Output shape:
 *   Same shape as the input.
 *
 * References:
 *   - [Zero-Bias Autoencoders and the Benefits of Co-Adapting
 * Features](http://arxiv.org/abs/1402.3337)
 */
export declare class ThresholdedReLU extends Layer {
    /** @nocollapse */
    static className: string;
    readonly theta: number;
    readonly DEFAULT_THETA = 1;
    constructor(args?: ThresholdedReLULayerArgs);
    call(inputs: Tensor | Tensor[], kwargs: Kwargs): Tensor | Tensor[];
    computeOutputShape(inputShape: Shape | Shape[]): Shape | Shape[];
    getConfig(): serialization.ConfigDict;
}
export declare interface SoftmaxLayerArgs extends LayerArgs {
    /**
     * Integer, axis along which the softmax normalization is applied.
     * Defaults to `-1` (i.e., the last axis).
     */
    axis?: number;
}
/**
 * Softmax activation layer.
 *
 * Input shape:
 *   Arbitrary. Use the configuration `inputShape` when using this layer as the
 *   first layer in a model.
 *
 * Output shape:
 *   Same shape as the input.
 */
export declare class Softmax extends Layer {
    /** @nocollapse */
    static className: string;
    readonly axis: number;
    readonly softmax: (t: Tensor, a?: number) => Tensor;
    readonly DEFAULT_AXIS = 1;
    constructor(args?: SoftmaxLayerArgs);
    call(inputs: Tensor | Tensor[], kwargs: Kwargs): Tensor | Tensor[];
    computeOutputShape(inputShape: Shape | Shape[]): Shape | Shape[];
    getConfig(): serialization.ConfigDict;
}
