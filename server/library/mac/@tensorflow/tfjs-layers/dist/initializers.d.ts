/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * =============================================================================
 */
import { DataType, serialization, Tensor } from '@tensorflow/tfjs-core';
import { Shape } from './keras_format/common';
import { Distribution, FanMode } from './keras_format/initializer_config';
export declare function checkFanMode(value?: string): void;
export declare function checkDistribution(value?: string): void;
/**
 * Initializer base class.
 *
 * @doc {
 *   heading: 'Initializers', subheading: 'Classes', namespace: 'initializers'}
 */
export declare abstract class Initializer extends serialization.Serializable {
    fromConfigUsesCustomObjects(): boolean;
    /**
     * Generate an initial value.
     * @param shape
     * @param dtype
     * @return The init value.
     */
    abstract apply(shape: Shape, dtype?: DataType): Tensor;
    getConfig(): serialization.ConfigDict;
}
/**
 * Initializer that generates tensors initialized to 0.
 */
export declare class Zeros extends Initializer {
    /** @nocollapse */
    static className: string;
    apply(shape: Shape, dtype?: DataType): Tensor;
}
/**
 * Initializer that generates tensors initialized to 1.
 */
export declare class Ones extends Initializer {
    /** @nocollapse */
    static className: string;
    apply(shape: Shape, dtype?: DataType): Tensor;
}
export interface ConstantArgs {
    /** The value for each element in the variable. */
    value: number;
}
/**
 * Initializer that generates values initialized to some constant.
 */
export declare class Constant extends Initializer {
    /** @nocollapse */
    static className: string;
    private value;
    constructor(args: ConstantArgs);
    apply(shape: Shape, dtype?: DataType): Tensor;
    getConfig(): serialization.ConfigDict;
}
export interface RandomUniformArgs {
    /** Lower bound of the range of random values to generate. */
    minval?: number;
    /** Upper bound of the range of random values to generate. */
    maxval?: number;
    /** Used to seed the random generator. */
    seed?: number;
}
/**
 * Initializer that generates random values initialized to a uniform
 * distribution.
 *
 * Values will be distributed uniformly between the configured minval and
 * maxval.
 */
export declare class RandomUniform extends Initializer {
    /** @nocollapse */
    static className: string;
    readonly DEFAULT_MINVAL = -0.05;
    readonly DEFAULT_MAXVAL = 0.05;
    private minval;
    private maxval;
    private seed;
    constructor(args: RandomUniformArgs);
    apply(shape: Shape, dtype?: DataType): Tensor;
    getConfig(): serialization.ConfigDict;
}
export interface RandomNormalArgs {
    /** Mean of the random values to generate. */
    mean?: number;
    /** Standard deviation of the random values to generate. */
    stddev?: number;
    /** Used to seed the random generator. */
    seed?: number;
}
/**
 * Initializer that generates random values initialized to a normal
 * distribution.
 */
export declare class RandomNormal extends Initializer {
    /** @nocollapse */
    static className: string;
    readonly DEFAULT_MEAN = 0;
    readonly DEFAULT_STDDEV = 0.05;
    private mean;
    private stddev;
    private seed;
    constructor(args: RandomNormalArgs);
    apply(shape: Shape, dtype?: DataType): Tensor;
    getConfig(): serialization.ConfigDict;
}
export interface TruncatedNormalArgs {
    /** Mean of the random values to generate. */
    mean?: number;
    /** Standard deviation of the random values to generate. */
    stddev?: number;
    /** Used to seed the random generator. */
    seed?: number;
}
/**
 * Initializer that generates random values initialized to a truncated normal.
 * distribution.
 *
 * These values are similar to values from a `RandomNormal` except that values
 * more than two standard deviations from the mean are discarded and re-drawn.
 * This is the recommended initializer for neural network weights and filters.
 */
export declare class TruncatedNormal extends Initializer {
    /** @nocollapse */
    static className: string;
    readonly DEFAULT_MEAN = 0;
    readonly DEFAULT_STDDEV = 0.05;
    private mean;
    private stddev;
    private seed;
    constructor(args: TruncatedNormalArgs);
    apply(shape: Shape, dtype?: DataType): Tensor;
    getConfig(): serialization.ConfigDict;
}
export interface IdentityArgs {
    /**
     * Multiplicative factor to apply to the identity matrix.
     */
    gain?: number;
}
/**
 * Initializer that generates the identity matrix.
 * Only use for square 2D matrices.
 */
export declare class Identity extends Initializer {
    /** @nocollapse */
    static className: string;
    private gain;
    constructor(args: IdentityArgs);
    apply(shape: Shape, dtype?: DataType): Tensor;
    getConfig(): serialization.ConfigDict;
}
export interface VarianceScalingArgs {
    /** Scaling factor (positive float). */
    scale?: number;
    /** Fanning mode for inputs and outputs. */
    mode?: FanMode;
    /** Probabilistic distribution of the values. */
    distribution?: Distribution;
    /** Random number generator seed. */
    seed?: number;
}
/**
 * Initializer capable of adapting its scale to the shape of weights.
 * With distribution=NORMAL, samples are drawn from a truncated normal
 * distribution centered on zero, with `stddev = sqrt(scale / n)` where n is:
 *   - number of input units in the weight tensor, if mode = FAN_IN.
 *   - number of output units, if mode = FAN_OUT.
 *   - average of the numbers of input and output units, if mode = FAN_AVG.
 * With distribution=UNIFORM,
 * samples are drawn from a uniform distribution
 * within [-limit, limit], with `limit = sqrt(3 * scale / n)`.
 */
export declare class VarianceScaling extends Initializer {
    /** @nocollapse */
    static className: string;
    private scale;
    private mode;
    private distribution;
    private seed;
    /**
     * Constructor of VarianceScaling.
     * @throws ValueError for invalid value in scale.
     */
    constructor(args: VarianceScalingArgs);
    apply(shape: Shape, dtype?: DataType): Tensor;
    getConfig(): serialization.ConfigDict;
}
export interface SeedOnlyInitializerArgs {
    /** Random number generator seed. */
    seed?: number;
}
/**
 * Glorot uniform initializer, also called Xavier uniform initializer.
 * It draws samples from a uniform distribution within [-limit, limit]
 * where `limit` is `sqrt(6 / (fan_in + fan_out))`
 * where `fan_in` is the number of input units in the weight tensor
 * and `fan_out` is the number of output units in the weight tensor
 *
 * Reference:
 *   Glorot & Bengio, AISTATS 2010
 *       http://jmlr.org/proceedings/papers/v9/glorot10a/glorot10a.pdf.
 */
export declare class GlorotUniform extends VarianceScaling {
    /** @nocollapse */
    static className: string;
    /**
     * Constructor of GlorotUniform
     * @param scale
     * @param mode
     * @param distribution
     * @param seed
     */
    constructor(args?: SeedOnlyInitializerArgs);
    getClassName(): string;
}
/**
 * Glorot normal initializer, also called Xavier normal initializer.
 * It draws samples from a truncated normal distribution centered on 0
 * with `stddev = sqrt(2 / (fan_in + fan_out))`
 * where `fan_in` is the number of input units in the weight tensor
 * and `fan_out` is the number of output units in the weight tensor.
 *
 * Reference:
 *   Glorot & Bengio, AISTATS 2010
 *       http://jmlr.org/proceedings/papers/v9/glorot10a/glorot10a.pdf
 */
export declare class GlorotNormal extends VarianceScaling {
    /** @nocollapse */
    static className: string;
    /**
     * Constructor of GlorotNormal.
     * @param scale
     * @param mode
     * @param distribution
     * @param seed
     */
    constructor(args?: SeedOnlyInitializerArgs);
    getClassName(): string;
}
/**
 * He normal initializer.
 *
 * It draws samples from a truncated normal distribution centered on 0
 * with `stddev = sqrt(2 / fanIn)`
 * where `fanIn` is the number of input units in the weight tensor.
 *
 * Reference:
 *     He et al., http://arxiv.org/abs/1502.01852
 */
export declare class HeNormal extends VarianceScaling {
    /** @nocollapse */
    static className: string;
    constructor(args?: SeedOnlyInitializerArgs);
    getClassName(): string;
}
/**
 * He uniform initializer.
 *
 * It draws samples from a uniform distribution within [-limit, limit]
 * where `limit` is `sqrt(6 / fan_in)`
 * where `fanIn` is the number of input units in the weight tensor.
 *
 * Reference:
 *     He et al., http://arxiv.org/abs/1502.01852
 */
export declare class HeUniform extends VarianceScaling {
    /** @nocollapse */
    static className: string;
    constructor(args?: SeedOnlyInitializerArgs);
    getClassName(): string;
}
/**
 * LeCun normal initializer.
 *
 * It draws samples from a truncated normal distribution centered on 0
 * with `stddev = sqrt(1 / fanIn)`
 * where `fanIn` is the number of input units in the weight tensor.
 *
 * References:
 *   [Self-Normalizing Neural Networks](https://arxiv.org/abs/1706.02515)
 *   [Efficient Backprop](http://yann.lecun.com/exdb/publis/pdf/lecun-98b.pdf)
 */
export declare class LeCunNormal extends VarianceScaling {
    /** @nocollapse */
    static className: string;
    constructor(args?: SeedOnlyInitializerArgs);
    getClassName(): string;
}
/**
 * LeCun uniform initializer.
 *
 * It draws samples from a uniform distribution in the interval
 * `[-limit, limit]` with `limit = sqrt(3 / fanIn)`,
 * where `fanIn` is the number of input units in the weight tensor.
 */
export declare class LeCunUniform extends VarianceScaling {
    /** @nocollapse */
    static className: string;
    constructor(args?: SeedOnlyInitializerArgs);
    getClassName(): string;
}
export interface OrthogonalArgs extends SeedOnlyInitializerArgs {
    /**
     * Multiplicative factor to apply to the orthogonal matrix. Defaults to 1.
     */
    gain?: number;
}
/**
 * Initializer that generates a random orthogonal matrix.
 *
 * Reference:
 * [Saxe et al., http://arxiv.org/abs/1312.6120](http://arxiv.org/abs/1312.6120)
 */
export declare class Orthogonal extends Initializer {
    /** @nocollapse */
    static className: string;
    readonly DEFAULT_GAIN = 1;
    protected readonly gain: number;
    protected readonly seed: number;
    constructor(args?: OrthogonalArgs);
    apply(shape: Shape, dtype?: DataType): Tensor;
    getConfig(): serialization.ConfigDict;
}
/** @docinline */
export declare type InitializerIdentifier = 'constant' | 'glorotNormal' | 'glorotUniform' | 'heNormal' | 'heUniform' | 'identity' | 'leCunNormal' | 'leCunUniform' | 'ones' | 'orthogonal' | 'randomNormal' | 'randomUniform' | 'truncatedNormal' | 'varianceScaling' | 'zeros' | string;
export declare const INITIALIZER_IDENTIFIER_REGISTRY_SYMBOL_MAP: {
    [identifier in InitializerIdentifier]: string;
};
export declare function serializeInitializer(initializer: Initializer): serialization.ConfigDictValue;
export declare function getInitializer(identifier: InitializerIdentifier | Initializer | serialization.ConfigDict): Initializer;
