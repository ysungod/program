/**
 * @license
 * Copyright 2018 Google Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
import * as tfc from '@tensorflow/tfjs-core';
import { NodeJSKernelBackend } from '../nodejs_kernel_backend';
import { TFEOpAttr } from '../tfjs_binding';
/** Returns an instance of the Node.js backend. */
export declare function nodeBackend(): NodeJSKernelBackend;
/** Returns the TF dtype for a given DataType. */
export declare function getTFDType(dataType: tfc.DataType): number;
/**
 * Creates a TFEOpAttr for a 'type' OpDef attribute.
 * @deprecated Please use createTensorsTypeOpAttr() going forward.
 */
export declare function createTypeOpAttr(attrName: string, dtype: tfc.DataType): TFEOpAttr;
/**
 * Creates a TFEOpAttr for a 'type' OpDef attribute from a Tensor or list of
 * Tensors.
 */
export declare function createTensorsTypeOpAttr(attrName: string, tensors: tfc.Tensor | tfc.Tensor[]): {
    name: string;
    type: number;
    value: number;
};
