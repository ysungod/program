/**
 * @license
 * Copyright 2017 Google Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
/// <reference types="webgl2" />
export interface TextureConfig {
    internalFormatFloat: number;
    textureFormatFloat: number;
    internalFormatPackedHalfFloat: number;
    internalFormatHalfFloat: number;
    internalFormatPackedFloat: number;
    downloadTextureFormat: number;
    downloadUnpackNumChannels: number;
    defaultNumChannels: number;
    textureTypeHalfFloat: number;
}
export declare function createVertexShader(gl: WebGLRenderingContext, debug: boolean): WebGLShader;
export declare function createVertexBuffer(gl: WebGLRenderingContext, debug: boolean): WebGLBuffer;
export declare function createIndexBuffer(gl: WebGLRenderingContext, debug: boolean): WebGLBuffer;
export declare function getTextureConfig(gl: WebGLRenderingContext, textureHalfFloatExtension?: any): TextureConfig;
export declare function createFloat32MatrixTexture(gl: WebGLRenderingContext, debug: boolean, rows: number, columns: number, textureConfig: TextureConfig): WebGLTexture;
export declare function createFloat16MatrixTexture(gl: WebGLRenderingContext, debug: boolean, rows: number, columns: number, textureConfig: TextureConfig): WebGLTexture;
export declare function createUnsignedBytesMatrixTexture(gl: WebGLRenderingContext, debug: boolean, rows: number, columns: number, textureConfig: TextureConfig): WebGLTexture;
export declare function createPackedMatrixTexture(gl: WebGLRenderingContext, debug: boolean, rows: number, columns: number, textureConfig: TextureConfig): WebGLTexture;
export declare function createFloat16PackedMatrixTexture(gl: WebGLRenderingContext, debug: boolean, rows: number, columns: number, textureConfig: TextureConfig): WebGLTexture;
export declare function bindVertexProgramAttributeStreams(gl: WebGLRenderingContext, debug: boolean, program: WebGLProgram, vertexBuffer: WebGLBuffer): boolean;
export declare function uploadPixelDataToTexture(gl: WebGLRenderingContext, debug: boolean, texture: WebGLTexture, pixels: ImageData | HTMLImageElement | HTMLCanvasElement | HTMLVideoElement): void;
export declare function uploadMatrixToTexture(gl: WebGLRenderingContext, debug: boolean, texture: WebGLTexture, rows: number, columns: number, matrix: Float32Array, numChannels: number, textureConfig: TextureConfig): void;
/**
 * This method writes a tensor to a packed texture in a way that respects how we
 * represent data using each texel's r,g,b,a channels. Specifically, we lay
 * out the four channels in two rows each containing two channels, so a single
 * texel can represent up to four values from the tensor. That means a texture
 * that has a channel width of 11 and channel height of 4 will have a texel
 * width of 6 and texel height of 2.
 *
 * rows, columns: Logical number of rows and columns in the tensor to be
 * uploaded.
 *
 * physicalRows, physicalCols: Channel dimensions of the texture that will hold
 * the tensor.
 *
 * width, height (internal parameters): Texel dimensions of the texture.
 */
export declare function uploadMatrixToPackedTexture(gl: WebGLRenderingContext, debug: boolean, texture: WebGLTexture, batch: number, rows: number, columns: number, physicalRows: number, physicalCols: number, matrix: Float32Array, textureConfig: TextureConfig): void;
export declare function createBufferFromOutputTexture(gl2: WebGL2RenderingContext, debug: boolean, rows: number, columns: number, textureConfig: TextureConfig): WebGLBuffer;
export declare function downloadFloat32MatrixFromBuffer(gl: WebGLRenderingContext, buffer: WebGLBuffer, rows: number, columns: number, textureConfig: TextureConfig): Float32Array;
export declare function downloadFloat32MatrixFromOutputTexture(gl: WebGLRenderingContext, debug: boolean, rows: number, columns: number, textureConfig: TextureConfig): Float32Array;
export declare function downloadByteEncodedFloatMatrixFromOutputTexture(gl: WebGLRenderingContext, debug: boolean, rows: number, columns: number, textureConfig: TextureConfig): Float32Array;
export declare function downloadPackedMatrixFromBuffer(gl: WebGLRenderingContext, buffer: WebGLBuffer, batch: number, rows: number, cols: number, physicalRows: number, physicalCols: number, textureConfig: TextureConfig): Float32Array;
export declare function downloadMatrixFromPackedOutputTexture(gl: WebGLRenderingContext, debug: boolean, batch: number, rows: number, cols: number, physicalRows: number, physicalCols: number, textureConfig: TextureConfig): Float32Array;
