"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @license
 * Copyright 2017 Google Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
var backend_1 = require("./backends/backend");
var engine_1 = require("./engine");
var environment_1 = require("./environment");
Error.stackTraceLimit = Infinity;
exports.NODE_ENVS = {
    flags: { 'IS_NODE': true }
};
exports.CHROME_ENVS = {
    flags: { 'IS_CHROME': true }
};
exports.BROWSER_ENVS = {
    flags: { 'IS_BROWSER': true }
};
exports.ALL_ENVS = {};
// Tests whether the current environment satisfies the set of constraints.
function envSatisfiesConstraints(env, activeBackend, registeredBackends, constraints) {
    if (constraints == null) {
        return true;
    }
    if (constraints.flags != null) {
        for (var flagName in constraints.flags) {
            var flagValue = constraints.flags[flagName];
            if (env.get(flagName) !== flagValue) {
                return false;
            }
        }
    }
    if (constraints.activeBackend != null) {
        return activeBackend === constraints.activeBackend;
    }
    if (constraints.registeredBackends != null) {
        for (var i = 0; i < constraints.registeredBackends.length; i++) {
            var registeredBackendConstraint = constraints.registeredBackends[i];
            if (registeredBackends.indexOf(registeredBackendConstraint) === -1) {
                return false;
            }
        }
    }
    return true;
}
exports.envSatisfiesConstraints = envSatisfiesConstraints;
function parseKarmaFlags(args) {
    var flags;
    var factory;
    var backendName = '';
    var backendNames = engine_1.ENGINE.backendNames()
        .map(function (backendName) { return '\'' + backendName + '\''; })
        .join(', ');
    args.forEach(function (arg, i) {
        if (arg === '--flags') {
            flags = JSON.parse(args[i + 1]);
        }
        else if (arg === '--backend') {
            var type = args[i + 1];
            backendName = type;
            factory = engine_1.ENGINE.findBackendFactory(backendName.toLowerCase());
            if (factory == null) {
                throw new Error("Unknown value " + type + " for flag --backend. " +
                    ("Allowed values are " + backendNames + "."));
            }
        }
    });
    if (flags == null && factory == null) {
        return null;
    }
    if (flags != null && factory == null) {
        throw new Error('--backend flag is required when --flags is present. ' +
            ("Available values are " + backendNames + "."));
    }
    return { flags: flags || {}, name: backendName, backendName: backendName };
}
exports.parseKarmaFlags = parseKarmaFlags;
function describeWithFlags(name, constraints, tests) {
    exports.TEST_ENVS.forEach(function (testEnv) {
        environment_1.ENV.setFlags(testEnv.flags);
        if (envSatisfiesConstraints(environment_1.ENV, testEnv.backendName, engine_1.ENGINE.backendNames(), constraints)) {
            var testName = name + ' ' + testEnv.name + ' ' + JSON.stringify(testEnv.flags);
            executeTests(testName, tests, testEnv);
        }
    });
}
exports.describeWithFlags = describeWithFlags;
exports.TEST_ENVS = [];
// Whether a call to setTestEnvs has been called so we turn off
// registration. This allows command line overriding or programmatic
// overriding of the default registrations.
var testEnvSet = false;
function setTestEnvs(testEnvs) {
    testEnvSet = true;
    exports.TEST_ENVS = testEnvs;
}
exports.setTestEnvs = setTestEnvs;
function registerTestEnv(testEnv) {
    // When using an explicit call to setTestEnvs, turn off registration of
    // test environments because the explicit call will set the test
    // environments.
    if (testEnvSet) {
        return;
    }
    exports.TEST_ENVS.push(testEnv);
}
exports.registerTestEnv = registerTestEnv;
if (typeof __karma__ !== 'undefined') {
    var testEnv = parseKarmaFlags(__karma__.config.args);
    if (testEnv != null) {
        setTestEnvs([testEnv]);
    }
}
function executeTests(testName, tests, testEnv) {
    describe(testName, function () {
        beforeAll(function () {
            engine_1.ENGINE.reset();
            if (testEnv.flags != null) {
                environment_1.ENV.setFlags(testEnv.flags);
            }
            environment_1.ENV.set('IS_TEST', true);
            engine_1.ENGINE.setBackend(testEnv.backendName);
        });
        beforeEach(function () {
            engine_1.ENGINE.startScope();
        });
        afterEach(function () {
            engine_1.ENGINE.endScope();
            engine_1.ENGINE.disposeVariables();
        });
        afterAll(function () {
            engine_1.ENGINE.reset();
        });
        tests(testEnv);
    });
}
var TestKernelBackend = /** @class */ (function (_super) {
    __extends(TestKernelBackend, _super);
    function TestKernelBackend() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TestKernelBackend.prototype.dispose = function () { };
    return TestKernelBackend;
}(backend_1.KernelBackend));
exports.TestKernelBackend = TestKernelBackend;
//# sourceMappingURL=jasmine_util.js.map