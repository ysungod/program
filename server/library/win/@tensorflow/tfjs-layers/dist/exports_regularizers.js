"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * =============================================================================
 */
var regularizers = require("./regularizers");
// tslint:disable-next-line:max-line-length
var regularizers_1 = require("./regularizers");
/**
 * @doc {
 *   heading: 'Regularizers',
 *   namespace: 'regularizers',
 *   useDocsFrom: 'L1L2'
 * }
 */
function l1l2(config) {
    return new regularizers_1.L1L2(config);
}
exports.l1l2 = l1l2;
/**
 * @doc {
 *   heading: 'Regularizers',
 *   namespace: 'regularizers',
 *   useDocsFrom: 'L1L2'
 * }
 */
function l1(config) {
    return regularizers.l1(config);
}
exports.l1 = l1;
/**
 * @doc {
 *   heading: 'Regularizers',
 *   namespace: 'regularizers',
 *   useDocsFrom: 'L1L2'
 * }
 */
function l2(config) {
    return regularizers.l2(config);
}
exports.l2 = l2;
//# sourceMappingURL=exports_regularizers.js.map