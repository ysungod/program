/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * =============================================================================
 */
import { InputLayerArgs } from './engine/input_layer';
import { Layer, LayerArgs } from './engine/topology';
import { input } from './exports';
import { ELULayerArgs, LeakyReLULayerArgs, PReLULayerArgs, ReLULayerArgs, SoftmaxLayerArgs, ThresholdedReLULayerArgs } from './layers/advanced_activations';
import { ConvLayerArgs, Cropping2DLayerArgs, SeparableConvLayerArgs, UpSampling2DLayerArgs } from './layers/convolutional';
import { DepthwiseConv2DLayerArgs } from './layers/convolutional_depthwise';
import { ActivationLayerArgs, DenseLayerArgs, DropoutLayerArgs, PermuteLayerArgs, RepeatVectorLayerArgs, ReshapeLayerArgs, MaskingArgs } from './layers/core';
import { EmbeddingLayerArgs } from './layers/embeddings';
import { ConcatenateLayerArgs, DotLayerArgs } from './layers/merge';
import { BatchNormalizationLayerArgs } from './layers/normalization';
import { ZeroPadding2DLayerArgs } from './layers/padding';
import { GlobalPooling2DLayerArgs, Pooling1DLayerArgs, Pooling2DLayerArgs } from './layers/pooling';
import { GRUCellLayerArgs, GRULayerArgs, LSTMCellLayerArgs, LSTMLayerArgs, RNN, RNNCell, RNNLayerArgs, SimpleRNNCellLayerArgs, SimpleRNNLayerArgs, StackedRNNCellsArgs } from './layers/recurrent';
import { Bidirectional, BidirectionalLayerArgs, WrapperLayerArgs } from './layers/wrappers';
import { GaussianNoiseArgs, GaussianNoise, GaussianDropoutArgs, GaussianDropout, AlphaDropoutArgs, AlphaDropout } from './layers/noise';
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Inputs',
 *   namespace: 'layers',
 *   useDocsFrom: 'InputLayer'
 * }
 */
export declare function inputLayer(args: InputLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'ELU'
 * }
 */
export declare function elu(args?: ELULayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'ReLU'
 * }
 */
export declare function reLU(args?: ReLULayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'LeakyReLU'
 * }
 */
export declare function leakyReLU(args?: LeakyReLULayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'PReLU'
 * }
 */
export declare function prelu(args?: PReLULayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'Softmax'
 * }
 */
export declare function softmax(args?: SoftmaxLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'ThresholdedReLU'
 * }
 */
export declare function thresholdedReLU(args?: ThresholdedReLULayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'Conv1D'
 * }
 */
export declare function conv1d(args: ConvLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'Conv2D'
 * }
 */
export declare function conv2d(args: ConvLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'Conv2DTranspose'
 * }
 */
export declare function conv2dTranspose(args: ConvLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'Conv3D'
 * }
 */
export declare function conv3d(args: ConvLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'SeparableConv2D'
 * }
 */
export declare function separableConv2d(args: SeparableConvLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'Cropping2D'
 * }
 */
export declare function cropping2D(args: Cropping2DLayerArgs): Layer;
/**
 * @doc{
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'UpSampling2D'
 * }
 */
export declare function upSampling2d(args: UpSampling2DLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'DepthwiseConv2D'
 * }
 */
export declare function depthwiseConv2d(args: DepthwiseConv2DLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Activation'
 * }
 */
export declare function activation(args: ActivationLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Dense'
 * }
 */
export declare function dense(args: DenseLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Dropout'
 * }
 */
export declare function dropout(args: DropoutLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Flatten'
 * }
 */
export declare function flatten(args?: LayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'RepeatVector'
 * }
 */
export declare function repeatVector(args: RepeatVectorLayerArgs): Layer;
/**
 * @doc{
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Reshape'
 * }
 */
export declare function reshape(args: ReshapeLayerArgs): Layer;
/**
 * @doc{
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Permute'
 * }
 */
export declare function permute(args: PermuteLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *    useDocsFrom: 'Embedding'
 * }
 */
export declare function embedding(args: EmbeddingLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Add'
 * }
 */
export declare function add(args?: LayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Average'
 * }
 */
export declare function average(args?: LayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Concatenate'
 * }
 */
export declare function concatenate(args?: ConcatenateLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Maximum'
 * }
 */
export declare function maximum(args?: LayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Minimum'
 * }
 */
export declare function minimum(args?: LayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Multiply'
 * }
 */
export declare function multiply(args?: LayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Dot'
 * }
 */
export declare function dot(args: DotLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Normalization',
 *   namespace: 'layers',
 *   useDocsFrom: 'BatchNormalization'
 * }
 */
export declare function batchNormalization(args?: BatchNormalizationLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Padding',
 *   namespace: 'layers',
 *   useDocsFrom: 'ZeroPadding2D'
 * }
 */
export declare function zeroPadding2d(args?: ZeroPadding2DLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'AveragePooling1D'
 * }
 */
export declare function averagePooling1d(args: Pooling1DLayerArgs): Layer;
export declare function avgPool1d(args: Pooling1DLayerArgs): Layer;
export declare function avgPooling1d(args: Pooling1DLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'AveragePooling2D'
 * }
 */
export declare function averagePooling2d(args: Pooling2DLayerArgs): Layer;
export declare function avgPool2d(args: Pooling2DLayerArgs): Layer;
export declare function avgPooling2d(args: Pooling2DLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'GlobalAveragePooling1D'
 * }
 */
export declare function globalAveragePooling1d(args?: LayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'GlobalAveragePooling2D'
 * }
 */
export declare function globalAveragePooling2d(args: GlobalPooling2DLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'GlobalMaxPooling1D'
 * }
 */
export declare function globalMaxPooling1d(args?: LayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'GlobalMaxPooling2D'
 * }
 */
export declare function globalMaxPooling2d(args: GlobalPooling2DLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'MaxPooling1D'
 * }
 */
export declare function maxPooling1d(args: Pooling1DLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'MaxPooling2D'
 * }
 */
export declare function maxPooling2d(args: Pooling2DLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'GRU'
 * }
 */
export declare function gru(args: GRULayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'GRUCell'
 * }
 */
export declare function gruCell(args: GRUCellLayerArgs): RNNCell;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'LSTM'
 * }
 */
export declare function lstm(args: LSTMLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'LSTMCell'
 * }
 */
export declare function lstmCell(args: LSTMCellLayerArgs): RNNCell;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'SimpleRNN'
 * }
 */
export declare function simpleRNN(args: SimpleRNNLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'SimpleRNNCell'
 * }
 */
export declare function simpleRNNCell(args: SimpleRNNCellLayerArgs): RNNCell;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'RNN'
 * }
 */
export declare function rnn(args: RNNLayerArgs): Layer;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'RNN'
 * }
 */
export declare function stackedRNNCells(args: StackedRNNCellsArgs): RNNCell;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Wrapper',
 *   namespace: 'layers',
 *   useDocsFrom: 'Bidirectional'
 * }
 */
export declare function bidirectional(args: BidirectionalLayerArgs): Bidirectional;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Wrapper',
 *   namespace: 'layers',
 *   useDocsFrom: 'TimeDistributed'
 * }
 */
export declare function timeDistributed(args: WrapperLayerArgs): Layer;
export declare const globalMaxPool1d: typeof globalMaxPooling1d;
export declare const globalMaxPool2d: typeof globalMaxPooling2d;
export declare const maxPool1d: typeof maxPooling1d;
export declare const maxPool2d: typeof maxPooling2d;
export { Layer, RNN, RNNCell, input };
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Noise',
 *   namespace: 'layers',
 *   useDocsFrom: 'GaussianNoise'
 * }
 */
export declare function gaussianNoise(args: GaussianNoiseArgs): GaussianNoise;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Noise',
 *   namespace: 'layers',
 *   useDocsFrom: 'GaussianDropout'
 * }
 */
export declare function gaussianDropout(args: GaussianDropoutArgs): GaussianDropout;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Noise',
 *   namespace: 'layers',
 *   useDocsFrom: 'AlphaDropout'
 * }
 */
export declare function alphaDropout(args: AlphaDropoutArgs): AlphaDropout;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Mask',
 *   namespace: 'layers',
 *   useDocsFrom: 'Masking'
 * }
 */
export declare function masking(args?: MaskingArgs): Layer;
