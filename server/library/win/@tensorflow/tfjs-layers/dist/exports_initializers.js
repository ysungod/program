"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * =============================================================================
 */
// tslint:disable-next-line:max-line-length
var initializers_1 = require("./initializers");
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'Zeros'
 * }
 */
function zeros() {
    return new initializers_1.Zeros();
}
exports.zeros = zeros;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'Ones'
 * }
 */
function ones() {
    return new initializers_1.Ones();
}
exports.ones = ones;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'Constant'
 * }
 */
function constant(args) {
    return new initializers_1.Constant(args);
}
exports.constant = constant;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'RandomUniform'
 * }
 */
function randomUniform(args) {
    return new initializers_1.RandomUniform(args);
}
exports.randomUniform = randomUniform;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'RandomNormal'
 * }
 */
function randomNormal(args) {
    return new initializers_1.RandomNormal(args);
}
exports.randomNormal = randomNormal;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'TruncatedNormal'
 * }
 */
function truncatedNormal(args) {
    return new initializers_1.TruncatedNormal(args);
}
exports.truncatedNormal = truncatedNormal;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'Identity'
 * }
 */
function identity(args) {
    return new initializers_1.Identity(args);
}
exports.identity = identity;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'VarianceScaling'
 * }
 */
function varianceScaling(config) {
    return new initializers_1.VarianceScaling(config);
}
exports.varianceScaling = varianceScaling;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'GlorotUniform'
 * }
 */
function glorotUniform(args) {
    return new initializers_1.GlorotUniform(args);
}
exports.glorotUniform = glorotUniform;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'GlorotNormal'
 * }
 */
function glorotNormal(args) {
    return new initializers_1.GlorotNormal(args);
}
exports.glorotNormal = glorotNormal;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'HeNormal'
 * }
 */
function heNormal(args) {
    return new initializers_1.HeNormal(args);
}
exports.heNormal = heNormal;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'HeUniform'
 * }
 */
function heUniform(args) {
    return new initializers_1.HeUniform(args);
}
exports.heUniform = heUniform;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'LeCunNormal'
 * }
 */
function leCunNormal(args) {
    return new initializers_1.LeCunNormal(args);
}
exports.leCunNormal = leCunNormal;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'LeCunUniform'
 * }
 */
function leCunUniform(args) {
    return new initializers_1.LeCunUniform(args);
}
exports.leCunUniform = leCunUniform;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'Orthogonal'
 * }
 */
function orthogonal(args) {
    return new initializers_1.Orthogonal(args);
}
exports.orthogonal = orthogonal;
//# sourceMappingURL=exports_initializers.js.map