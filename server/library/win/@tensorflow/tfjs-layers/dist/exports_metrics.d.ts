/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * =============================================================================
 */
import { Tensor } from '@tensorflow/tfjs-core';
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'binaryAccuracy'
 * }
 */
export declare function binaryAccuracy(yTrue: Tensor, yPred: Tensor): Tensor;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'binaryCrossentropy'
 * }
 */
export declare function binaryCrossentropy(yTrue: Tensor, yPred: Tensor): Tensor;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'sparseCategoricalAccuracy'
 * }
 */
export declare function sparseCategoricalAccuracy(yTrue: Tensor, yPred: Tensor): Tensor;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'categoricalAccuracy'
 * }
 */
export declare function categoricalAccuracy(yTrue: Tensor, yPred: Tensor): Tensor;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'categoricalCrossentropy'
 * }
 */
export declare function categoricalCrossentropy(yTrue: Tensor, yPred: Tensor): Tensor;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'precision'
 * }
 */
export declare function precision(yTrue: Tensor, yPred: Tensor): Tensor;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'recall'
 * }
 */
export declare function recall(yTrue: Tensor, yPred: Tensor): Tensor;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'cosineProximity'
 * }
 */
export declare function cosineProximity(yTrue: Tensor, yPred: Tensor): Tensor;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'meanAbsoluteError'
 * }
 */
export declare function meanAbsoluteError(yTrue: Tensor, yPred: Tensor): Tensor;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'meanAbsolutePercentageError'
 * }
 */
export declare function meanAbsolutePercentageError(yTrue: Tensor, yPred: Tensor): Tensor;
export declare function MAPE(yTrue: Tensor, yPred: Tensor): Tensor;
export declare function mape(yTrue: Tensor, yPred: Tensor): Tensor;
/**
 * @doc {
 *   heading: 'Metrics',
 *   namespace: 'metrics',
 *   useDocsFrom: 'meanSquaredError'
 * }
 */
export declare function meanSquaredError(yTrue: Tensor, yPred: Tensor): Tensor;
export declare function MSE(yTrue: Tensor, yPred: Tensor): Tensor;
export declare function mse(yTrue: Tensor, yPred: Tensor): Tensor;
