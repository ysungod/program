/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * =============================================================================
 */
import { ConstantArgs, IdentityArgs, Initializer, OrthogonalArgs, RandomNormalArgs, RandomUniformArgs, SeedOnlyInitializerArgs, TruncatedNormalArgs, VarianceScalingArgs, Zeros } from './initializers';
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'Zeros'
 * }
 */
export declare function zeros(): Zeros;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'Ones'
 * }
 */
export declare function ones(): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'Constant'
 * }
 */
export declare function constant(args: ConstantArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'RandomUniform'
 * }
 */
export declare function randomUniform(args: RandomUniformArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'RandomNormal'
 * }
 */
export declare function randomNormal(args: RandomNormalArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'TruncatedNormal'
 * }
 */
export declare function truncatedNormal(args: TruncatedNormalArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'Identity'
 * }
 */
export declare function identity(args: IdentityArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'VarianceScaling'
 * }
 */
export declare function varianceScaling(config: VarianceScalingArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'GlorotUniform'
 * }
 */
export declare function glorotUniform(args: SeedOnlyInitializerArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'GlorotNormal'
 * }
 */
export declare function glorotNormal(args: SeedOnlyInitializerArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'HeNormal'
 * }
 */
export declare function heNormal(args: SeedOnlyInitializerArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'HeUniform'
 * }
 */
export declare function heUniform(args: SeedOnlyInitializerArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'LeCunNormal'
 * }
 */
export declare function leCunNormal(args: SeedOnlyInitializerArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'LeCunUniform'
 * }
 */
export declare function leCunUniform(args: SeedOnlyInitializerArgs): Initializer;
/**
 * @doc {
 *   heading: 'Initializers',
 *   namespace: 'initializers',
 *   useDocsFrom: 'Orthogonal'
 * }
 */
export declare function orthogonal(args: OrthogonalArgs): Initializer;
