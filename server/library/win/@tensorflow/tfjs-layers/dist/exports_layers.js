"use strict";
/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * =============================================================================
 */
Object.defineProperty(exports, "__esModule", { value: true });
var input_layer_1 = require("./engine/input_layer");
var topology_1 = require("./engine/topology");
exports.Layer = topology_1.Layer;
var exports_1 = require("./exports");
exports.input = exports_1.input;
var advanced_activations_1 = require("./layers/advanced_activations");
var convolutional_1 = require("./layers/convolutional");
var convolutional_depthwise_1 = require("./layers/convolutional_depthwise");
var core_1 = require("./layers/core");
var embeddings_1 = require("./layers/embeddings");
var merge_1 = require("./layers/merge");
var normalization_1 = require("./layers/normalization");
var padding_1 = require("./layers/padding");
var pooling_1 = require("./layers/pooling");
var recurrent_1 = require("./layers/recurrent");
exports.RNN = recurrent_1.RNN;
exports.RNNCell = recurrent_1.RNNCell;
var wrappers_1 = require("./layers/wrappers");
var noise_1 = require("./layers/noise");
// TODO(cais): Add doc string to all the public static functions in this
//   class; include exectuable JavaScript code snippets where applicable
//   (b/74074458).
// Input Layer.
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Inputs',
 *   namespace: 'layers',
 *   useDocsFrom: 'InputLayer'
 * }
 */
function inputLayer(args) {
    return new input_layer_1.InputLayer(args);
}
exports.inputLayer = inputLayer;
// Advanced Activation Layers.
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'ELU'
 * }
 */
function elu(args) {
    return new advanced_activations_1.ELU(args);
}
exports.elu = elu;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'ReLU'
 * }
 */
function reLU(args) {
    return new advanced_activations_1.ReLU(args);
}
exports.reLU = reLU;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'LeakyReLU'
 * }
 */
function leakyReLU(args) {
    return new advanced_activations_1.LeakyReLU(args);
}
exports.leakyReLU = leakyReLU;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'PReLU'
 * }
 */
function prelu(args) {
    return new advanced_activations_1.PReLU(args);
}
exports.prelu = prelu;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'Softmax'
 * }
 */
function softmax(args) {
    return new advanced_activations_1.Softmax(args);
}
exports.softmax = softmax;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Advanced Activation',
 *   namespace: 'layers',
 *   useDocsFrom: 'ThresholdedReLU'
 * }
 */
function thresholdedReLU(args) {
    return new advanced_activations_1.ThresholdedReLU(args);
}
exports.thresholdedReLU = thresholdedReLU;
// Convolutional Layers.
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'Conv1D'
 * }
 */
function conv1d(args) {
    return new convolutional_1.Conv1D(args);
}
exports.conv1d = conv1d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'Conv2D'
 * }
 */
function conv2d(args) {
    return new convolutional_1.Conv2D(args);
}
exports.conv2d = conv2d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'Conv2DTranspose'
 * }
 */
function conv2dTranspose(args) {
    return new convolutional_1.Conv2DTranspose(args);
}
exports.conv2dTranspose = conv2dTranspose;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'Conv3D'
 * }
 */
function conv3d(args) {
    return new convolutional_1.Conv3D(args);
}
exports.conv3d = conv3d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'SeparableConv2D'
 * }
 */
function separableConv2d(args) {
    return new convolutional_1.SeparableConv2D(args);
}
exports.separableConv2d = separableConv2d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'Cropping2D'
 * }
 */
function cropping2D(args) {
    return new convolutional_1.Cropping2D(args);
}
exports.cropping2D = cropping2D;
/**
 * @doc{
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'UpSampling2D'
 * }
 */
function upSampling2d(args) {
    return new convolutional_1.UpSampling2D(args);
}
exports.upSampling2d = upSampling2d;
// Convolutional(depthwise) Layers.
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Convolutional',
 *   namespace: 'layers',
 *   useDocsFrom: 'DepthwiseConv2D'
 * }
 */
function depthwiseConv2d(args) {
    return new convolutional_depthwise_1.DepthwiseConv2D(args);
}
exports.depthwiseConv2d = depthwiseConv2d;
// Basic Layers.
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Activation'
 * }
 */
function activation(args) {
    return new core_1.Activation(args);
}
exports.activation = activation;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Dense'
 * }
 */
function dense(args) {
    return new core_1.Dense(args);
}
exports.dense = dense;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Dropout'
 * }
 */
function dropout(args) {
    return new core_1.Dropout(args);
}
exports.dropout = dropout;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Flatten'
 * }
 */
function flatten(args) {
    return new core_1.Flatten(args);
}
exports.flatten = flatten;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'RepeatVector'
 * }
 */
function repeatVector(args) {
    return new core_1.RepeatVector(args);
}
exports.repeatVector = repeatVector;
/**
 * @doc{
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Reshape'
 * }
 */
function reshape(args) {
    return new core_1.Reshape(args);
}
exports.reshape = reshape;
/**
 * @doc{
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *   useDocsFrom: 'Permute'
 * }
 */
function permute(args) {
    return new core_1.Permute(args);
}
exports.permute = permute;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Basic',
 *   namespace: 'layers',
 *    useDocsFrom: 'Embedding'
 * }
 */
function embedding(args) {
    return new embeddings_1.Embedding(args);
}
exports.embedding = embedding;
// Merge Layers.
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Add'
 * }
 */
function add(args) {
    return new merge_1.Add(args);
}
exports.add = add;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Average'
 * }
 */
function average(args) {
    return new merge_1.Average(args);
}
exports.average = average;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Concatenate'
 * }
 */
function concatenate(args) {
    return new merge_1.Concatenate(args);
}
exports.concatenate = concatenate;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Maximum'
 * }
 */
function maximum(args) {
    return new merge_1.Maximum(args);
}
exports.maximum = maximum;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Minimum'
 * }
 */
function minimum(args) {
    return new merge_1.Minimum(args);
}
exports.minimum = minimum;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Multiply'
 * }
 */
function multiply(args) {
    return new merge_1.Multiply(args);
}
exports.multiply = multiply;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Merge',
 *   namespace: 'layers',
 *   useDocsFrom: 'Dot'
 * }
 */
function dot(args) {
    return new merge_1.Dot(args);
}
exports.dot = dot;
// Normalization Layers.
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Normalization',
 *   namespace: 'layers',
 *   useDocsFrom: 'BatchNormalization'
 * }
 */
function batchNormalization(args) {
    return new normalization_1.BatchNormalization(args);
}
exports.batchNormalization = batchNormalization;
// Padding Layers.
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Padding',
 *   namespace: 'layers',
 *   useDocsFrom: 'ZeroPadding2D'
 * }
 */
function zeroPadding2d(args) {
    return new padding_1.ZeroPadding2D(args);
}
exports.zeroPadding2d = zeroPadding2d;
// Pooling Layers.
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'AveragePooling1D'
 * }
 */
function averagePooling1d(args) {
    return new pooling_1.AveragePooling1D(args);
}
exports.averagePooling1d = averagePooling1d;
function avgPool1d(args) {
    return averagePooling1d(args);
}
exports.avgPool1d = avgPool1d;
// For backwards compatibility.
// See https://github.com/tensorflow/tfjs/issues/152
function avgPooling1d(args) {
    return averagePooling1d(args);
}
exports.avgPooling1d = avgPooling1d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'AveragePooling2D'
 * }
 */
function averagePooling2d(args) {
    return new pooling_1.AveragePooling2D(args);
}
exports.averagePooling2d = averagePooling2d;
function avgPool2d(args) {
    return averagePooling2d(args);
}
exports.avgPool2d = avgPool2d;
// For backwards compatibility.
// See https://github.com/tensorflow/tfjs/issues/152
function avgPooling2d(args) {
    return averagePooling2d(args);
}
exports.avgPooling2d = avgPooling2d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'GlobalAveragePooling1D'
 * }
 */
function globalAveragePooling1d(args) {
    return new pooling_1.GlobalAveragePooling1D(args);
}
exports.globalAveragePooling1d = globalAveragePooling1d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'GlobalAveragePooling2D'
 * }
 */
function globalAveragePooling2d(args) {
    return new pooling_1.GlobalAveragePooling2D(args);
}
exports.globalAveragePooling2d = globalAveragePooling2d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'GlobalMaxPooling1D'
 * }
 */
function globalMaxPooling1d(args) {
    return new pooling_1.GlobalMaxPooling1D(args);
}
exports.globalMaxPooling1d = globalMaxPooling1d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'GlobalMaxPooling2D'
 * }
 */
function globalMaxPooling2d(args) {
    return new pooling_1.GlobalMaxPooling2D(args);
}
exports.globalMaxPooling2d = globalMaxPooling2d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'MaxPooling1D'
 * }
 */
function maxPooling1d(args) {
    return new pooling_1.MaxPooling1D(args);
}
exports.maxPooling1d = maxPooling1d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Pooling',
 *   namespace: 'layers',
 *   useDocsFrom: 'MaxPooling2D'
 * }
 */
function maxPooling2d(args) {
    return new pooling_1.MaxPooling2D(args);
}
exports.maxPooling2d = maxPooling2d;
// Recurrent Layers.
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'GRU'
 * }
 */
function gru(args) {
    return new recurrent_1.GRU(args);
}
exports.gru = gru;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'GRUCell'
 * }
 */
function gruCell(args) {
    return new recurrent_1.GRUCell(args);
}
exports.gruCell = gruCell;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'LSTM'
 * }
 */
function lstm(args) {
    return new recurrent_1.LSTM(args);
}
exports.lstm = lstm;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'LSTMCell'
 * }
 */
function lstmCell(args) {
    return new recurrent_1.LSTMCell(args);
}
exports.lstmCell = lstmCell;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'SimpleRNN'
 * }
 */
function simpleRNN(args) {
    return new recurrent_1.SimpleRNN(args);
}
exports.simpleRNN = simpleRNN;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'SimpleRNNCell'
 * }
 */
function simpleRNNCell(args) {
    return new recurrent_1.SimpleRNNCell(args);
}
exports.simpleRNNCell = simpleRNNCell;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'RNN'
 * }
 */
function rnn(args) {
    return new recurrent_1.RNN(args);
}
exports.rnn = rnn;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Recurrent',
 *   namespace: 'layers',
 *   useDocsFrom: 'RNN'
 * }
 */
function stackedRNNCells(args) {
    return new recurrent_1.StackedRNNCells(args);
}
exports.stackedRNNCells = stackedRNNCells;
// Wrapper Layers.
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Wrapper',
 *   namespace: 'layers',
 *   useDocsFrom: 'Bidirectional'
 * }
 */
function bidirectional(args) {
    return new wrappers_1.Bidirectional(args);
}
exports.bidirectional = bidirectional;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Wrapper',
 *   namespace: 'layers',
 *   useDocsFrom: 'TimeDistributed'
 * }
 */
function timeDistributed(args) {
    return new wrappers_1.TimeDistributed(args);
}
exports.timeDistributed = timeDistributed;
// Aliases for pooling.
exports.globalMaxPool1d = globalMaxPooling1d;
exports.globalMaxPool2d = globalMaxPooling2d;
exports.maxPool1d = maxPooling1d;
exports.maxPool2d = maxPooling2d;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Noise',
 *   namespace: 'layers',
 *   useDocsFrom: 'GaussianNoise'
 * }
 */
function gaussianNoise(args) {
    return new noise_1.GaussianNoise(args);
}
exports.gaussianNoise = gaussianNoise;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Noise',
 *   namespace: 'layers',
 *   useDocsFrom: 'GaussianDropout'
 * }
 */
function gaussianDropout(args) {
    return new noise_1.GaussianDropout(args);
}
exports.gaussianDropout = gaussianDropout;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Noise',
 *   namespace: 'layers',
 *   useDocsFrom: 'AlphaDropout'
 * }
 */
function alphaDropout(args) {
    return new noise_1.AlphaDropout(args);
}
exports.alphaDropout = alphaDropout;
/**
 * @doc {
 *   heading: 'Layers',
 *   subheading: 'Mask',
 *   namespace: 'layers',
 *   useDocsFrom: 'Masking'
 * }
 */
function masking(args) {
    return new core_1.Masking(args);
}
exports.masking = masking;
//# sourceMappingURL=exports_layers.js.map