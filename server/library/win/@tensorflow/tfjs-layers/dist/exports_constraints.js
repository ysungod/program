"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * =============================================================================
 */
// tslint:disable-next-line:max-line-length
var constraints_1 = require("./constraints");
/**
 * @doc {
 *   heading: 'Constraints',
 *   namespace: 'constraints',
 *   useDocsFrom: 'MaxNorm'
 * }
 */
function maxNorm(args) {
    return new constraints_1.MaxNorm(args);
}
exports.maxNorm = maxNorm;
/**
 * @doc {
 *   heading: 'Constraints',
 *   namespace: 'constraints',
 *   useDocsFrom: 'UnitNorm'
 * }
 */
function unitNorm(args) {
    return new constraints_1.UnitNorm(args);
}
exports.unitNorm = unitNorm;
/**
 * @doc {
 *   heading: 'Constraints',
 *   namespace: 'constraints',
 *   useDocsFrom: 'NonNeg'
 * }
 */
function nonNeg() {
    return new constraints_1.NonNeg();
}
exports.nonNeg = nonNeg;
/**
 * @doc {
 *   heading: 'Constraints',
 *   namespace: 'constraints',
 *   useDocsFrom: 'MinMaxNormConfig'
 * }
 */
function minMaxNorm(config) {
    return new constraints_1.MinMaxNorm(config);
}
exports.minMaxNorm = minMaxNorm;
//# sourceMappingURL=exports_constraints.js.map