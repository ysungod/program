/**
 * @license
 * Copyright 2018 Google LLC. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
import * as tensorflow from '../data/compiled_api';
import { Graph } from './types';
export declare class OperationMapper {
    private static _instance;
    private opMappers;
    static readonly Instance: OperationMapper;
    private constructor();
    private isControlFlow;
    private isDynamicShape;
    transformGraph(graph: tensorflow.IGraphDef): Graph;
    private mapNode;
    private decodeBase64;
    private getStringParam;
    private getBoolParam;
    private getNumberParam;
    private getDtypeParam;
    private getTensorShapeParam;
    private getNumericArrayParam;
}
