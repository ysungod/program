let data = null;
let api = null;
let file = null;
let ndcs = null;
let tensorflow = null;
let js = null;

try {data = require("./data/index.js");} catch(e) {console.log(e);}
try {api = require("./api/index.js");} catch(e) {console.log(e);}
try {file = require("./file/index.js");} catch(e) {console.log(e);}
try {ndcs = require("./node_case/index.js");} catch(e) {console.log(e);}
//try {tensorflow = require("./tensorflow/init.js");} catch(e) {console.log(e);}
try {js = require("./js/index.js");} catch(e) {console.log(e);}
try {flush = require("./flush.js");} catch(e) {console.log(e);}

 
module.exports = { 
  name: "@@模块系统: " + "单元测试" + "-----加载文件URL: " +　__filename,
  
 
  async run() {
    console.log(this.name); 

    //业务模型
    data.run();  
    api.run();
    file.run();
    ndcs.run();
    //tensorflow.run();  
    js.run(); 
    flush.run();
    this.init();   
  
  }, 
  //启动接口  
  async init() {
    //get请求
    router.get("/", async (ctx, next) => {
      let resultJSON = {state: 0, msg: "GET返回数据", dataList: [], data: {}};
      resultJSON.data["query"] = ctx.request.query;
      ctx.body = resultJSON; 
    }); 
    //post请求
    router.post("/test", async (ctx, next) => {
      let resultJSON = {state: 0, msg: "POST返回数据", dataList: [], data: {}};
      resultJSON.data["body"] = ctx.request.body;
      ctx.body = resultJSON; 
    });

    //触发事件
    router.get("/handle", async (ctx, next) => {
      let resultJSON = {state: 0, msg: "启动程序", dataList: [], data: {}};
      resultJSON.data["query"] = ctx.request.query;
      application.run();
      ctx.body = resultJSON; 
    });
  }


};