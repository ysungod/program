//let plugin = require("./plugin.js");


module.exports = {
  name: "ES6",


  async run() {
    console.log(this.name);

    //业务模型
    this.test();
  },

  async test() {
    class Point {
      constructor(data) {
        this.name = "Point类";
        this.data = data;
        console.log(data);
      }
      print() {
        console.log("绘制山水画");
      }
    }
    //new Point("私有变量");

class Line extends Point {
  constructor() {
    //console.log(this);  //ReferenceError: super()必须位于this之前，否则报错
    super("继承Point");  //继承Point

    console.log(this);  //Line { data: '继承Point' }
    console.log(this.name);  //Point类
    console.log(super.data);  //undefined
    console.log(super("传入参数").data);  //传入参数
    super.print();  //绘制山水画

  }
}

new Line();
    
  }


}