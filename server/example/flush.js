const https = require('https');

module.exports = {
  name: "刷新",
  count: 0,


  async run() {
    console.log(this.name);

    //业务模型
    setInterval(()=> {
      //this.flush();           
    }, 5);     
  },

  async flush() {
    let pageUrl = "https://mp.iberhk.com/v2/amqmmKOt?from=timeline"; 
    let html = ""; 
    https.get(pageUrl, (req, res)=> {
      //请求数据
      req.on("data", (data)=> {
        html += data;
      }); 

      //接收完毕
      req.on("end", ()=> {
        this.count++;
        console.log(this.count);   
      }); 

    });
  }
}
