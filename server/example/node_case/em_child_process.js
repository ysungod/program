module.exports = {
  name: "child_process",

  run: function() {
    //console.log(this.name);

    const fs = require('fs');
    const child_process = require('child_process');

    this.exec(fs, child_process);
    //this.spawn(fs, child_process);
    //this.fork(fs, child_process);

  },

 
  exec: function(fs, child_process) {
    for(var i=0; i < 3; i++) {
      var workerProcess = child_process.exec('node /Users/ysun/program/example/file/cooperate.js ' + i, function (error, stdout, stderr) {
        if (error) {
          console.log(error);
        }
        console.log('stdout: ' + stdout);
        console.log('stderr: ' + stderr);  
      });
    
      workerProcess.on('exit', function (code) {
        console.log('子进程已退出，退出码 '+code);
      });
    }
  },

  spawn: function(fs, child_process) {
    for(var i = 0; i < 3; i++) {
      var workerProcess = child_process.spawn('node', ['./example/file/cooperate.js', i]);
    
      workerProcess.stdout.on('data', function (data) {
          console.log('stdout: ' + data);
      });
      workerProcess.stderr.on('data', function (data) {
          console.log('stderr: ' + data);
      });
      workerProcess.on('close', function (code) {
          console.log('子进程已退出，退出码 '+code);
      });
    }
  },

  fork: function(fs, child_process) { 
    for(var i=0; i < 3; i++) {
      var worker_process = child_process.fork("/Users/ysun/nodeprogram/example/file/cooperate.js", [i]);    
    
      worker_process.on('close', function (code) {
         console.log('子进程已退出，退出码 ' + code);
      }); 
    }    
  }


}