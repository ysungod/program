global.mogo = require('mongoose');


module.exports = {
  name: "@@模块系统: " + "MongoDB数据库" + "-----加载文件URL: " +　__filename,


  async run() {
    console.log(this.name);

    //业务模型
    await this.connect();
     
  }, 

  async connect() {
    mogo.connect('mongodb://localhost/test', {useNewUrlParser: true});

    let db = mogo.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
      console.log("we're connected!");
    });
  }


}