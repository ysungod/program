const os = require("os");

try {global.tf = require('@tensorflow/tfjs-node');} catch(e) {console.log(e);}
 
  
module.exports = {  
  name: "TensorFlow机器学习平台", 

 
  async run() {
    console.log(`\r\r\n\n${this.name}\r\n`);

    //业务模型   
    //this.tensor();
    this.variable();
    //this.model();  

    console.log(`\r\n`);
  },
  //张量 tf.tensor()    
  async tensor() {
    let shape = [2, 3]; // 2 行, 3 列
    let a = tf.tensor([1.0, 2.0, 3.0, 10.0, 20.0, 30.0], shape); 

    a.print();  //打印张量值
  },
  //变量 tf.variable()
  async variable() { 
    let initialValues = tf.zeros([5]);
    let biases = tf.variable(initialValues); // 初始化biases

    biases.print(); // 输出: [0, 0, 0, 0, 0]
  },
  //模型
  async model() {
    const input = tf.input({shape: [5]});
    const denseLayer = tf.layers.dense({units: 1});
    const activationLayer = tf.layers.activation({activation: 'relu6'});

    // Obtain the output symbolic tensors by applying the layers in order.
    const denseOutput = denseLayer.apply(input);
    const activationOutput = activationLayer.apply(denseOutput);

    // Create the model based on the inputs.
    const model = tf.LayersModel({
        inputs: input,
        outputs: [denseOutput, activationOutput]
    }); 

    // Collect both outputs and print separately.
    const [denseOut, activationOut] = model.predict(tf.randomNormal([6, 5]));
    denseOut.print();
    activationOut.print();
  }


}