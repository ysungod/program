module.exports = {
  name: "@@模块系统: " + "配置文件" + "-----加载文件URL: " +　__filename,
  env: "debug",
  //env: "development",  //debug-单元测试  development-开发环境  test-测试环境  product-生产环境

 
  run: function() {
    console.log(this.name);

    try {
      //setMongoDB(); 
    } catch(e) {
      console.log(e);
    }
  }
};  
 
function setMongoDB() {
  console.log(`\r\n\r\n正在连接MongoDB...`);
  console.log(`...\r\n\r\n`);

  global.mogo = require("mongoose");
  global.Schema = mogo.Schema;
  const linkUrl = "mongodb://localhost:27017/program";
  const options = {useNewUrlParser: true, useCreateIndex: true, autoIndex: true, poolSize: 10};
  
  mogo.connect(linkUrl, options);

  let collect = mogo.connection; 
  collect.once("open", (res)=> { 
    console.log(`Connection is OK`); 
    console.log(`--------------------------------------------------------------------------------\r\n\n\n`); 
  }); 
  collect.on("error", (err)=> {
    console.log(err);
  });
  collect.on('disconnected', function() {
    console.log('Mongoose connection disconnected'); 
  });
} 