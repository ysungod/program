process.on('uncaughtException', (err)=> {
  console.log(`
***************************************************************************************
********************${new Date()} ${(new Date()).getMilliseconds()}********************

  uncaughtException: ${err}
*****
*

  `);
}); 


const domain = require("domain");
const domt = domain.create();

const EventEmitter = require("events").EventEmitter;


const Koa = require('koa');
const Router = require('koa-router');

global.app = new Koa();
global.router = new Router();

//跨域配置 
const convert = require('koa-convert');
const cors = require('koa-cors');
app.use(cors());   

//通信配置
const koaBody = require('koa-body');
app.use(koaBody());


//配置静态资源目录  
const static = require('koa-static');
app.use(static('./')); 

//路由配置
app.use(router.routes());





domt.on("error", (err)=> {
  console.log("domain异常: " + err);
});

//异常程序
domt.run(()=> {
  //公共函数
  try {
    global.plug = require("./plugin.js"); 
    plug.run(); 
  } catch(e) { 
    console.log(e);   
  } 

  //单元测试
  try {  
    let example = require('./example/init.js');
    example.run();
  } catch(e) { 
    console.log(e);    
  }  
});     
  


/*--------------------------------------------------*/
app.listen(7901);   
 
console.log(`
············································································· 
············································································· 
···············${new Date()} ${(new Date()).getMilliseconds()}···············
PORT: 7901
Server is Running...
····· 
·  

`); 