//异常处理
process.on('uncaughtException', (err)=> {
  console.log(`
***************************************************************************************
********************${new Date()} ${(new Date()).getMilliseconds()}********************

  uncaughtException: ${err}
*****
*

  `);
});  


const Koa = require('koa');
const Router = require('koa-router');

global.app = new Koa();
global.router = new Router();

//全局变量
try {
  global.plug = require("./plugin.js"); 
  plug.run();
} catch(e) {
  console.log(e); 
}
try {  
  const cfg = require("./config.js"); 
  cfg.run(); 
} catch(e) { 
  console.log(e); 
}

//socket通信机制
const server = require('http').createServer(app.callback());
global.io = require('socket.io')(server);
   

//跨域配置 
const convert = require('koa-convert');
const cors = require('koa-cors');
app.use(cors());    

//通信配置
const koaBody = require('koa-body');
app.use(koaBody());
 

//中间件过滤系统
app.use(async (ctx, next)=> {
  //初始化变量
  
  await next();
 
  //日志记录系统: logger日志记录


  //响应异常处理系统
  //GET: ctx.request.query
  //POST: ctx.request.body
  //console.log(ctx); 
}); 


//配置静态资源目录  
const static = require('koa-static');
app.use(static('./')); 

//路由配置
app.use(router.routes());


//API接口
try {
  let api = require("./api/init.js");
  api.run();
} catch(e) {
 console.log("程序异常: " + e); 
} 





app.on('error', (err, ctx) => {
  log.error('server error', err, ctx);
});

 
/*--------------------------------------------------*/
server.listen(8030);  

console.log(` 
············································································· 
············································································· 
···············${new Date()} ${(new Date()).getMilliseconds()}···············
PORT: 8030
Server is Running...
····· 
· 

`); 