const path = require("path");
const uuid = require("node-uuid");


module.exports = {
  name: "plugin",

  //配置路由
  root: path.join(__dirname, '../'),
  base: __dirname + "/",
  spiderPath: __dirname + "/application/spider/",

  regPhone: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\\d{8}$/,

  //初始化运行函数
  async run() { 
    console.log(this.root);  
  },  

  async sleep(time) {
    const sleep = (time) => new Promise((resolve)=> {
      setTimeout(resolve, time);
    });
    return sleep(time);
  },

  getImgName(url) {
    if (!url) {
      return "";
    }
    let index = url.lastIndexOf("/") + 1;
    let name = url.slice(index);
    let rst = name.match(/\.\w+/g)[0];
    let id = name.indexOf(rst);
    let result = name.slice(0, id + 4);
    
    return result;
  },
  getHTMLName(url) {
    if (!url) {
      return "";
    }
    let index = url.lastIndexOf("/") + 1;
    let name = url.slice(index);
    let rst = name.match(/\.\w+/g)[0];
    let id = name.indexOf(rst);
    let result = name.slice(0, id);
    
    return result;
  },

  //数组去重
  ergodic(array) { 
    let queue = [];

    for (let i = 0; i < array.length; i++) {
      let value = array[i];
      if (queue.indexOf(value) === -1) {
        queue.push(value); 
      }
    }
    return queue;
  },

  //2019.04.20 10:58:40 276
  formatDate(type, object) {
    let date = null; 
    if (object) { 
      date = object;
    } else {
      date = new Date();
    }

    let yr = date.getFullYear();
    let mh = date.getMonth() + 1; 
        mh = (mh > 9 ? mh : '0' + mh); 
    let dt = date.getDate();
        dt = (dt > 9 ? dt : '0' + dt);
    let hr = date.getHours();
        hr = (hr > 9 ? hr : '0' + hr);
    let mt = date.getMinutes();
        mt = (mt > 9 ? mt : '0' + mt);
    let sd = date.getSeconds();
        sd = (sd > 9 ? sd : '0' + sd); 
    let ms = date.getMilliseconds();  

    if (type && type == 'file') {
      return yr + "" + mh + ""+ dt + "-"+ hr;
    }
    if (type && type == 'minute') {
      return yr + "" + mh + ""+ dt + "-"+ hr + ""+ mt;
    }
    if (type && type == 'string') {
      return yr + "" + mh + ""+ dt + "-"+ hr + ""+ mt + ""+ sd + "-"+ ms;
    }
    return yr + "." + mh + "."+ dt + " "+ hr + ":"+ mt + ":"+ sd + " "+ ms; 
  }, 
  setDate(y, m, d, h, n, s) {
    let date = new Date();

    date.setYear(y);  //Max:   Min: 
    date.setMonth(m);  //Max: 12   Min: 1
    date.setDate(d);  //Max:   Min: 
    date.setHours(h);  //Max:   Min: 1  
    date.setMinutes(n);  //Max:   Min: 1  
    date.setSeconds(s);  //Max:   Min: 

    return date; 
  },


  //判断空对象plug.objectValue()
  objectValue(obj) {
    for (let i in obj) {
      return true;
    }
    return false;
  },

  //生成随机ID
  randomID() {
    let vid = uuid.v1();
    let id = vid.replace(/\-/g, "");
    
    return id;
  },

  //生成26个字母
  character() {
    let character = [];
    for(let i = 97; i < 123; i++) {
      character.push(String.fromCharCode(i));
    }
    return character.join("");   //abcdefghijklmnopqrstuvwxyz
  },

  //随机生成多位数字字符
  randomNumber(count) {
    let rand = "";  
    
    for (let i = 0; i < count; i++) {
      rand += Math.floor(Math.random() * 10);  
    }   
    return rand;
  },
  
  //获取远程资源
  async getRefs(url) {
    const https = require('https');
    const http = require('http');

    let htp = null;
    if (url.match(/^https/)) {
      htp = https;
    } else { 
      htp = http;
    }

    let html = "";
    htp.get(url, (req, res)=> {
      //请求数据
      req.on("data", (data)=> {
        html += data;
      }); 

      //接收完毕
      req.on("end", ()=> {
        
      }); 
      req.on("error", (err)=> {
        console.log();
      });
    });
  }

  
};