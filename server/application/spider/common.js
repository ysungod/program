const request = require("request");


module.exports = {
  name: "",


  async run() {},

  
  //下载图片
  async downloadImg(saveFilePath, srcLink, headers) { 
    let wsm = fs.createWriteStream(saveFilePath);

    let reqArgs = {url: srcLink};
    if (headers) {
      reqArgs['headers'] = headers;
    } 
    let rds = request(reqArgs);
    rds.pipe(wsm);

    rds.on('end', function() {
      console.log('下载图片已完成');
    }); 
    rds.on('error', (err)=> {
      console.log('下载图片失败了');
    });

    wsm.on('finish', ()=> {
      console.log('保存图片已成功');
      wsm.end(); 
    }); 
  },


  //保存数据
  async saveData(saveFilePath, saveData) {
    let callback = ()=> {
      return new Promise((resolve, reject)=> {
        //业务操作
        let wsm = fs.createWriteStream(saveFilePath);
        wsm.write(saveData,'UTF8');
        wsm.end();
        
        let pmDt = {label: "Server", state: 0, msg: "", data: {}, dataList: []};

        wsm.on('finish', ()=> {
          resolve(pmDt);
        });
        wsm.on('error', (err)=> {
          pmDt.state = 1;
          pmDt.msg = JSON.stringify(err);
          reject(pmDt); 
        });
      });
    }
    let cb = await callback();
    
    let stDt = {state: 0, msg: '提示消息', data: {}, dataList: []};
    if (cb.state == 0) {
      stDt.msg = "保存成功"; 
      console.log(stDt);
      return stDt;
    }
    stDt.state = 1;
    stDt.msg = "保存失败";
    console.log(stDt);
    return stDt;
  },


  //存储文件
  async store(fileUrl, data) {
    let callback = ()=> {
      return new Promise((resolve, reject)=> {
        //业务操作
        let wsm = fs.createWriteStream(fileUrl);
        wsm.write(data,'UTF8');
        wsm.end();
        
        let pmDt = {label: "Server", state: 0, msg: "提示消息", data: {}, dataList: []};

        wsm.on('finish', ()=> {
          resolve(pmDt);
        });
        wsm.on('error', (err)=> {
          pmDt.state = 1;
          pmDt.msg = JSON.stringify(err);
          reject(pmDt); 
        });
      });
    }

    let cb = await callback();
    return cb;    
  }

}