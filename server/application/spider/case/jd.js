const common = require("./../common.js");
common.run();


module.exports = {
  name: "@@模块系统: " + "京东数据" + "-----加载文件URL: " +　__filename,


  async run() {
    console.log(this.name);

    //业务模型
    //this.scroll();
  },

  async spider() {
    
  },

  async scroll() {
    let url = "./../data/jd/html/index.html";

    let loadingCount = 0;
    let loadingPage = setInterval(()=> {console.log(`加载页面资源进度. . . . . . ${loadingCount}`);loadingCount ++;}, 1000);
 
    await page.goto(url, {waitUntil: 'networkidle2'}); 
    await page.mainFrame().addScriptTag({
      url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'
    });
    await page.waitFor(1000); 
    clearInterval(loadingPage);


    console.log("开始解析页面数据");
    let parseCount = 0;
    let parsePage = setInterval(()=> {console.log(`解析页面数据进度_ _ _ _ _ _ ${parseCount}`);parseCount ++;}, 1000);


    await page.evaluate(async ()=> {
      var $ = window.$;
      var links = [];
    });  

    clearInterval(parsePage);

    await console.log("result[0]: " + JSON.stringify(result[0]));
    await console.log("result数组长度: " + result.length); 

    await page.waitFor(3000);

    let fileUrl = await plug.spiderPath + "data/jd/html/index.html";
    let result = await page.content();
    await common.saveData(fileUrl, result);

    //end 
    await browser.close();  

  },

  async index() {
    let url = "https://www.jd.com/";

    let loadingCount = 0;
    let loadingPage = setInterval(()=> {console.log(`加载页面资源进度. . . . . . ${loadingCount}`);loadingCount ++;}, 1000);
 
    await page.goto(url, {waitUntil: 'networkidle2'}); 
    await page.mainFrame().addScriptTag({
      url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'
    });
    await page.waitFor(2000); 
    clearInterval(loadingPage);


    console.log("开始解析页面数据");
    let parseCount = 0;
    let parsePage = setInterval(()=> {console.log(`解析页面数据进度_ _ _ _ _ _ ${parseCount}`);parseCount ++;}, 1000);

    let result = await this.indexJSON(page);

    clearInterval(parsePage);

    await console.log("result[0]: " + JSON.stringify(result[0]));
    await console.log("result数组长度: " + result.length); 

    await page.waitFor(3000);

    let clipPath = await plug.spiderPath + "data/jd/images/page/" + Math.random() + ".png";
    await page.screenshot({path: clipPath, fullPage: true});

    let fileUrl = await plug.spiderPath + "data/jd/json/" + result.length + "~~" + plug.formatDate('file') + ".json";
    await common.saveData(fileUrl, JSON.stringify(result));

    //end 
    await browser.close(); 
  },

  async indexJSON(page) {
    let result = await page.evaluate(async ()=> {
      var $ = window.$;
      var links = [];
      let height = null;

      await new Promise((resolve)=> {
        let scrollTop = 0;
        let distance = 300;

        let interval = window.setInterval(()=> {
          height = document.body.scrollHeight; 

          window.scrollBy(0, distance);
          scrollTop += distance;
          if (scrollTop >= height) {
            clearInterval(interval); 
            resolve();
          }
        }, 1000);
      });

      var items = $('li.more2_item');

      if (items.length >= 1) {
        items.each((index, item)=> {
          let it = $(item); 

          let title = it.find('a.more2_lk').attr('title');
          let detail = it.find('a.more2_lk').attr('href');
          let img = it.find("img.lazyimg_img").attr('src');
          let price = "￥" + it.find('span.more2_info_price_txt').text();
          links.push({title, detail, img, price});
        });
      }

      return links; 
    }); 

    return result;
  }


}