const common = require("./../common.js");
common.run();
const spider = require("./../spider.js");


module.exports = {
  name: "@@模块系统: " + "淘宝数据" + "-----加载文件URL: " +　__filename,


  async run() {
    console.log(this.name);

    //业务模型
    this.asyncgetHomePage();
  },
  
  //获取首页数据
  async asyncgetHomePage() {
    let pagePath = "https://www.taobao.com/";

    /*
     *@解析数据 
     **/
    let anatomyModel = async (page)=> {
      let result = await page.evaluate(async ()=> {
        let $ = window.$;

        let items = null;
        let backJSON = {type: 'array', msg: '提示消息', data: {}, dataList: []};
        
        //解析数据
        items = $('.item');
        if (items.length >= 1) {
          items.each((index, item)=> {
            let it = $(item);
            let node = item.nodeName.toLowerCase();

            let type = node;
            let name = null;
            let thumbnail = null;
            let links = null;
            let price = null;
            let pomo = null;
            let sales = null;
            let comment = null;
            let collect = null;
            
            //li-热卖单品
            if (node == 'li') {
              name = it.find('a.line-1').text();
              thumbnail = it.find('a.image').find('img').attr('src');
              links = it.find('a.image').attr('href');
              price = it.find('a.p4p-promo').find('em').text();
              pomo = it.find('a.price').find('em').text();
              sales = it.find('a.comment').find('em').text();
              comment = it.find('a.comment').find('em').text();
              collect = it.find('a.collect').find('em').text();
            }

            //div-猜你喜欢
            if (node == 'div') {
              name = it.find('h4').text();
              thumbnail = it.find('div.img-wrapper').find('img').attr('src');
              links = it.find('a.hotsale-item').attr('href');
              price = it.find('span.price').text(); 
              sales = it.find('span.sales').text();
            }
            if ((node == 'li' || node == 'div') && price) {
              backJSON.dataList.push({type, name, thumbnail, links, price, pomo, sales, comment, collect});
            }
            
          });  
        }

        return backJSON;
      });

      return result;
    }


    /*
     *@数据模型
     **/
    let resultModel = async (page, result)=> {
      let progCount = 0;
      let progress = await setInterval(()=> {console.log(`正在处理数据... | 当前进度为: ${progCount}`);progCount ++;}, 1000);


      //业务处理
      let dtList = result.dataList;

      //持久化JSON
      let saveFilePath = plug.spiderPath + "data/taobao/json/home-" + dtList.length + "~" + plug.formatDate('file') + ".json";
      await common.saveData(saveFilePath, JSON.stringify(dtList));


      await clearInterval(progress);
    } 


    //调取业务数据模型
    await spider.spider(pagePath, anatomyModel, resultModel);
  }

}