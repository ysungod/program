const common = require("./../common.js");
common.run();


module.exports = {
  name: "@@模块系统: " + "豌豆荚数据" + "-----加载文件URL: " +　__filename,


  async run() {
    console.log(this.name);

    //业务模型
    this.spider();
  },

  async spider() {
    let url = "https://www.wandoujia.com/category/5029";

    await page.goto(url, {waitUntil: 'networkidle2'}); 

    for (let i = 0; i < 2; i++) {
      await plug.sleep(3000);
      // 点击加载更多
      try {
        console.log(`当前进度: -----${i}`);

        //模拟用户操作
        await page.click(".load-more");
      } catch(e) {
        break;
      }
    }

    let result = await page.evaluate(()=> {
      var $ = window.$;
      var items = $('#j-tag-list li');
      var links = [];
 
      if (items.length >= 1) {
        items.each((index, item)=> {
          let it = $(item); 

          let name = it.find('a.name').text();
          let count = it.find('span.install-count').text();
          let size = it.find("div.meta").find("[title]").text();
          let comment = it.find('div.comment').text();
          let detail = it.find('a.detail-check-btn').attr('href');
          links.push({name, count, size, comment, detail});
        });
      }
      return links;
    }); 

    console.log("result[0]: " + JSON.stringify(result[0]));
    console.log("result数组长度: " + result.length);
 
    let fileUrl = await plug.spiderPath + "data/json/wandoujia/" + result.length + "~~" + plug.formatDate('string') + ".json";
    await common.saveData(fileUrl, JSON.stringify(result)); 
    
    //end 
    browser.close();  
  }


}