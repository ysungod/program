const common = require(plug.spiderPath + "common.js");


module.exports = {
  name: "@@模块系统: " + "美图录数据" + "-----加载文件URL: " +　__filename,


  async run() {
    console.log(this.name);

    //业务模型
    //this.spider();
    //this.thumbnail();
    //this.detail(); 
    //this.getDetail(); 
    this.getHomeDetail();
  }, 

  //爬取首页列表与相对详情页数据
  async getHomeDetail() {
    let progCount = 0;
    let progress = await setInterval(()=> {console.log(`正在加载首页... | 当前进度为:  ${progCount}`);progCount ++;}, 1000);

    let pageLink = "https://www.meitulu.com/";
    await page.goto(pageLink, {waitUntil: 'networkidle2'}); 

    //载入jQuery
    try {
      await page.mainFrame().addScriptTag({url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'});
    } catch(e) {
      console.log(JSON.stringify(e));
    }
    await page.waitFor(1500);
    clearInterval(progress); 
    console.log(`\r\n首页已经全部载入.\r\r\n\n`);


    console.log(`开始解析页面数据\r\n`);
    progCount = 0;
    progress = await setInterval(()=> {console.log(`正在解析页面数据... | 当前进度为:  ${progCount}`);progCount ++;}, 1000);

    let result = await page.evaluate(async ()=> {
      let $ = window.$;

      //下拉页面到底部
      await new Promise((resolve)=> {
        let scrollTop = 0;
        let distance = 300;

        let interval = window.setInterval(()=> {
          height = document.body.scrollHeight; 

          window.scrollBy(0, distance);
          scrollTop += distance;
          if (scrollTop >= height) {
            clearInterval(interval); 
            resolve();
          }
        }, 1000);
      });

      let items = null; 
      let backJSON = {type: 'array', msg: '提示消息', data: {}, dataList: []};

      //解析数据
      items = $("li");
      if (items.length >= 1) {
        items.each((index, item)=> {
          let it = $(item);   

          if (it.find('img').length == 0) {
            return ;
          }
          if (it.find('a.tags').length == 0) {
            return ;
          }

          let name = it.find('a.tags').first().text();
          let href = it.children('a:first-child').attr("href"); 
          let thumbnail = it.children('a:first-child').find("img").attr("src"); 
          let count = it.children('p:nth-of-type(1)').text().replace(/\D/g, '');  

          let tag = "";
          let tagArr = it.children('p:nth-of-type(3)').find('a.tags');
          tagArr.each((_index, _item)=> {
            tag += "、" + $(_item).text();
          });
          tag = tag.replace(/^、/, '');

          backJSON.dataList.push({name, href, thumbnail, count, tag});  
        }); 
      }

      return backJSON; 
    });  

    await clearInterval(progress); 
    console.log(`\r\n解析首页数据已完成.\r\r\n\n`);

    let rstJSON = result.dataList;
    console.log("result[0]: " + JSON.stringify(rstJSON[0])); 
    console.log("result数组长度: " + rstJSON.length);   
    await plug.sleep(1000);


    console.log(`\r\r\n\n\n开始处理数据\r\n`);
    progCount = 0;
    progress = await setInterval(()=> {console.log(`正在处理数据... | 当前进度为:  ${progCount}`);progCount ++;}, 1000);

    /*
     *@----------------------------------------------------
     *@处理数据业务模型 
     *@s输出结果
     *@----------------------------------------------------
     **/
    let saveFilePath = await plug.spiderPath + "data/meitulu/json/home-" + rstJSON.length + "-" + plug.formatDate('string') + ".json";
    await common.saveData(saveFilePath, JSON.stringify(rstJSON));

    await clearInterval(progress); 
    console.log(`\r\r\n\n处理数据已OK.\r\r\n\n\n\n\n`);

    await plug.sleep(1000);

    //遍历详情页
    console.log(`\r\r\n\n=======================================================================================\r\r\n\n`);
    console.log(`开始遍历详情页`);
    console.log(`\r\r\n\n=======================================================================================\r\r\n\n`);

    for (let i = 0; i < rstJSON.length; i++) {
      await plug.sleep(1000);
      await this.getDetail(rstJSON[i].href, page);
    }
    
    console.log(`\r\r\n\n=======================================================================================\r\r\n\n`);
    console.log(`结束遍历详情页`);
    console.log(`\r\r\n\n=======================================================================================\r\r\n\n`);


    //end 
    await page.close();
    await browser.close(); 

  },

  //重新爬取详情页
  async getDetail(pagePath, page) {
    let progCount = 0;
    let progress = await setInterval(()=> {console.log(`正在加载网页... | 当前进度为:  ${progCount}`);progCount ++;}, 1000);

    let pageLink = pagePath;
    await page.goto(pageLink, {waitUntil: 'networkidle2'}); 

    //载入jQuery
    try {
      await page.mainFrame().addScriptTag({url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'});
    } catch(e) {
      console.log(JSON.stringify(e));
    }
    await page.waitFor(1500);
    clearInterval(progress); 
    console.log(`\r\n网页已经全部载入.\r\r\n\n`);

    console.log(`开始解析页面数据\r\n`);
    progCount = 0;
    progress = await setInterval(()=> {console.log(`正在解析页面数据... | 当前进度为:  ${progCount}`);progCount ++;}, 1000);

    let result = await page.evaluate(async ()=> {
      let $ = window.$;

      //下拉页面到底部
      await new Promise((resolve)=> {
        let scrollTop = 0;
        let distance = 200;

        let interval = window.setInterval(()=> {
          height = document.body.scrollHeight; 

          window.scrollBy(0, distance);
          scrollTop += distance;
          if (scrollTop >= height) {
            clearInterval(interval); 
            resolve();
          }
        }, 1000);
      });

      let items = null; 
      let backJSON = {type: 'array', msg: '提示消息', data: {}, dataList: []};

      //解析数据
      backJSON.data['title'] = $('#weizhi').find('h1').text();
      backJSON.data['info'] = $("p.buchongshuoming").text();

      items = $('img.content_img');
      if (items.length >= 1) {
        items.each((index, item)=> {
          let it = $(item); 

          let name = it.attr('alt');
          let src = it.attr('src');

          backJSON.dataList.push({name, src});
        });
      }

      //轮询翻页兄弟页面
      let pageCount = $('#pages').children('a');
      if (pageCount.length < 2) {
        return ;
      }
      let pageArray = [];
      pageCount.each((index, item)=> {
        if (index == pageCount.length - 1) {
          return ;
        }
        let it = $(item);

        let href = it.attr('href');

        pageArray.push(href);
      });

      backJSON.data['page'] = pageArray;

      return backJSON;
    });

    clearInterval(progress); 
    console.log(`\r\n解析网页数据已完成.\r\r\n\n`);

    console.log("result[0]: " + JSON.stringify(result.dataList[0])); 
    console.log("result数组长度: " + result.dataList.length);   
    await plug.sleep(1000);

    let pageArray = result.data['page'];
    console.log("页面数组:" + pageArray);

    console.log(`\r\r\n\n\n开始处理数据\r\n`);

    //处理数据业务模型
    let dtList = result.dataList;
    let headers = {'Referer': 'https://www.meitulu.com/'};





    //继续处理翻页页面数据
    console.log(`\r\r\n\n=======================================================================================\r\r\n\n`);
    for (let i = 0; i < pageArray.length; i++) {
      await plug.sleep(1000);

      progCount = 0;
      progress = await setInterval(()=> {console.log(`正在加载网页... | 当前进度为:  ${progCount}`);progCount ++;}, 1000);

      let pagePath = "https://www.meitulu.com" + pageArray[i];
      console.log(`\r\n当前网页路径: ${pagePath} \r\n`);
      await page.goto(pagePath, {waitUntil: 'networkidle2'}); 

      //载入jQuery
      try {
        await page.mainFrame().addScriptTag({url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'});
      } catch(e) {
        console.log(JSON.stringify(e));
      }
      await page.waitFor(1500);
      clearInterval(progress); 
      console.log(`\r\n网页已经全部载入.\r\r\n\n`);

      console.log(`开始解析页面数据\r\n`);
      progCount = 0;
      progress = await setInterval(()=> {console.log(`正在解析页面数据... | 当前进度为:  ${progCount}`);progCount ++;}, 1000);

      result = null;
      result = await page.evaluate(async ()=> {
        let $ = window.$;
  
        //下拉页面到底部
        await new Promise((resolve)=> {
          let scrollTop = 0;
          let distance = 200;
  
          let interval = window.setInterval(()=> {
            height = document.body.scrollHeight; 
  
            window.scrollBy(0, distance);
            scrollTop += distance;
            if (scrollTop >= height) {
              clearInterval(interval); 
              resolve();
            }
          }, 1000);
        });
  
        let items = null; 
        let backJSON = {type: 'array', msg: '提示消息', data: {}, dataList: []};
  
        //解析数据
        backJSON.data['title'] = $('#weizhi').find('h1').text();
        backJSON.data['info'] = $("p.buchongshuoming").text();

        items = $('img.content_img');
        if (items.length >= 1) {
          items.each((index, item)=> {
            let it = $(item); 

            let name = it.attr('alt');
            let src = it.attr('src');

            backJSON.dataList.push({name, src});
          });
        }
  
        return backJSON;
      });
  
      await clearInterval(progress); 
      console.log(`\r\n解析网页数据已完成.\r\r\n\n`);
  
      console.log("result[0]: " + JSON.stringify(result.dataList[0])); 
      console.log("result数组长度: " + result.dataList.length);   
      await plug.sleep(1000);
  

     /*
      *@----------------------------------------------------
      *@处理数据业务模型
      *@s输出结果
      *@----------------------------------------------------
      **/
      console.log(`\r\r\n\n\n开始处理数据\r\n`);
      let dtList = result.dataList;

      //持久化JSON
      progCount = 0;
      progress = setInterval(()=> {console.log(`正在保存JSON... | 当前进度为:  ${progCount}`);progCount ++;}, 1000);

      let saveFilePath = await plug.spiderPath + "data/meitulu/json/" + plug.getHTMLName(pagePath) + "-" + plug.formatDate('file') + ".json";
      await common.saveData(saveFilePath, JSON.stringify(dtList));

      clearInterval(progress); 
      console.log(`\r\n输出JSON数据已保存.\r\r\n\n`);

      progCount = 0;
      progress = setInterval(()=> {console.log(`正在下载图片... | 当前进度为:  ${progCount}`);progCount ++;}, 1000);

      for (let i = 0; i < dtList.length; i++) {  
        await plug.sleep(100);
        let saveFilePath = plug.spiderPath + "data/meitulu/images/" + plug.getHTMLName(pageLink) + "-" + plug.getImgName(dtList[i].src); 
        await common.downloadImg(saveFilePath, dtList[i].src, headers);
      } 

      clearInterval(progress); 
      console.log(`\r\n图片下载已完成.\r\r\n\n`);

    }





    console.log(`\r\r\n\n=======================================================================================\r\r\n\n`);
    console.log(`\r\r\n\n处理数据全部已OK.\r\r\n\n\n\n\n`);


    //end  
    //await page.close(); 
    //await browser.close();     
  },





  //爬取详情页
  async detail() {
    let progCount = 0;
    let progress = setInterval(()=> {console.log(`当前爬取进度...... ${progCount}`);progCount ++;}, 1000);

    let url = "https://www.meitulu.com/";
    await page.goto(url, {waitUntil: 'networkidle2'}); 

    await page.mainFrame().addScriptTag({url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'});
    await page.waitFor(1000); 
    
    let result = await page.evaluate(async ()=> {
      let $ = window.$;

      //下拉页面到底部
      await new Promise((resolve)=> {
        let scrollTop = 0;
        let distance = 200;

        let interval = window.setInterval(()=> {
          height = document.body.scrollHeight; 

          window.scrollBy(0, distance);
          scrollTop += distance;
          if (scrollTop >= height) {
            clearInterval(interval); 
            resolve();
          }
        }, 1000);
      });

      let items = null; 
      let links = {type: 'array', msg: '', data: {}, dataList: []};

      //解析数据
      items = $("li");
      if (items.length >= 1) {
        items.each((index, item)=> {
          let it = $(item);   

          if (it.find('img').length == 0) {
            return ;
          }
          if (it.find('a.tags').length == 0) {
            return ;
          }

          let name = it.find('a.tags').first().text();
          let href = it.children('a:first-child').attr("href"); 
          let thumbnail = it.children('a:first-child').find("img").attr("src"); 
          let count = it.children('p:nth-of-type(1)').text().replace(/\D/g, '');  

          let tag = "";
          let tagArr = it.children('p:nth-of-type(3)').find('a.tags');
          tagArr.each((_index, _item)=> {
            tag += "、" + $(_item).text();
          });
          tag = tag.replace(/^、/, '');

          links.dataList.push({name, href, thumbnail, count, tag});  
        }); 
      }

      return links; 
    });  

    clearInterval(progress); 

    console.log("result[0]: " + JSON.stringify(result.dataList[0])); 
    console.log("result数组长度: " + result.dataList.length);   
    await plug.sleep(1000);

    //爬取详情页
    console.log(`爬取详情页`);

    let arr = result.dataList;
    for (let i = 0; i< arr.length; i++) {
      progCount = 0;
      progress = await setInterval(()=> {console.log(`当前爬取进度...... ${progCount}`);progCount ++;}, 1000);
      await plug.sleep(1000);

      let _url = arr[i].href;

      await page.goto(_url, {waitUntil: 'networkidle2'}); 
      await page.mainFrame().addScriptTag({
        url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'
      });
      await page.waitFor(1000); 

      let _result = await page.evaluate(async ()=> {
        let $ = window.$;

        //下拉页面到底部
        await new Promise((resolve)=> {
          let scrollTop = 0;
          let distance = 300;

          let interval = window.setInterval(()=> {
            height = document.body.scrollHeight; 

            window.scrollBy(0, distance); 
            scrollTop += distance;
            if (scrollTop >= height) {
              clearInterval(interval); 
              resolve();
            }
          }, 1000);
        });


        let items = null;
        let links = {state: 0, msg: '', data: {}, dataList: []};

        //解析数据
        links.data['title'] = $('#weizhi').find('h1').text();
        links.data['info'] = $("p.buchongshuoming").text();

        items = $('img.content_img');
        if (items.length >= 1) {
          items.each((index, item)=> {
            let it = $(item); 

            let name = it.attr('alt');
            let src = it.attr('src');

            links.dataList.push({name, src});
          });
        }
        return links;

      }); 

      let _arr = _result.dataList;
      console.log("result[0]: " + JSON.stringify(_arr[0]));
      console.log("result数组长度: " + _arr.length);  

      //持久化JSON
      //let fileUrl = await plug.spiderPath + "data/meitulu/json/detail-" + plug.getHTMLName(arr[i].href) + "~" + _arr.length + "~" + plug.formatDate('string') + ".json";
      //await common.saveData(fileUrl, JSON.stringify(_result));

      await clearInterval(progress);
      
      //下载
      let downCount = 0;
      let downProgress = await setInterval(()=> {console.log(`下载图片进度...... ${downCount}`);downCount ++;}, 1000);
      let headers = {
        'Referer': 'https://www.meitulu.com/'
      }
      for (let i = 0; i < _arr.length; i++) {  
        await plug.sleep(100);
        let _url = plug.spiderPath + "data/meitulu/images/detail-" + plug.getHTMLName(result.dataList[i].href) + "-" + plug.formatDate('string') + "-" + plug.getImgName(_arr[i].src); 
        await common.downloadImg(_url, _arr[i].src, headers);
      }  

      console.log(`\r\r\n\n下载完成。`);
      await clearInterval(downProgress);
 

      //end 
      //await page.close();
    }

    console.log(`\r\r\n\n 爬取已经完成`);


    await browser.close();
  },





  //下载缩略图
  async thumbnail() {
    let progCount = 0;
    let progress = setInterval(()=> {console.log(`当前爬取进度...... ${progCount}`);progCount ++;}, 1000);

    let url = "https://www.meitulu.com/";
    await page.goto(url, {waitUntil: 'networkidle2'}); 

    await page.mainFrame().addScriptTag({
      url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'
    });
    await page.waitFor(1000); 

    let result = await page.evaluate(async ()=> {
      let $ = window.$;

      //下拉页面到底部
      await new Promise((resolve)=> {
        let scrollTop = 0;
        let distance = 300;

        let interval = window.setInterval(()=> {
          height = document.body.scrollHeight; 

          window.scrollBy(0, distance);
          scrollTop += distance;
          if (scrollTop >= height) {
            clearInterval(interval); 
            resolve();
          }
        }, 1000);
      });

      let items = null; 
      let links = {type: 'array', msg: '', data: {}, dataList: []};

      //解析数据
      items = $("li");
      if (items.length >= 1) {
        items.each((index, item)=> {
          let it = $(item);   

          if (it.find('img').length == 0) {
            return ;
          }
          if (it.find('a.tags').length == 0) {
            return ;
          }

          let name = it.find('a.tags').first().text();
          let href = it.children('a:first-child').attr("href"); 
          let thumbnail = it.children('a:first-child').find("img").attr("src"); 
          let count = it.children('p:nth-of-type(1)').text().replace(/\D/g, '');  

          let tag = "";
          let tagArr = it.children('p:nth-of-type(3)').find('a.tags');
          tagArr.each((_index, _item)=> {
            tag += "、" + $(_item).text();
          });
          tag = tag.replace(/^、/, '');

          links.dataList.push({name, href, thumbnail, count, tag});  
        }); 
      }

      return links; 
    });  

    clearInterval(progress);  

    console.log("result[0]: " + JSON.stringify(result.dataList[0])); 
    console.log("result数组长度: " + result.dataList.length); 

    //下载
    let downCount = 0;
    let downProgress = await setInterval(()=> {console.log(`下载图片进度...... ${downCount}`);downCount ++;}, 1000);
    let headers = {'Referer': 'https://www.meitulu.com/'};
    let arr = result.dataList;
    for (let i = 0; i < arr.length; i++) {  
      await plug.sleep(100);
      let _url = plug.spiderPath + "data/meitulu/thumbnail/" + plug.getHTMLName(arr[i].href) + plug.getImgName(arr[i].thumbnail); 
      await common.downloadImg(_url, arr[i].thumbnail, headers);
    }  

    console.log(`\r\r\n\n下载完成。`);
    clearInterval(downProgress);

    //end 
    await page.close();
    await browser.close(); 

  },





  async spider() {
    let progCount = 0;
    let progress = setInterval(()=> {console.log(`当前爬取进度...... ${progCount}`);progCount ++;}, 1000);

    let url = "https://www.meitulu.com/";
    await page.goto(url, {waitUntil: 'networkidle2'}); 

    await page.mainFrame().addScriptTag({
      url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'
    });
    await page.waitFor(1000); 
    
    let result = await page.evaluate(async ()=> {
      let $ = window.$;

      //下拉页面到底部
      await new Promise((resolve)=> {
        let scrollTop = 0;
        let distance = 300;

        let interval = window.setInterval(()=> {
          height = document.body.scrollHeight; 

          window.scrollBy(0, distance);
          scrollTop += distance;
          if (scrollTop >= height) {
            clearInterval(interval); 
            resolve();
          }
        }, 1000);
      });

      let items = null; 
      let links = {type: 'array', msg: '', data: {}, dataList: []};

      //解析数据
      items = $("li");
      if (items.length >= 1) {
        items.each((index, item)=> {
          let it = $(item);   

          if (it.find('img').length == 0) {
            return ;
          }
          if (it.find('a.tags').length == 0) {
            return ;
          }

          let name = it.find('a.tags').first().text();
          let href = it.children('a:first-child').attr("href"); 
          let thumbnail = it.children('a:first-child').find("img").attr("src"); 
          let count = it.children('p:nth-of-type(1)').text().replace(/\D/g, '');  

          let tag = "";
          let tagArr = it.children('p:nth-of-type(3)').find('a.tags');
          tagArr.each((_index, _item)=> {
            tag += "、" + $(_item).text();
          });
          tag = tag.replace(/^、/, '');

          links.dataList.push({name, href, thumbnail, count, tag});  
        }); 
      }

      return links; 
    });  

    clearInterval(progress); 

    console.log("result[0]: " + JSON.stringify(result.dataList[0])); 
    console.log("result数组长度: " + result.dataList.length);   

    let fileUrl = await plug.spiderPath + "data/meitulu/json/" + result.dataList.length + "~" + plug.formatDate('file') + ".json";
    await common.saveData(fileUrl, JSON.stringify(result.dataList));

    await plug.sleep(1000);

    //爬取详情页
    console.log(`爬取详情页`);
    let arr = result.dataList;
    for (let i = 0; i< arr.length; i++) {
      await plug.sleep(1000); 

      let _url = arr[i].href;
      let _name = _url.slice(_url.lastIndexOf("/") + 1);

      await page.goto(_url, {waitUntil: 'networkidle2'}); 
      await page.mainFrame().addScriptTag({
        url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'
      });
      await page.waitFor(1000); 

      let _result = await page.evaluate(async ()=> {

      });
      let fileUrl = await plug.spiderPath + "data/meitulu/html/" + _name;
      let document = await page.content();
  
      await common.saveData(fileUrl, document);

      await page.close();

    } 


    await browser.close();

  }


}