

module.exports = {
  name: "爬虫模型",


  async run() {},


  //公共爬虫业务模型
  async spider(pagePath, anatomyModel, resultModel) {
    let progCount = 0;
    let progress = setInterval(()=> {console.log(`正在加载网页... | 当前进度为:  ${progCount}`);progCount ++;}, 1000);

    await page.goto(pagePath, {waitUntil: 'networkidle2'}); 

    //载入jQuery
    try {
      await page.mainFrame().addScriptTag({url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'});
    } catch(e) {
      console.log(JSON.stringify(e));
    }
    await page.waitFor(1500);
    clearInterval(progress); 
    console.log(`\r\n网页已经全部载入.\r\r\n\n`);

    console.log(`开始解析页面数据\r\n`);
    progCount = 0;
    progress = setInterval(()=> {console.log(`正在解析页面数据... | 当前进度为:  ${progCount}`);progCount ++;}, 1000);


    //下拉页面到底部
    await page.evaluate(async ()=> {
      await new Promise((resolve)=> {
        let scrollTop = 0;
        let distance = 250;

        let interval = window.setInterval(()=> {
          height = document.body.scrollHeight; 

          window.scrollBy(0, distance);
          scrollTop += distance;
          if (scrollTop >= height) {
            clearInterval(interval); 
            resolve();
          }
        }, 1000);
      });
    });

    await plug.sleep(1000);

    //业务模型
    let result = await anatomyModel(page);

    await plug.sleep(1000);
    await clearInterval(progress); 
    console.log(`\r\n解析网页数据已完成.\r\r\n\n`);

    console.log("result[0]: " + JSON.stringify(result.dataList[0])); 
    console.log("result数组长度: " + result.dataList.length);   


    /*
     *@----------------------------------------------------
     *@处理数据业务模型
     *@输出结果
     *@----------------------------------------------------
     **/ 
    let dtList = result.dataList;
    console.log(`\r\r\n\n\n开始处理数据\r\n`);


    console.log(`\r\r\n\n=======================================================================================\r\r\n\n`);

    //业务模型
    await resultModel(page, result);

    console.log(`\r\r\n\n=======================================================================================\r\r\n\n`);


    await plug.sleep(1000); 
    console.log(`\r\r\n\n处理数据已全部OK.\r\r\n\n\n\n\n`);


    //end 
    //await page.close();
    //await browser.close(); 

  }
}