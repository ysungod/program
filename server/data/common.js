module.exports = {
  name: "@@模块系统: " + "API接口异步调用公共函数" + "-----加载文件URL: " +　__filename,


  run() {
    console.log(this.name);

    //业务模型
  },



  //查询数据
  findData(md, condition) {
    console.log(`\r\n## 查询数据...`);
    let self = this; 
    let callback = new Promise((resolve, reject)=> {
      md.collection.findOne(condition, (err, result)=> {
        self.svrRst(resolve, reject, err, result); 
      }); 
    }); 
    return callback;
  }, 

  //修改数据、根据条件插入数据
  updateData(md, condition, doc) {
    console.log(`\r\n## 修改数据...`);
    let self = this;
    let callback = new Promise((resolve, reject)=> {
      md.collection.updateOne(condition, doc, (err, result)=> {
        self.svrRst(resolve, reject, err, result);
      });    
    });
    return callback;
  },

  //插入数据
  insert(md, doc) {
    console.log(`\r\n## 插入数据...`);
    let self = this;
    let callback = new Promise((resolve, reject)=> {
      md.collection.insertOne(doc, (err, result)=> {
        self.svrRst(resolve, reject, err, result);            
      }); 
    }); 
    return callback;
  },




  /* 
   *@通信数据JSON
   *@业务组件通信
   *@API通信 
   */

  //Server数据JSON
  svrRst(resolve, reject, err, result) {
    if (err) {
      console.log(err); 
      let pmDt = {label: "Server", state: -1, msg: JSON.stringify(err), result: null};
      reject(pmDt); 
      return;  
    }
    console.log(`\r\n--------------------------------------------------------------------------------`);
    console.log("## 服务器请求数据库已经返回JSON");  
    console.log(`## collection...(err, result)=>{}函数内容:\r\n${JSON.stringify(result)}`); 
    console.log(`-----\r\n`); 

    let pmDt = {label: "Server", state: 0, msg: JSON.stringify(result), result: result};
    if (!result) { 
      pmDt.state = 1; 
    }    
    resolve(pmDt);
  },

  //Client数据JSON
  cltRst(cb, cbDt, args) {
    if (cb.state == 0) { 
      cbDt.msg = args.success;
    }
    if (cb.state == 1) {
      cbDt.state = 1;
      cbDt.msg = args.exception;
    }
    if (cb.state == -1) { 
      cbDt.state = 1;
      cbDt.msg = args.err;
    }
    console.log(`\r\n--------------------------------------------------------------------------------`);
    console.log(`## 返回API数据结构`);
    console.log(`## \r\n${JSON.stringify(cbDt)}`);  
    return cbDt; 
  }


}