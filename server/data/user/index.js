const model = require(plug.base + "data/user/model.js");
const common = require(plug.base + "data/common.js");


module.exports = {
  name: "@@模块系统: " + "用户数据管理" + "-----加载文件URL: " +　__filename,


  run: function() {
    console.log(this.name);

    //业务模型 
  }, 


  //提交注册
  async register(args) {
    let userMD = model.userInfo; 
    let regInfoMD =  model.regInfo;

    let query = {token: args.token};
    let fd = await common.findData(regInfoMD, query);

    let cbDt = {label: "client", state: 0, msg: "提示消息", data: {}, dataList: []};
    if (fd.state == 0) { 
      let doc = {token: plug.randomID(), phone: fd.result['phone'], password: args.password, regDate: new Date()};
      let cb = common.insert(userMD, doc);
      return common.cltRst(fd, cbDt, {success: "OK", except: "该注册信息不存在", err: "系统服务异常"});
    }
    if (fd.state == 1) {
      return common.cltRst(fd, cbDt, {success: "OK", except: "该注册信息不存在", err: "系统服务异常"});
    }
    return common.cltRst(fd, cbDt, {success: "OK", except: "该注册信息不存在", err: "系统服务异常"});

  },

  //注册-下一步 
  async regToken(args) {
    let userMD = model.userInfo; 
    let regInfoMD =  model.regInfo;
    
    //生成token校验注册信息 
    console.log("生成token校验注册信息");
    let token = {token: plug.randomID()};
    let update = {$set: token};

    let cbDt = {label: "client", state: 0, msg: "提示消息", data: {}, dataList: []};
    //处于为用户手机号保密，因此只有注册者输入手机号，同时接收到验证码后, 才对用户手机号是否已经注册进行验证
    let pd = await common.findData(userMD, {phone: args.phone});
    if (pd.state == 0) { 
      cbDt.state = 1;
      cbDt.msg = "此手机号已经注册";
      return cbDt;
    }  

    //查询手机号与验证码
    let query = args;
    let fd = await common.findData(regInfoMD, query);

    //OK
    if (fd.state == 0) { 
      let ud = await common.updateData(regInfoMD, query, update);  
      cbDt.data = token; 
      return common.cltRst(ud, cbDt, {success: "OK", except: "手机号与验证码不匹配", err: "系统服务异常"});
    } 
    if (fd.state == 1) {
      return common.cltRst(fd, cbDt, {success: "OK", except: "手机号与验证码不匹配", err: "系统服务异常"});
    }
    return common.cltRst(fd, cbDt, {success: "OK", except: "手机号与验证码不匹配", err: "系统服务异常"});

  },

  //注册-验证码
  async regCode(args) {
    let regInfoMD =  model.regInfo;

    //发送验证码
    let regCode = plug.randomNumber(4);
    let doc = {phone: args.phone, code: regCode};
    //查询手机号是否存在
    let fd = await common.findData(regInfoMD, {phone: args.phone});
    
    let cbDt = {label: "client", state: 0, msg: "提示消息", data: {code: regCode}, dataList: []};

    //已存在手机号, 仅保存验证码
    if (fd.state == 0) {
      let update = {$set: {code: regCode}}; 
      let ud = await common.updateData(regInfoMD, {phone: args.phone}, update);
      return common.cltRst(ud, cbDt, {success: "OK", exception: "业务出现异常", err: "系统出现错误"}); 
    }  
    //保存手机号与验证码
    if (fd.state == 1) {
      let cb = await common.insert(regInfoMD, doc);
      return common.cltRst(cb, cbDt, {success: "OK", exception: "业务出现异常", err: "系统出现错误"});  
    }
    //服务异常
    return common.cltRst(fd, cbDt, {success: "OK", exception: "业务出现异常", err: "系统出现错误"});  

  },

  //登录
  async login(args) {
    let userMD =  model.userInfo;

    let doc = {phone: args.phone, password: args.password};
    let fd = await common.findData(userMD, doc);

    let cbDt = {label: "client", state: 0, msg: "提示消息", data: {}, dataList: []};
    if (fd.state == 0) {
      console.log("生成token实现登录");
      let token = {token: plug.randomID()};
      let update = {$set: token};
      let ud = await common.updateData(userMD, {phone: args.phone}, update);    
      cbDt.data = token; 
      return common.cltRst(ud, cbDt, {success: "OK", except: "手机号与验证码不匹配", err: "系统服务异常"});
    }
    
    return common.cltRst(fd, cbDt, {success: "登录成功", except: "系统服务异常", err: "当前用户尚未注册"}); 

  }

}