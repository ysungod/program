module.exports = {
  name: "@@模块系统: " + "MongoDB数据模型" + "-----加载文件URL: " +　__filename,


  //用户模型
  userInfo: mogo.model("userMD", new Schema({
    token: String,
    account: String,
    password: String,
    phone: String,
    regDate: Date,
    updatePs: Number 
  }, {collection: 'user'})),

  //Token  
  token: mogo.model("tokenMD", new Schema({
    id: String,
    startDate: Date,  
    validDate: Number,
    account: String  
  }, {collection: 'token'})),
  
  //注册信息  
  regInfo: mogo.model("regInfoMD", new Schema({
    phone: String,
    code: String,
    account: String,
    password: String 
  }, {collection: 'reg_info'})),

  //Article
  article: mogo.model("articleMD", new Schema({
    title:  String,
    author: String,
    body:   String,
    comments: [{ body: String, date: Date }],
    date: { type: Date, default: Date.now },
    hidden: Boolean,
    meta: {
      votes: Number,
      favs:  Number
    }
  }, {collection: 'article'}))


}