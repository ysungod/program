module.exports = {
  name: "@@模块系统: " + "MongoDB数据模型" + "-----加载文件URL: " +　__filename,

  //一对一 对话模型 type: "dialog"
  //群 对话模型    type: "group"
  //广播 对话模型   type: "broadcast"

  //一对一 对话模型
  dialog: mogo.model("dialogMd", new Schema(
    {
      type: String,
      target: String,
      id: String,
      content: String,  
      date: {   
        type: String, 
        default: plug.formatDate()
      }  
    }, {collection: "dialog"} 
  )),

  //群 对话模型
  group: mogo.model("groupMd", new Schema(
    {
      type: String,
      target: String,
      id: String,
      content: String,  
      date: {   
        type: String, 
        default: plug.formatDate()
      }    
    }, {collection: "group"} 
  )),

  //广播 对话模型
  broadcast: mogo.model("broadcastMd", new Schema(
    {
      type: String,
      target: String,
      id: String,
      content: String,  
      date: {   
        type: String,  
        default: plug.formatDate()
      }   
    }, {collection: "broadcast"} 
  ))

}