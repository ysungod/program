module.exports = {
  name: "socket.io通信机制",


  run: function() {
    console.log(this.name);

    //业务模型
    this.link();
  },

  getMsg: function(socket) {
    socket.on('ioMsg', (msg)=> {
      console.log('message: ' + msg);  
      io.emit('ioMsg', msg); 
    });
  },

  link: function() {
    io.on('connection', (socket)=> {
      console.log('an user connected');
      this.getMsg(socket);

      socket.on('disconnect', ()=> {
        console.log('user disconnected');
      });
    });
  }

}