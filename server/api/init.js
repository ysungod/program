let im = null;
let user = null; 

try {im = require(plug.base + "api/im/index.js");} catch(e) {console.log(e);}
try {user = require(plug.base + "api/user/index.js"); } catch(e) {console.log(e);}


module.exports = {
  name: "@@模块系统: " + "业务服务API" + "-----加载文件URL: " +　__filename,

  
  async run() {
    console.log(this.name);  

    //业务模型 
    im.run(); 
    user.run();
    this.init(); 
  },     
   
  //启动接口 
  async init() {
    //get请求
    router.get("/", async (ctx, next) => {
      let resultJSON = {state: 0, msg: "GET返回数据", dataList: [], data: {}};
      resultJSON.data["query"] = ctx.request.query;
      ctx.body = resultJSON; 
    }); 
    //post请求
    router.post("/test", async (ctx, next) => {
      let resultJSON = {state: 0, msg: "POST返回数据", dataList: [], data: {}};
      resultJSON.data["body"] = ctx.request.body;
      ctx.body = resultJSON; 
    });

    //触发事件
    router.get("/handle", async (ctx, next) => {
      let resultJSON = {state: 0, msg: "启动程序", dataList: [], data: {}};
      resultJSON.data["query"] = ctx.request.query;
      application.run();
      ctx.body = resultJSON; 
    });
  }
};