module.exports = {
  name: "API公共函数",


  router: function(path, dataObject) {
    router.post(path, async (ctx, next)=> {
      let reqDt = ctx.request.body; 

      console.log(`********************************************************************************`);
      console.log(`********************************************************************************`);
      console.log(`********************************************************************************`);
      console.log(`## API路由开始服务, 请注意监控日常日志\r\n`);
      console.log(`## 客户端传值数据为... \r\n\r\n${JSON.stringify(reqDt)}`); 
      console.log(`-----`);
      console.log(`-\r\n\r\n`); 

      //响应数据
      console.log(`--------------------------------------------------------------------------------`);
      console.log(`\r\n## 开始运行业务模型...`);
      console.log(`...`);
      console.log(`...`);
      console.log(`##\r\n`); 

      let resDt = await dataObject(reqDt); 

      console.log(`\r\n\r\n`);
      console.log(`********************************************************************************`);
      console.log(`********************************************************************************`);
      console.log(`## 已经完成业务模型...`);
      console.log(`## 开始向客户端输出数据...`);
      console.log(`## OUTPUT...`);
      console.log(`## HOST Path: ${path}\r\n-----\r## Response data:\r\n##`); 
      console.log(`${JSON.stringify(resDt)}`);
      console.log(`\r\n \r\n\r\n\r\n`);
      ctx.body = resDt;    
    });     
  } 
}