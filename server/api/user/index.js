const user = require(plug.base + "data/user/index.js");
const common = require(plug.base + "api/common.js");


module.exports = {
  name: "@@模块系统: " + "用户管理系统" + "-----加载文件URL: " +　__filename,


  run: function() {
    console.log(this.name);

    //业务模型
    user.run();
    this.login();
    this.regCode(); 
    this.regToken();
    this.register();
  },


  //登录
  login: function() { 
    common.router("/api/login", user.login);  
  },   

  //注册-验证码
  regCode: function() {
    common.router("/api/getRegCode", user.regCode);
  },

  //注册-下一步
  regToken: function() {
    common.router("/api/getRegToken", user.regToken);
  },

  //注册提交
  register: function() {
    common.router("/api/register", user.register);
  },

  //找回密码-验证码
  forgetCode: function() { 
    //common.router("/api/forgetCode", user.regCode);
  },

  //找回密码-重置密码
  repeatPs: function() {
    //common.router("/api/findPs", user.regCode);
  }

}