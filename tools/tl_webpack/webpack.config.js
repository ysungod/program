module.exports = {
  devtool: 'eval-source-map',
  entry:  __dirname + "/js/main.js",
  output: {
    path: __dirname + "/dist",  //打包后文件存储位置
    filename: "app.js"  //打包文件后文件名
  },
  module: {
    rules: []
  },
  plugins: []
}