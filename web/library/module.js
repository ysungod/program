/*
 *@
  <ysun v-></ysun>
 **/
Vue.directive("", {
  inserted: function(el) {
    console.log(el);
  } 
}); 


/*
 *@名称：
  <com-ysun></com-ysun>
 **/
Vue.component("com-ysun", {
  template: `
    <div class="com-"></div>
  `,
  props: {
    params: {
      type: Object,
      default() {
        return {} 
      }
    }
  },
  data() {
    return {}
  },
  created() {},
  watch: {},
  methods: {}
});


/*
 *@名称：
  <com-ysun></com-ysun>
 **/
Vue.component('com-ysun', {
  template: `
  <transition class="fade">
    <dl v-if="value" class="com-mask-layer cmt-">
      <dt class="aux-mask" @touchmove.prevent.stop></dt>
      <dd></dd>
    </dl>
  </transition>
  `,
  props: {
    value: String
  },
  model: {
    prop: "value",
    event: "submit"   
  },
  data() {
    return {} 
  }, 
  created() {
    this.comVisual(this.value);
  },
  watch: {    
    'value': function(val) {
      this.comVisual(val);
    }
  },
  methods: {
    update() {
      this.$emit("submit", false);
    },
    close: function() {
      this.update();
    },    
    comVisual(state) {
      if (!state) { 
        this.close();
      }  
    }
  }
});


/*
 *@名称：模态框@从中心向四周展开
 <com-blast-></com-blast->
 **/
Vue.component("com-blast-", {
  template: `
    <transition name="fade">
      <dl v-if="value" class="com-mask-layer com-flex cmt-">
        <dt class="cmt-mask" @click="close" @touchmove.prevent.stop></dt>
        <dd class="com-blastpicker" :class="{comBlast: slide}">
          <slot></slot>
        </dd>
      </dl>
    </transition>
  `,
  props: {
    value: String
  },
  model: {
    prop: "value",
    event: "submit"   
  },
  data() {
    return {
      slide: false
    }
  },
  created() {
    this.comVisual(this.value);
  },
  watch: {    
    'value': function(val) {
      this.comVisual(val);
    }
  },
  methods: {
    update() {
      this.$emit("submit", false);
    },
    //关闭
    close: function() {
      this.slide = false;
      this.update(); 
    },
    //打开
    open: function() {
      window.setTimeout(()=> {
        this.slide = true;
      }, 50);
    },
    comVisual(state) {
      if (!state) { 
        this.close();
        return ; 
      } 
      this.open();
    }
  }
});


/*
 *@名称：模态框@从下向上
 <com-slip-></com-slip->
 **/
Vue.component("com-slip-", {
  template: `
    <transition name="fade">
      <dl v-if="value" class="com-mask-layer com-disx cmt-">
        <dt class="cmt-mask" @click="close"></dt>
        <dd class="com-slippicker" :class="{comSlip: slide}">
          <slot></slot>
        </dd>
      </dl>
    </transition>
  `,
  props: {
    value: String
  },
  model: {
    prop: "value",
    event: "submit"   
  },
  data() {
    return {
      slide: false
    }
  },
  created() {
    this.comVisual(this.value);
  },
  watch: {    
    'value': function(val) {
      this.comVisual(val);
    }
  },
  methods: {
    update() {
      this.$emit("submit", false);
    },
    //关闭
    close() {
      this.slide = false;
      this.update(); 
    },
    //打开
    open() {
      window.setTimeout(()=> {
        this.slide = true;
      }, 50);
    },
    comVisual(state) {
      if (!state) { 
        this.close();
        return ; 
      } 
      this.open();
    }
  }
});