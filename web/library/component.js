import Vue from 'vue/dist/vue.min.js';


/*
 *@组件
 *---------------------------------------------------------------------
 *---------------------------------------------------------------------
 *---------------------------------------------------------------------
 *---------------------------------------------------------------------
 *---------------------------------------------------------------------
 **/


/*
 *@名称: 消息框
 <com-alert :params="comAlertData"></com-alert>
 this.$root.comAlertData = {layer: 0, title: "", info: ""}
 **/
Vue.component("com-alert", {
  props: {
    params: {
      type: Object,
      default: ()=> {
        return {} 
      }
    }
  },
  template: `
    <transition name="fade">
      <dl v-if="show" class="com-mask-layer com-flex cmt-alert">
        <dt class="cmt-mask" @click="close" @touchmove.prevent.stop></dt>
        <dd class="com-blastpicker" :class="{comBlast: slide}">
          <h3>{{title}}</h3>
          <p>{{params.info}}</p>
          <button @click="close" type="button" class="aux">关闭</button>
        </dd>
      </dl>
    </transition>
  `,
  data: function() {
    return {
      show: false,
      slide: false,
      title: "消息提示"
    }
  },
  created: function() {
    this.comVisual(this.params.layer);
  },
  watch: {    
    'params.layer': function(val) {
      this.comVisual(val);
    }
  },
  methods: {
    //关闭
    close: function() {
      this.slide = false;
      window.setTimeout(()=> {
        this.show = false;
        this.$root.comAlertData.layer = 0;
      }, 50);
    },
    //打开
    open: function() {
      this.show = true;
      window.setTimeout(()=> {
        this.slide = true;
      }, 50);
    },
    comVisual: function(layer) {
      if (layer == 0) {
        this.close();
        return ; 
      } 
      if (plug.judgeValid(this.params.title)) {
        this.title = this.params.title;
      }
      if (plug.judgeValid(this.params.info)) {
        this.info = this.params.info;
      }
      this.open();
    }
  }
});


/*
 *@名称: 确认框
 <com-confirm :params="comConfirmData"></com-confirm>
 this.$root.comConfirmData = {layer: 0, title: "", info: ""}
 **/
Vue.component("com-confirm", {
  props: {
    params: {
      type: Object,
      default: ()=> {
        return {} 
      }
    }
  },
  template: `
    <transition name="fade">
      <dl v-if="show" class="com-mask-layer com-flex cmt-confirm">
        <dt class="cmt-mask" @click="close" @touchmove.prevent.stop></dt>
        <dd class="com-blastpicker" :class="{comBlast: slide}">
          <h3>{{title}}</h3>
          <p>{{params.info}}</p>
          <div class="com-flex">
            <button @click="cancel" type="button" class="aux">{{button[0]}}</button>
            <button @click="confirm" type="button" class="aux">{{button[1]}}</button>
          </div>
        </dd>
      </dl>
    </transition> 
  `,
  data: function() {
    return {
      show: false,
      slide: false,
      title: "操作确认",
      button: ["取消", "确认"]
    } 
  },
  created: function() {
    this.comVisual(this.params.layer);
  },
  watch: {    
    'params.layer': function(val) {
      this.comVisual(val);
    }
  },
  methods: {
    //关闭
    close: function() {
      this.slide = false;
      window.setTimeout(()=> {
        this.show = false;
        this.$root.comConfirmData.layer = 0;
      }, 50);
    },
    //打开
    open: function() {
      this.show = true;
      window.setTimeout(()=> {
        this.slide = true;
      }, 50);
    },
    comVisual: function(layer) {
      if (layer == 0) {
        this.close();
        return ; 
      } 
      if (plug.judgeValid(this.params.title)) {
        this.title = this.params.title;
      }
      if (plug.judgeValid(this.params.info)) {
        this.info = this.params.info;
      }
      if (plug.judgeValid(this.params.button) && (this.params.button instanceof Array)) {
        let arr = this.params.button;
        if (arr[0]) {
          this.button[0] = arr[0];
        }
        if (arr[1]) {
          this.button[1] = arr[1];
        }
      }
      this.open();
    },
    cancel: function() {
      if (this.params.cancel) {
        this.params.cancel();
        return ;
      }
      this.close();
    },
    confirm: function() { 
      if (this.params.confirm) {
        this.params.confirm(); 
        return ;
      }  
      this.close();
      this.$root.comToastData.layer = 1;
      this.$root.comToastData.info = "当前操作无效";
    }
  }
});


/*
 *@名称: 页面加载框
  <com-page-loading :params="comPageLoadingData"></com-page-loading>
  comPageLoadingData: {
    layer: 0, //0: 关闭  1: 加载  2: 错误
    info: ""
  }
  intercept: (msg)=> {
    this.$root.comPageLoadingData.layer = 2;
    if (plug.judgeValid(msg)) { 
      this.$root.comPageLoadingData.info = msg;
    }  
  } 
 **/
Vue.component('com-page-loading', {
  props: {
    params: {
      type: Object, 
      default: ()=> {
        return {} 
      }
    }
  },
  template: `
  <transition class="fade">
    <!--页面加载中-->
    <div v-if="show" class="com-screen com-page-loading">
      <div v-if="params.layer == 1" class="com-flexv com-screen page">
        <com-spinner type="bounce"></com-spinner>
        <p>{{info}}</p>
      </div>
      <!--错误页-->
      <div v-if="params.layer == 2" class="com-flexv com-screen error">
        <i class="iconfont">&#xe65c;</i>
        <p>{{info}}</p>
        <button type="button" @click="flush" class="aux aux-font">重新加载</button>
      </div>
    </div>
  </transition>
  `,
  data: function() {
    return {
      show: false,
      info: ""
    } 
  }, 
  created: function() {
    this.controlVisual(this.params.layer);
  },
  watch: {
    "params.layer": function(val) {
      this.controlVisual(val);
    } 
  },
  methods: { 
    open: function() { 
      this.show = true;  
    },
    close: function() {
      this.show = false; 
      this.$root.comPageLoadingData.layer = 0;
    },
    controlVisual: function(val) {
      if (val == 0) {
        this.close();
        return ;
      }
      if (val == 1) {
        this.info = "页面加载中";
      }
      if (val == 2) {
        this.info = "很抱歉, 页面数据异常";
      }
      if (plug.judgeValid(this.params.info)) {
        this.info = this.params.info;
      }
      this.open();
    },
    flush: function() {
      if (this.params.callback) {
        this.params.callback(this);
        return ;
      }
      window.location.reload();
    }
  }
});


/*
 *@名称: Toast框
  <com-toast :params="comToastData"></com-toast>
  this.$root.comToastData = {layer: 0, info: ""};
 **/
Vue.component("com-toast", {
  props: {
    params: {
      type: Object,
      default: ()=> {
        return {} 
      }
    }
  },
  template: `
  <transition class="fade">
    <dl v-if="show" class="com-mask-layer com-flex cmt-toast">
      <dt></dt>
      <dd class="com-flexv" :class="{'cmt-show': slide}">{{params.info}}</dd>
    </dl>
  </transition>
  `, 
  data () {
    return {
      show: false,
      slide: false
    }
  },
  created: function() {
    this.comVisual(this.params.layer);
  },
  watch: {    
    'params.layer': function(val) {
      this.comVisual(val);
    }
  },
  methods: {
    open: function(info) {
      this.show = true;
      this.info = plug.judgeValid(info) ? "消息提示" : info;
      window.setTimeout(()=> {
        this.slide = true;
      }, 50); 
      window.setTimeout(()=> {
        this.close();
      }, 3000); 
    },
    close: function() {
      this.slide = false;
      window.setTimeout(()=> {
        this.show = false;
        this.$root.comToastData.layer = 0;
      }, 50);
    },
    comVisual: function(layer) {
      if (layer == 0) {
        this.close();
        return ; 
      } 
      if (plug.judgeValid(this.params.info)) {
        this.info = this.params.info;
      }
      this.open();
    }
  }
});


/*
 *@名称: 加载数据框
  <com-loading :params="comLoadingData"></com-loading>
 **/
Vue.component('com-loading', {
  props: {
    params: {
      type: Object,
      default: ()=> {
        return {} 
      }
    }
  },
  template: `
  <transition class="fade">
    <dl v-if="show" class="com-mask-layer com-flex cmt-loading">
      <dt class="trans" @touchmove.prevent.stop></dt>
      <dd class="com-flexv" :class="{'cmt-show': slide}">
        <com-spinner></com-spinner>
        <p>{{info}}</p>
      </dd>
    </dl>
  </transition>
  `,
  data: function() {
    return {
      show: false,
      slide: false,
      info: "加载数据中"
    } 
  }, 
  created: function() {
    this.comVisual(this.params.layer);
  },
  watch: {    
    'params.layer': function(val) {
      this.comVisual(val);
    }
  },
  methods: {
    open: function() {
      this.show = true;
      window.setTimeout(()=> {
        this.slide = true;
      }, 50);
    },
    close: function() {
      this.slide = false;
      window.setTimeout(()=> {
        this.show = false;
      }, 50); 
    },    
    comVisual: function(layer) {
      if (layer == 0) {
        this.close();
        return ; 
      } 
      if (plug.judgeValid(this.params.info)) {
        this.info = this.params.info;
      }
      this.open();
    }
  }
});


/*
 *@名称: 业务操作框
  <com-progress :params="comProgressData"></com-progress>
  this.$root.comProgressData = {layer: 0, info: ""};
 **/
Vue.component('com-progress', {
  props: {
    params: {
      type: Object,
      default: ()=> {
        return {} 
      }
    }
  },
  template: `
  <transition class="fade">
    <dl v-if="show" class="com-mask-layer com-flex cmt-progress">
      <dt class="trans"></dt>
      <dd class="com-flex" :class="{'cmt-show': slide}">
        <com-spinner></com-spinner>
        <em>{{info}}</em>
      </dd>
    </dl>
  </transition>
  `, 
  data: function() {
    return {
      show: false,
      slide: false,
      info: "提交数据中"
    } 
  }, 
  created: function() {
    this.comVisual(this.params.layer);
  },
  watch: {    
    'params.layer': function(val) {
      this.comVisual(val);
    }
  },
  methods: {
    open: function(info) {
      if (plug.judgeValid(info)) {
        this.info = info;
      }
      this.show = true;
      window.setTimeout(()=> {
        this.slide = true;
      }, 50);
    },
    close: function() {
      this.slide = false;
      window.setTimeout(()=> {
        this.show = false;
      }, 50); 
    },
    comVisual: function(layer) {
      if (layer == 0) {
        this.close();
        return ;  
      } 
      if (plug.judgeValid(this.params.info)) {
        this.info = this.params.info;
      }
      this.open();
    }
  }
});


/*
 *@名称: 操作框
 <com-action-sheet :actions="actions" v-model="showSheet"></com-action-sheet>
 **/  
Vue.component("com-action-sheet", {
  template: `
    <transition name="fade">
      <dl v-if="value" class="com-mask-layer com-disx com-action-sheet">
        <dt class="cmt-mask" @click.stop="close"></dt>
        <dd class="com-slippicker" :class="{comSlip: slide}">
          <ul class="aux">
            <li class="com-active-bg" v-for="(dx,index) in actions" :key="index" @click="close(dx.method)">{{dx.name}}</li>
          </ul> 
          <button @click="close" class="aux com-active-bg" type="button">取消</button>
        </dd>
      </dl>
    </transition>
  `,
  props: {
    value: String,
    actions: { 
      type: Array,
      default() {
        return []; 
      } 
    }
  },
  model: {
    prop: "value",
    event: "submit"   
  },
  data() {
    return {
      slide: false
    }
  },
  created() {
    this.comVisual(this.value);
  },
  watch: {    
    'value': function(val) {
      this.comVisual(val);
    }
  },
  methods: {
    update() {
      this.$emit("submit", false);
    },
    //关闭
    close(fn) {
      this.slide = false;
      this.update(); 
      if (fn instanceof Function) {
        fn();
      }
    },
    //打开
    open() {
      window.setTimeout(()=> {
        this.slide = true;
      }, 50);
    },
    comVisual(state) {
      if (!state) { 
        this.close(); 
        return ;  
      } 
      this.open();
    }
  }
});

 
/*
 *@名称: 选择器 
  <com-picker index="0" :actions="pickerActions" title="Picker选择框" v-model="showPicker" @change="getPickerValue"></com-picker>
 **/
Vue.component('com-picker', { 
  template: ` 
    <transition name="fade">
      <dl v-if="value" class="aux com-mask-layer com-disx com-picker com-item-picker">
        <dt class="cmt-mask" @click.stop="close"></dt>
        <dd class="com-slippicker" :class="{comSlip: slide}">
          <ul class="com-disx"><li @click.stop="close" class="com-active-bg cancel">取消</li><li class="title">{{title}}</li><li class="com-active-bg confirm" @click.stop="confirm">确定</li></ul>
          <div class="picker-item" :style="{'max-height': length + 'rem'}" @touchstart.prevent.stop="startPos" @touchmove.prevent.stop="movePos" @touchend="endPos">
            <ul :style="transPos" class="aux picker-slots">
              <li v-for="(dx,index) in actions[0].array" :key="index" :class="{active: actions[0].active[index]}">{{dx}} {{actions[0].name}}</li> 
            </ul> 
            <div class="cmt-mask"></div>
            <h3></h3>
          </div>
        </dd>
      </dl>
    </transition>
  `,
  props: {
    value: String,
    index: {
      type: String,
      default() {return "0"}
    },
    actions: { 
      type: Array,
      default() {
        return []; 
      } 
    },
    title: {
      type: String,
      default() {
        return "";
      }
    }
  },
  model: {
    prop: "value",
    event: "submit"   
  },
  data() {
    return {
      slide: false,
      length: 5, 
      item: null,
      transPos: {},
      pos: {
        topY: null,
        bottomY: null, 
        startY: null,
        moveY: null,
        differY: null, 
        distance: null,
        distanceY: null
      }
    }
  },
  created() {
    this.init();
    this.comVisual(this.value);
  },
  watch: {    
    'value': function(val) {
      this.comVisual(val);
    }
  },
  mounted() {
    this.initPos();
  },
  methods: {
    startPos(e) {
      this.pos.startY = e.touches[0].pageY;
      this.pos.moveY = 0;
    },
    movePos(e) { 
      this.pos.moveY = e.touches[0].pageY;
      this.pos.differY = this.pos.moveY - this.pos.startY;

      this.pos.distance = this.pos.distanceY + this.pos.differY;
      this.transPos = {transform: "translateY(" + this.pos.distance + "px)"};
    },
    endPos() {
      if (this.pos.distance > this.pos.topY) {
        this.pos.distanceY = this.pos.topY;
      } else if (this.pos.distance < this.pos.bottomY) {
        this.pos.distanceY = this.pos.bottomY;
      } else {
        let time = Math.floor(this.pos.distance / font_size);
        let model = this.pos.distance % font_size;

        //回弹到正中位置 
        let renew = null; 

        if (model > font_size/2) {
          renew = (time + 1) * font_size;
        } else {
          renew = time * font_size;
        }  

        this.pos.distanceY = renew; 
      } 
 
      //重置this.actions[0].active
      this.item = Math.round((this.pos.topY - this.pos.distanceY) / font_size);

      this.actions[0].active.forEach((value, index)=> { 
        this.actions[0].active[index] = false;
      }); 
      this.actions[0].active[this.item] = true;

      //动画
      this.transPos = {transform: "translateY(" + this.pos.distanceY + "px)", 'transition-duration': '500ms'};

      //清除变量  
      this.pos.differY = null;
      this.pos.distance = null;
    },
    update() {
      this.$emit("submit", false);
    },
    //关闭
    close(fn) {
      this.slide = false;
      this.update(); 
      if (fn instanceof Function) {
        fn();
      }
    },
    //打开
    open() {
      window.setTimeout(()=> {
        this.slide = true;
      }, 50);
    },
    comVisual(state) {
      if (!state) { 
        this.close(); 
        return ;  
      }  
      this.open();
    }, 
    confirm() {
      this.$emit("change", {index: this.item, value: this.actions[0].array[this.item]});
      this.close();
    },
    initPos() { 
      this.pos.topY = font_size * (this.length - 1)/2;
      this.pos.bottomY = this.pos.topY - font_size * (this.actions[0].array.length - 1);

      this.pos.distanceY = font_size * ((this.length - 1)/2 - this.item);
      this.transPos = {transform: "translateY(" + this.pos.distanceY + "px)"};
    },
    //初始化数据 
    init() {
      let item = this.index;
      this.item = parseInt(item);
      //this.$emit("change", {index: item, value: this.actions[0].array[item]});

      this.actions[0].active = [];
      this.actions.forEach((value, index)=> {
        let array = this.actions[index].array;
 
        for (let i = 0; i < array.length; i++) {
          this.actions[index].active.push(false);
        }
      }); 
      this.actions[0].active[item] = true;
    }
  }
}); 

 
/*
 *@名称: 时间选择器
 <com-date-picker start="" end="" v-model="showDatePicker"></com-date-picker>
 **/  
Vue.component("com-date-picker", {
  template: `
    <transition name="fade">
      <dl v-if="value" class="com-mask-layer com-disx com-picker com-date-picker">
        <dt class="cmt-mask" @click.stop="close"></dt>
        <dd class="com-slippicker" :class="{comSlip: slide}">
          <ul class="com-disx">
            <li @click.stop="close" class="com-active-bg cancel">取消</li>
            <li class="title">{{title}}</li>
            <li class="com-active-bg confirm" @click.stop="confirm">确定</li>
          </ul>
          <div class="picker-item" :style="{'max-height': length + 'rem'}">
            <div class="com-disx cmt-slots">
              <ul class="aux" v-for="(dx,index) in dateList" :style="transPos[index].move">
                <li v-for="(sdx, sindex) in dx.list" class="com-nowrap" :class="{active: dx.active[sindex]}">{{sdx}} {{dx.label}}</li>
              </ul>
            </div>
            <div class="cmt-mask"></div> 
            <h3></h3>
            <ul @touchstart.prevent.stop="startPos" @touchmove.prevent.stop="movePos" @touchend="endPos"
              class="com-screen com-disx aux animate">
              <li class="year"></li>
              <li class="month"></li>
              <li class="day"></li>
            </ul>
          </div>
        </dd>
      </dl>
    </transition>
  `,
  props: {
    value: String,
    title: {
      value: String,
      default() {return "时间选择器";}
    },
    start: {
      value: String,
      default() {
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();

        return (year - 100) + "-" + (month > 9 ? month : '0' + month) + "-" + (day > 9 ? day : '0' + day);
      }
    },
    end: { 
      value: String,
      default() {
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate(); 
        
        return (year + 100) + "-" + (month > 9 ? month : '0' + month) + "-" + (day > 9 ? day : '0' + day);
      }
    },
    show: {
      value: String,
      default() {
        return null;
      }
    }
  },
  model: {
    prop: "value",
    event: "submit"   
  },
  data() {
    return {
      slide: false,
      length: 5, 
      transPos: [{move: {}},{move: {}},{move: {}}],
      pos: {
        index: null,
        list: [
          {topY:null, bottomY:null, startY:null, moveY:null, differY:null, distance:null, distanceY:null},
          {topY:null, bottomY:null, startY:null, moveY:null, differY:null, distance:null, distanceY:null},
          {topY:null, bottomY:null, startY:null, moveY:null, differY:null, distance:null, distanceY:null}
        ]
      },
      dateList: [
        {index: 0, label: "年", active: [], list: []}, 
        {index: 0, label: "月", active: [], list: []}, 
        {index: 0, label: "日", active: [], list: []}  
      ],
      date: {
        start: null,
        end: null,
        show: null
      } 
    } 
  },
  created() {
    this.init();
    this.comVisual(this.value);
  },
  watch: {    
    'value': function(val) {
      this.comVisual(val);
    }
  },
  methods: {
    startPos(e) {
      let tag = e.target.className;
      this.pos.index = tag == 'year' ? 0 : (tag == 'month' ? 1 : (tag == 'day' ? 2 : null));
      let id = this.pos.index; 
      
      this.pos.list[id].startY = e.touches[0].pageY;
      this.pos.list[id].moveY = 0;
    },
    movePos(e) {
      let id = this.pos.index; 
      
      this.pos.list[id].moveY = e.touches[0].pageY;
      this.pos.list[id].differY = this.pos.list[id].moveY - this.pos.list[id].startY;
      
      this.pos.list[id].distance = this.pos.list[id].distanceY + this.pos.list[id].differY;
      this.transPos[id].move = {transform: "translateY(" + this.pos.list[id].distance + "px)"};
    },
    endPos() {
      let id = this.pos.index; 

      if (this.pos.list[id].distance > this.pos.list[id].topY) {
        this.pos.list[id].distanceY = this.pos.list[id].topY;
      } else if (this.pos.list[id].distance < this.pos.list[id].bottomY) {
        this.pos.list[id].distanceY = this.pos.list[id].bottomY;
      } else {
        let time = Math.floor(this.pos.list[id].distance / font_size);
        let model = this.pos.list[id].distance % font_size;

        //回弹到正中位置 
        let renew = null; 

        if (model > font_size/2) {
          renew = (time + 1) * font_size;
        } else {
          renew = time * font_size;
        }  

        this.pos.list[id].distanceY = renew; 
      } 


      //处理业务模型
      this.dateList[id].index = Math.round((this.pos.list[id].topY - this.pos.list[id].distanceY) / font_size);

      this.dateList[id].list.forEach((value, index)=> { 
        this.dateList[id].active[index] = false;
      });    
      this.dateList[id].active[this.dateList[id].index] = true;

      //动画 
      this.transPos[id].move = {transform: "translateY(" + this.pos.list[id].distanceY + "px)", 'transition-duration':
      '500ms'}; 

      //同步年-月-日
      if (id == 0) {
        this.setDate();
      }
      if (id == 1) {
        this.setDate();
      }


      //清除变量
      this.pos.list[id].differY = null;
      this.pos.list[id].distance = null;
    },
    update() {
      this.$emit("submit", false);
    },
    //关闭
    close(fn) {
      this.slide = false;
      this.update(); 
      if (fn instanceof Function) {
        fn();
      }
    },
    //打开
    open() {
      window.setTimeout(()=> {
        this.slide = true;
      }, 50); 
    },
    comVisual(state) {
      if (!state) { 
        this.close(); 
        return ;  
      } 
      this.open();
    }, 
    confirm() {
      let submitData = [
        {
          index: this.dateList[0].index,
          name: this.dateList[0].list[this.dateList[0].index],
        },
        {
          index: this.dateList[1].index,
          name: this.dateList[1].list[this.dateList[1].index],
        }, 
        {
          index: this.dateList[2].index,   
          name: this.dateList[2].list[this.dateList[2].index]
        }
      ];
      this.$emit("change", submitData);
      this.close();
    },
    now() {
      let date = new Date();
      let year = date.getFullYear();
      let month = date.getMonth() + 1;
      let day = date.getDate(); 
      
      return year + "-" + (month > 9 ? month : '0' + month) + "-" + (day > 9 ? day : '0' + day);
    },
    setDate() {
      let year = this.dateList[0].list[this.dateList[0].index];
      let month = this.dateList[1].list[this.dateList[1].index];
      let date = (new Date(year, month, 0)).getDate();

      this.dateList[2].list = [];
      for (let i = 0; i < date; i++) {
        this.dateList[2].list.push(i + 1);
        this.dateList[2].active.push(false); 
      }
      this.dateList[2].index = 0; 
      this.dateList[2].active[this.dateList[2].index] = true;

      this.pos['list'][2].bottomY = this.pos['list'][2].topY - font_size * (this.dateList[2].list.length - 1);

      this.pos['list'][2].distanceY = font_size * ((this.length - 1)/2 - this.dateList[2].index);
      this.transPos[2].move = {transform: "translateY(" + this.pos.list[2].distanceY + "px)", 'transition-duration':
      '500ms'}; 
    },
    //初始化滚动栏位置 
    initPos() {  
      this.transPos.forEach((value, index)=> {
        this.pos['list'][index].distanceY = font_size * ((this.length - 1)/2 - this.dateList[index].index);
        this.transPos[index].move = {transform: "translateY(" + this.pos['list'][index].distanceY + "px)"};
      });   
    },
    initYear() {
      let startDate = this.date.start;
      let endDate = this.date.end;
      let showDate = this.date.show;

      this.dateList[0].list = [];

      let year = endDate[0] - startDate[0];
      for (let i = 0; i< year + 1; i++) {
        this.dateList[0].list.push(startDate[0] + i);
        this.dateList[0].active.push(false);
      }
      this.dateList[0].index = showDate[0] - startDate[0];
      this.dateList[0].active[this.dateList[0].index] = true; 

      //设置滚动栏高度
      this.pos['list'][0].bottomY = this.pos['list'][0].topY - font_size * (this.dateList[0].list.length - 1);
    },
    initMonth() {
      let startDate = this.date.start;
      let endDate = this.date.end;
      let showDate = this.date.show;

      let month_start = null; 
      let month_end = null;
      //条件: 显示日期的年份与开始日期的年份一致
      if (this.dateList[0].list[this.dateList[0].index] == startDate[0]) {
        month_start = startDate[1];
        month_end = 12; 
      //条件: 显示日期的年份与结束日期的年份一致
      } else if(this.dateList[0].list[this.dateList[0].index] == endDate[0]) {
        month_start = 1; 
        month_end = endDate[1];  
      //其他
      } else {
        month_start = 1;  
        month_end = 12; 
      } 

      this.dateList[1].list = [];

      for (let i = month_start - 1; i < month_end; i++) { 
        this.dateList[1].list.push(i + 1);
        this.dateList[1].active.push(false); 
      }
      let monthIndex = 0;
      for (let i = 0; i < this.dateList[1].list.length; i++) {
        if (i == parseInt(showDate[1]) - 1) {
          monthIndex = i;
          break;
        }    
      }
      this.dateList[1].index = monthIndex;
      this.dateList[1].active[this.dateList[1].index] = true;

      //设置滚动栏高度
      this.pos['list'][1].bottomY = this.pos['list'][1].topY - font_size * (this.dateList[1].list.length - 1);
    },
    initDate() {
      let startDate = this.date.start;
      let endDate = this.date.end;
      let showDate = this.date.show;

      let date_start = null;
      let date_end = null; 
      //条件: 显示日期的年份与开始日期的年份一致
      if (this.dateList[0].list[this.dateList[0].index] == startDate[0]) {
        date_start = startDate[2];
        date_end = (new Date(showDate[0], showDate[1], 0)).getDate();
      //条件: 显示日期的年份与结束日期的年份一致
      } else if(this.dateList[0].list[this.dateList[0].index] == endDate[0]) {
        date_start = 1;
        date_end = endDate[2]; 
      //其他   
      } else { 
        date_start = 1;
        date_end = (new Date(showDate[0], showDate[1], 0)).getDate();
      } 
      
      this.dateList[2].list = [];

      for (let i = date_start - 1; i < date_end; i++) {
        this.dateList[2].list.push(i + 1);
        this.dateList[2].active.push(false); 
      }
      let dayIndex = null;
      for (let i = 0; i < this.dateList[2].list.length; i++) {
        if (i == parseInt(showDate[2]) - 1) {
          dayIndex = i;
          break;
        }    
      }
      this.dateList[2].index = dayIndex; 
      this.dateList[2].active[this.dateList[2].index] = true;

      //设置滚动栏高度
      this.pos['list'][2].bottomY = this.pos['list'][2].topY - font_size * (this.dateList[2].list.length - 1);
    }, 
    init() {
      //起始日期
      let startDate = null;
      startDate = this.start == "now" ? this.now().split("-") : this.start.split("-");
      //结束日期
      let endDate = null; 
      endDate = this.end == "now" ? this.now().split("-") : this.end.split("-");

      //初始化中间值
      let showDate = null;
      let mid = (parseInt(startDate[0]) + Math.round((parseInt(endDate[0])-parseInt(startDate[0]))/2)) + "-" + startDate[1] + "-" + startDate[2];
      showDate = this.show == 'now' ? this.now().split('-') : (this.show && this.show != 'now') ? this.show : mid;

      startDate.forEach((value, index)=> {
        startDate[index] = parseInt(value);
      });
      endDate.forEach((value, index)=> {
        endDate[index] = parseInt(value);
      }); 
      showDate.forEach((value, index)=> {
        showDate[index] = parseInt(value);
      }); 

      this.date.start = startDate;
      this.date.end = endDate;
      this.date.show = showDate; 

      this.transPos.forEach((value, index)=> {
        this.pos['list'][index].topY = font_size * (this.length - 1)/2;  
      });   

      this.initYear();  //初始化-年
      this.initMonth();  //初始化-月      
      this.initDate();  //初始化-日    

      this.initPos();  //初始化位置
    }
  }
});


/*
 *@名称: 地址选择器
 <com-adrress-picker :slots="addrSlots" title="" v-model="showAddrPicker"></com-adrress-picker>
 **/  
Vue.component("com-adrress-picker", {
  template: `
    <transition name="fade">
      <dl v-if="value" class="com-mask-layer com-disx com-picker com-adrress-picker">
        <dt class="cmt-mask" @click.stop="close"></dt>
        <dd class="com-slippicker" :class="{comSlip: slide}">
          <ul class="com-disx">
            <li @click.stop="close" class="com-active-bg cancel">取消</li>
            <li class="title">{{title}}</li>
            <li class="com-active-bg confirm" @click.stop="confirm">确定</li>
          </ul>
          <div class="picker-item" :style="{'max-height': length + 'rem'}">
            <div class="com-disx cmt-slots">
              <ul class="aux" v-for="(dx,index) in addrList" :style="transPos[index].move">
                <li v-for="(sdx, sindex) in dx.list" class="com-nowrap" :class="{active: sdx.active}">{{sdx.name}}</li>
              </ul>
            </div>
            <div class="cmt-mask"></div>
            <h3></h3>
            <ul @touchstart.prevent.stop="startPos" @touchmove.prevent.stop="movePos" @touchend="endPos"
              class="com-screen com-disx aux animate">
              <li class="province"></li>
              <li class="city"></li>
              <li class="country"></li>
            </ul>
          </div>
        </dd>
      </dl>
    </transition>
  `,
  props: {
    value: String,
    title: {
      value: String,
      default() {return "地址选择器";}
    },
    slots: []
  },
  model: {
    prop: "value",
    event: "submit"   
  },
  data() {
    return {
      addrList: [{index:0, list:[]},{index:0, list:[]},{index:0, list:[]}],
      slide: false,
      length: 5, 
      transPos: [{move: {}},{move: {}},{move: {}}],
      pos: {
        index: null,
        list: [
          {topY:null, bottomY:null, startY:null, moveY:null, differY:null, distance:null, distanceY:null},
          {topY:null, bottomY:null, startY:null, moveY:null, differY:null, distance:null, distanceY:null},
          {topY:null, bottomY:null, startY:null, moveY:null, differY:null, distance:null, distanceY:null}
        ]
      }
    }
  },
  created() {
    this.init();
    this.comVisual(this.value);
  },
  watch: {    
    'value': function(val) {
      this.comVisual(val);
    }
  },
  methods: {
    startPos(e) {
      let tag = e.target.className;
      this.pos.index = tag == 'province' ? 0 : (tag == 'city' ? 1 : (tag == 'country' ? 2 : null));
      let id = this.pos.index;

      this.pos.list[id].startY = e.touches[0].pageY;
      this.pos.list[id].moveY = 0;
    },
    movePos(e) {
      let id = this.pos.index; 

      this.pos.list[id].moveY = e.touches[0].pageY; 
      this.pos.list[id].differY = this.pos.list[id].moveY - this.pos.list[id].startY;
      
      this.pos.list[id].distance = this.pos.list[id].distanceY + this.pos.list[id].differY;
      this.transPos[id].move = {transform: "translateY(" + this.pos.list[id].distance + "px)"};
    }, 
    endPos() {
      let id = this.pos.index; 

      if (this.pos.list[id].distance > this.pos.list[id].topY) {
        this.pos.list[id].distanceY = this.pos.list[id].topY;
      } else if (this.pos.list[id].distance < this.pos.list[id].bottomY) {
        this.pos.list[id].distanceY = this.pos.list[id].bottomY;
      } else {
        let time = Math.floor(this.pos.list[id].distance / font_size);
        let model = this.pos.list[id].distance % font_size;

        //回弹到正中位置 
        let renew = null; 

        if (model > font_size/2) {
          renew = (time + 1) * font_size;
        } else {
          renew = time * font_size;
        }  

        this.pos.list[id].distanceY = renew; 
      } 
 
      //重置active 
      this.addrList[id].index = Math.round((this.pos.list[id].topY - this.pos.list[id].distanceY) / font_size);

      this.addrList[id].list.forEach((value, index)=> { 
        this.addrList[id].list[index].active = false;
      }); 
      this.addrList[id].list[this.addrList[id].index].active = true;

      //动画 
      this.transPos[id].move = {transform: "translateY(" + this.pos.list[id].distanceY + "px)", 'transition-duration':
      '500ms'}; 

      //同步省市县数据
      if (id == 0) {
        this.addrList[1].index = 0;
        this.setCity();  //初始化-市

        this.pos.list[1].distanceY = font_size * ((this.length - 1)/2 - this.addrList[1].index);
        this.transPos[1].move = {transform: "translateY(" + this.pos.list[1].distanceY + "px)", 'transition-duration':
        '500ms'};

        this.addrList[2].index = 0;
        this.setCountry();  //初始化-县 

        this.pos.list[2].distanceY = font_size * ((this.length - 1)/2 - this.addrList[2].index);
        this.transPos[2].move = {transform: "translateY(" + this.pos.list[2].distanceY + "px)", 'transition-duration':
        '500ms'};
      } 
      if (id == 1) {
        this.addrList[2].index = 0;
        this.setCountry();  //初始化-县 

        this.pos.list[2].distanceY = font_size * ((this.length - 1)/2 - this.addrList[2].index);
        this.transPos[2].move = {transform: "translateY(" + this.pos.list[2].distanceY + "px)", 'transition-duration':
        '500ms'};
      }
      

      //清除变量   
      this.pos.list[id].differY = null;
      this.pos.list[id].distance = null;
    },
    update() { 
      this.$emit("submit", false);
    },
    //关闭
    close(fn) {
      this.slide = false;
      this.update(); 
      if (fn instanceof Function) {
        fn();
      }
    },
    //打开
    open() {
      window.setTimeout(()=> {
        this.slide = true;
      }, 50);
    },
    comVisual(state) {
      if (!state) { 
        this.close(); 
        return ;  
      } 
      this.open();
    },
    confirm() {
      let submitData = [
        {
          index: this.addrList[0].index,
          name: this.addrList[0].list[this.addrList[0].index].name,
          code: this.addrList[0].list[this.addrList[0].index].code
        },
        {
          index: this.addrList[1].index,
          name: this.addrList[1].list[this.addrList[1].index].name,
          code: this.addrList[1].list[this.addrList[1].index].code
        }, 
        {
          index: this.addrList[2].index, 
          name: this.addrList[2].list[this.addrList[2].index].name,
          code: this.addrList[2].list[this.addrList[2].index].code
        }
      ];
      this.$emit("change", submitData);
      this.close();
    },
    //初始化滚动栏位置
    initPos() { 
      this.transPos.forEach((value, index)=> {
        this.pos['list'][index].topY = font_size * (this.length - 1)/2; 
        
        this.pos['list'][index].distanceY = font_size * ((this.length - 1)/2 - this.addrList[index].index);
        this.transPos[index].move = {transform: "translateY(" + this.pos['list'][index].distanceY + "px)"};
      });
    },
    //设置省级
    setProvince() {
      let provicne = require("./data/address.json");
      this.addrList[0].list = [];
      provicne.forEach((value)=> {
        let obj = {name: value['name'], code: value['code'], active: false, city: value['city']};
        this.addrList[0].list.push(obj);
      });
      this.addrList[0].list[this.addrList[0].index]['active'] = true;

      this.pos['list'][0].bottomY = this.pos['list'][0].topY - font_size * (this.addrList[0].list.length - 1);
    },  
    //设置市级
    setCity() {   
      let city = this.addrList[0].list[this.addrList[0].index]['city'];
      this.addrList[1].list = [];
      city.forEach((value)=> {
        let obj = {name: value['name'], code: value['code'], active: false, area: value['area']};
        this.addrList[1].list.push(obj);
      });
      this.addrList[1].list[this.addrList[1].index]['active'] = true;

      this.pos['list'][1].bottomY = this.pos['list'][1].topY - font_size * (this.addrList[1].list.length - 1);
    },
    //设置县级
    setCountry() {
      let country = this.addrList[1].list[this.addrList[1].index]['area'];
      this.addrList[2].list = [];
      country.forEach((value)=> {
        let obj = {name: value['name'], code: value['code'], active: false};
        this.addrList[2].list.push(obj);
      });
      this.addrList[2].list[this.addrList[2].index]['active'] = true;

      this.pos['list'][2].bottomY = this.pos['list'][2].topY - font_size * (this.addrList[2].list.length - 1);
    },
    //初始化数据 
    init() { 
      //初始化栏目默认值
      this.slots.forEach((value, index)=> {
        this.addrList[index].index = value.index; 
      }); 

      this.initPos();  //初始化位置

      this.setProvince();  //初始化-省
      this.setCity();  //初始化-市
      this.setCountry();  //初始化-县
    } 
  }
});


/*
 *@名称: 加载动画
  <com-spinner border="border:1px solid #ddd;"></com-spinner>
 **/ 
Vue.component('com-spinner', {
  props: {
    type: {
      type: String,
      default: ()=> {
        return "default" 
      }
    },
    border: {
      type: String,
      default: ()=> {
        return ""
      }
    }
  },
  template: `
    <div class="com-spinner" :class="type">
      <!--<com-spinner></com-spinner>-->
      <div v-if="type == 'default'" class="default"></div>
      <!--<com-spinner type='pull'></com-spinner>-->
      <div v-if="type == 'pull'" class="pull"></div>
      <!--<com-spinner type='bounce'></com-spinner>-->
      <div v-if="type == 'bounce'" class="bounce bounce-inner"></div>
      <div v-if="type == 'bounce'" class="bounce bounce-outer"></div>
      <!--<com-spinner type="triple"></com-spinner>-->
      <ul v-if="type == 'triple'" class="aux com-disx triple">
        <li class="left"></li>
        <li class="center"></li>
        <li class="right"></li>
      </ul>
      <!--<com-spinner type='circle'></com-spinner>-->
      <ul v-if="type == 'circle'" class="aux circle">
        <li class="li-01"></li>
        <li class="li-02"></li>
        <li class="li-03"></li>
        <li class="li-04"></li>
        <li class="li-05"></li>
        <li class="li-06"></li>
        <li class="li-07"></li>
        <li class="li-08"></li>
        <li class="li-09"></li>
        <li class="li-10"></li>
        <li class="li-11"></li>
        <li class="li-12"></li>
        <li class="li-13"></li>
      </ul>
    </div>
  `,
  data: function() {
    return {
      show: false
    } 
  }, 
  created: function() {},
  watch: {},
  methods: {}
});
 

/*
 *@名称: 上下轮播
  <com-notice-turn :array="comNoticeList" time="1"></com-notice-turn>
 **/
Vue.component("com-notice-turn", {
  template: `
  <div v-if="notice.state" class="com-flex com-notice-turn"> 
    <em><i class="iconfont">&#xe694;</i></em>
    <div>
      <ul class="aux" :style="notice.slide">
        <li v-for="(dx, index) in notice.data" :key="index" class="com-nowrap alt">{{dx}}</li>
      </ul>
    </div>
  </div>
  `,
  props: {
    array: {
      type: Array,
      default: ()=> {
        return [] 
      }
    },
    time: {
      type: Number,
      default: ()=> {
        return "2000"
      }
    }
  },
  data() {
    return {
      notice: {
        state: false,
        data: [],
        slide: {},
        time: 2000,
        start: 0
      }
    }
  },
  created () {
    if (this.array instanceof Array && this.array.length > 0) {
      this.notice.state = true;
      this.notice.data = this.array;
    }
    if (plug.judgeValid(this.time) && parseInt(this.time) > 0) {
      this.notice.time = this.time;
    } 
    this.slide();
  },
  methods: {
    slide: function() {
      window.setInterval(()=> {
        if (this.notice.start == this.notice.data.length - 1) {
          this.notice.start = 0;
        } 
        //轮播过渡
        this.notice.slide = {transform: "translateY(-0.5rem)", 'transition-duration': '500ms'};

        //过渡逻辑
        window.setTimeout(()=> {
          this.notice.slide = {transform: "translateY(0)"};

          let first = this.notice.data.shift();  //删除第一个数据并存储
          this.notice.data.push(first);  //追加删除的第一个数据
        }, 500); 

        this.notice.start += 1;
      }, this.notice.time);
    }
  }
}); 

 
/* 
 *@名称: Swipe
  <com-swipe index="0" time="2000" auto="true" indicator="true" @change="getSlideCount"></com-swipe>
 **/
Vue.component("com-swipe", {
  template: `
  <div class="com-swipe" @scroll.stop @click.stop @touchstart.stop="startTurn" @touchmove.stop="moveTurn" @touchend.stop="endTurn">
    <div class="com-swipe-wrap">
      <slot></slot>
    </div>
    <p v-if="defaultCount > 1&& indicator == 'true'" class="com-flex">
      <em v-for="(dx,index) in defaultCount" :class="{active: labelIndex == index}"></em>
    </p>
  </div>
  `, 
  props: {
    index: {type: Number, default: ()=> {return 0}},
    time: {type: Number, default: ()=> {return 0}}, 
    auto: {type: String, default: ()=> {return "true"}},
    indicator: {type: String, default: ()=> {return "true"}},
    vhscroll: {type: String, default: ()=> {return "true"}}  //处理手势垂直滑动时(始点与移动点)，禁止轮播
  },
  data() {
    return {
      stopInterval: null,  //延时对象
      target: null,
      defaultCount: null,
      defaultIndex: null,
      labelIndex: null,
      defaultTime: 2000,
      width: null,  //容器width

      slide: {
        node: null,  //子元素数组
        length: null,  //子元素数量
        loop: null,  //数组上限
        state: false
      },
      turn: {
        prev: null, 
        next: null
      },
      pos: {
        startX: null,
        startY: null,
        moveX: null,
        moveY: null,
        endX: null,
        endY: null,
        distanceX: null,
        distanceY: null,
        stopSlide: false  
      },
    }
  },
  mounted() { 
    //解决变量AJAX请求延迟导致帧数为0的情况
    this.$nextTick(()=> {
      window.setTimeout(()=> {
        this.load();
        this.init();
        
        //只有一帧时，不执行轮播
        if (this.defaultCount == 1) {
        return ; 
        }
        this.autoInterval();
      }, 1000); 
    });
  }, 
  created() {
    this.$emit("change", this.defaultIndex);
  },
  watch: {
    defaultIndex(val) {
      this.setIndex(val);
    },
    labelIndex(val) {
      this.$emit("change", val);
    }
  }, 
  methods: {
    startTurn(e) {
      if (this.defaultCount < 2) {
        return ;
      }
      window.clearInterval(this.stopInterval); 
      this.stopSlide = false; 

      this.pos.startX = e.touches[0].pageX;
      this.pos.startY = e.touches[0].pageY;
      this.pos.moveX = 0; 
      this.pos.moveY = 0;
      
      plug.removeClass(this.target.children[this.turn.prev], "animate");
      plug.removeClass(this.target.children[this.defaultIndex], "animate");
      plug.removeClass(this.target.children[this.turn.next], "animate");
    },
    moveTurn(e) {
      if (this.defaultCount < 2) {
        return ;
      }
      if (this.stopSlide && this.vhscroll != "false") {
        return ;
      }
      this.pos.moveX = e.touches[0].pageX;
      this.pos.moveY = e.touches[0].pageY;
      this.pos.distanceX = this.pos.moveX - this.pos.startX;
      this.pos.distanceY = this.pos.moveY - this.pos.startY;

      let tanX = Math.abs(this.pos.distanceX);
      let tanY = Math.abs(this.pos.distanceY);
      let tanXY = tanY/tanX;
      if (tanXY > 1 && this.vhscroll != "false") {
        this.stopSlide = true;
        return ;
      }
      
      //to Right
      if (this.pos.distanceX > 0) {
        this.right();
      } 
      //to Left
      if (this.pos.distanceX < 0) {
        this.left();
      }
    }, 
    endTurn() {  
      if (this.defaultCount < 2) {
        return ;
      }
      plug.addClass(this.target.children[this.turn.prev], "animate");
      plug.addClass(this.target.children[this.defaultIndex], "animate");
      plug.addClass(this.target.children[this.turn.next], "animate");

      if (this.stopSlide && this.vhscroll != "false") {
        this.autoInterval();
        return ;
      } 

      if (this.pos.distanceX == 0) {
        return ;
      }

      //回流-滑动判断标准
      let judge = Math.abs(this.pos.distanceX) - this.width/2;
      
      //滑动 
      if (judge >= 0) {
        //滑动-向右
        if (this.pos.distanceX > 0) {
          plug.setStyle(this.target.children[this.turn.next], "display", "none");
          plug.removeClass(this.target.children[this.turn.next], "animate");
          plug.setStyle(this.target.children[this.turn.next], "transform", "translateX(0)");

          plug.addClass(this.target.children[this.defaultIndex], "animate");
          plug.setStyle(this.target.children[this.defaultIndex], "transform", "translateX(" + this.width + "px)");

          plug.addClass(this.target.children[this.turn.prev], "animate"); 
          plug.setStyle(this.target.children[this.turn.prev], "transform", "translateX(0)");

          //重置序列号
          this.continue(false);
          this.more();

          plug.addClass(this.target.children[this.turn.prev], "animate");
          plug.setStyle(this.target.children[this.turn.prev], "transform", "translateX(-" + this.width + "px )");
          plug.setStyle(this.target.children[this.turn.prev], "display", "block"); 
        } 
        //滑动-向左  
        if (this.pos.distanceX < 0) {
          this.loopSlide();
        }  
      }
      //回流
      else {
        plug.setStyle(this.target.children[this.turn.prev], "transform", "translateX(-" + this.width + "px)");
        plug.setStyle(this.target.children[this.defaultIndex], "transform", "translateX(0)");
        plug.setStyle(this.target.children[this.turn.next], "transform", "translateX(" + this.width + "px)");
      }

      //清除变量
      this.pos.distanceX = null; 
      this.autoInterval();
    }, 
    left() {
      plug.setStyle(this.target.children[this.turn.prev], "transform", "translateX(-" + this.width - this.pos.distanceX + "px)");
      plug.setStyle(this.target.children[this.defaultIndex], "transform", "translateX(" + this.pos.distanceX + "px)");
      plug.setStyle(this.target.children[this.turn.next], "transform", "translateX(" + (this.width + this.pos.distanceX) + "px)");
    }, 
    right() { 
      plug.setStyle(this.target.children[this.turn.prev], "transform", "translateX(-" + (this.width - this.pos.distanceX) + "px)");
      plug.setStyle(this.target.children[this.defaultIndex], "transform", "translateX(" + this.pos.distanceX + "px)");
      plug.setStyle(this.target.children[this.turn.next], "transform", "translateX(" + this.width + this.pos.distanceX + "px)");  
    },
    autoInterval() {
      if (this.auto != "false") {
        this.stopInterval = window.setInterval(()=> {
          this.loopSlide();  
        }, this.defaultTime);  
      }
    },
    loopSlide() { 
      plug.setStyle(this.target.children[this.turn.prev], "display", "none");
      plug.removeClass(this.target.children[this.turn.prev], "animate");
      plug.setStyle(this.target.children[this.turn.prev], "transform", "translateX(0)");
      
      plug.addClass(this.target.children[this.defaultIndex], "animate");
      plug.setStyle(this.target.children[this.defaultIndex], "transform", "translateX(-" + this.width + "px)");

      plug.addClass(this.target.children[this.turn.next], "animate");
      plug.setStyle(this.target.children[this.turn.next], "transform", "translateX(0)");
      
      //重置序列号
      this.continue(true); 
      this.more();   
      
      //延时: 处理只有三帧画面时出现滑动错乱
      window.setTimeout(()=> {
        plug.setStyle(this.target.children[this.turn.next], "transform", "translateX(" + this.width + "px)");
        plug.setStyle(this.target.children[this.turn.next], "display", "block");
        plug.addClass(this.target.children[this.turn.next], "animate");     
      }, 50); 
    },
    continue(state) {
      //递进序列号
      if (state) {
        if (this.defaultIndex == this.slide.loop) {
          this.defaultIndex = 0;
        } else { 
          this.defaultIndex++;
        }
        return ;
      }
      //回流序列号 
      if (this.defaultIndex == 0) {
        this.defaultIndex = this.slide.loop;
      } else { 
        this.defaultIndex--;
      }
    },
    more() {
      //预置上一帧、下一帧
      if (this.defaultIndex == this.slide.loop) {
        this.turn.prev = this.defaultIndex - 1;
        this.turn.next = 0;
      } else if (this.defaultIndex >= 1) {
        this.turn.prev = this.defaultIndex - 1;
        this.turn.next = this.defaultIndex + 1;
      } else { 
        this.turn.prev = this.slide.loop;
        this.turn.next = this.defaultIndex + 1; 
      }
    }, 
    judgeSlide() {
      this.more();
      plug.setStyle(this.target.children[this.turn.prev], "transform", "translateX(-" + this.width + "px)");
      plug.setStyle(this.target.children[this.turn.prev], "display", "block");
      plug.addClass(this.target.children[this.turn.prev], "animate");
       
      plug.setStyle(this.target.children[this.turn.next], "transform", "translateX(" + this.width + "px)");
      plug.setStyle(this.target.children[this.turn.next], "display", "block");
      plug.addClass(this.target.children[this.turn.next], "animate");
    },
    init() {
      plug.setStyle(this.target.children[this.defaultIndex], "display", "block");
      plug.addClass(this.target.children[this.defaultIndex], "animate");

      //只有一帧时，不执行轮播
      if (this.defaultCount == 1) {
        return ;
      }
      this.judgeSlide();
    },
    load() {
      this.target = this.$el.children[0];
      this.width = parseFloat((plug.getStyle(this.$el, "width") || "").replace("px", ""));
      this.defaultCount = this.target.children.length;
      
      //轮播时间
      if (this.time) {
        this.defaultTime = this.time;
      } else {
        this.defaultTime = 2000;
      }
      //轮播序列号
      if (this.index >= this.defaultCount) {
        this.defaultIndex = this.defaultCount - 1; 
      } else {
        this.defaultIndex = this.index;
      }
      this.setIndex(this.defaultIndex);

      //处理只有2帧情况兼容方案
      if (this.defaultCount == 2) {
        let first = this.target.children[0].cloneNode(true);
        let last = this.target.children[1].cloneNode(true);

        this.target.insertBefore(last, this.target.children[0]);
        this.target.appendChild(first);
      }

      //多帧情况 
      this.slide.node = this.target.children;
      this.slide.length = this.target.children.length;
      this.slide.loop = this.slide.length - 1; 

      if (this.defaultCount == 2) { 
        this.defaultIndex++; 
      }
    },
    setIndex(val) {
      if (this.defaultCount == 2) {
        if (val % 2 == 0) {
          this.labelIndex = 1;
          return ;
        }
        this.labelIndex = 0;
        return ;  
      }
      this.labelIndex = val;
    }
  }
});


/*
*@名称: Swipe Item
  <com-swipe-item></com-swipe-item>
**/ 
Vue.component("com-swipe-item", {
  template: `
  <div class="com-swipe-item">
    <slot></slot>
  </div>
  ` 
}); 


/*
 *@名称: Tab栏目
  <com-tab-container></com-tab-container>
**/
Vue.component("com-tab-container", {
  template: `

  `,
  props: {
    params: {
      type: Object,
      default: ()=> {
        return {} 
      }
    }
  },
  data() {
    return {
      name: ""
    }
  },
  created() {},
  watch: {},
  methods: {} 
});


/* 
 *@名称: 下拉刷新
  <com-pull-flush></com-pull-flush>
 **/
Vue.component("com-pull-flush", {
  props: {
    params: {
      type: Object,
      default: ()=> {
        return {} 
      }
    }
  },
  template: `
    <div class="com-pull-flush" @touchstart="startPull" @touchmove="movePull" @touchend="endPull">
      <div class="priv-frame" :style="translateDown">
        <div class="com-flexv priv-top">
          <em :style="pullShow">↓</em>
          <com-spinner type="pull" :style="pullLoading"></com-spinner>
        </div>
        <slot></slot>
      </div>
    </div>
  `,
  data() {
    return {
      translateDown: {},
      pullShow: {},
      pullLoading: {display: "none"},
      stopPull: false,
      pos: {
        startY: null,
        moveY: null,
        endY: null,
        distanceY: null
      }
    }
  },
  created() {
    this.translateDown = {transform: "translate3d(0px, 0px, 0px)"};
  },
  watch: {},
  methods: {
    startPull(e) {
      if (this.stopPull) {
        return ;
      }
      this.pos.startY = e.touches[0].pageY;
      this.pos.moveY = 0;
    },
    movePull(e) {
      if (this.stopPull) {
        return ;
      }
      this.pos.moveY = e.touches[0].pageY;
      this.pos.distanceY = this.pos.moveY - this.pos.startY;

      this.translateDown = {transform: "translate3d(0px, " + this.pos.distanceY + "px, 0px)"};

      if (this.pos.distanceY > 70) {
        this.pullShow = {transform: "rotate(180deg)", "transition-duration": "200ms"};
      } else {
        this.pullShow = {};
      }
    },
    endPull() {
      this.stopPull = true;
      this.translateDown = {"transition-duration": "600ms", transform: "translate3d(0px, 50px, 0px)"};
      window.setTimeout(()=> {
        this.pullShow = {display: "none"};
        this.pullLoading = {display: "inline-block"};
      }, 600);
      window.setTimeout(()=> {
        this.translateDown = {"transition-duration": "600ms", transform: "translate3d(0px, 0px, 0px)"};
        this.pullLoading = {display: "none"};
        this.stopPull = false;
      }, 2000);
    }
  }
});




















/*
 *@指令
 *---------------------------------------------------------------------
 *---------------------------------------------------------------------
 *---------------------------------------------------------------------
 *---------------------------------------------------------------------
 *---------------------------------------------------------------------
 **/


 /*
  *@名称: 无限滚动
   <ysun v-load-scroll="loadMore"></ysun>
  **/
Vue.directive("load-scroll", {
  inserted(el, binding) {
    let bottom = 50;
    let h = el.clientHeight;
    let bottomState = false;

    el.addEventListener("scroll", (e)=> {
      let sh = e.target.scrollHeight;
      let top = e.target.scrollTop;

      let d = sh - h;
      
      //距离底部距离50px时触发事件
      if (top > d - bottom) {
        //阻止执行多次加载
        if (bottomState) { 
          return ;
        }
        bottomState = true;

        if (binding.expression) { 
          binding.value();
        } 
      } else { 
        bottomState = false;
      }
    });
  } 
}); 


/*
 *@名称: 图片懒加载
  <ysun v-loading></ysun>
 **/
Vue.directive("loading", {
  inserted(el) {
    let parent = el.parentNode;

    let attr = null;
    if (window.getComputedStyle) {
      attr = window.getComputedStyle(el, null);
    } else {
      attr = el.currentStyle;
    }
    
    let css = {
      position: "absolute",
      top: attr['top'] == "auto" ? "0" : el.offsetTop + "px",
      right: attr.right == "auto" ? "0" : attr['right'],
      bottom: attr.bottom == "auto" ? "0" : attr['bottom'],
      left: attr.left == "auto" ? "0" : el.offsetLeft + "px",
      width: attr['width'],
      height: attr['height'], 
      'border-radius': attr['border-radius'],
      'box-shadow': attr['box-shadow']
    }
    let string = "";
    for (let i in css) {
      string += ";" + i + ":" + css[i];
    }

    let div = document.createElement("div");
    div.setAttribute("style", string);
    div.setAttribute("class", "com-flexv com-el-load");

    let load = document.createElement("div");
    load.setAttribute("class", "com-spinner-snake");
    div.appendChild(load); 

    let state = document.createElement("div");
    state.setAttribute("style", ";display:none;");
    state.setAttribute("class", "com-flexv com-spinner-failed");
    state.innerHTML = `<i class="iconfont">&#xe663;</i>`;
    div.appendChild(state);

    parent.appendChild(div);

    //业务逻辑
    let loadFailed = function() {
      load.setAttribute("style", ";display:none;");
      state.setAttribute("style", ";");
    };  

    let imgObj = null;
    let img = new Image();
    img.onload = function() {
      div.remove();  
    }
    img.onerror = function() { 
      loadFailed(); 
    }

    if (!plug.judgeValid(el.getAttribute("src"))) {
      loadFailed();
    }
    let count = 1;
    let interval = window.setInterval(()=> {
      count++;
      imgObj = el.getAttribute("src");
      if (imgObj) {
        img.src = imgObj;
        window.clearInterval(interval);
        return ;
      }
      //取整
      let inter = count / 5;
      if (count % 5 == 0) {
        if (inter > 15) {   
          window.clearInterval(interval); 
          loadFailed(); 
        }
      }
    }, 200);
  }
});

