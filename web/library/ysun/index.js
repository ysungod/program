const version = '1.0.1';
const ysun_name = "王军-H5生态系统架构师-Node.js架构师-AI人工智能工程师-TensorFlow实践者！";
const install = function(Vue, config = {}) {
  if (install.installed) return;
};


// auto install
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}; 

module.exports = {
  install, 
  version,
  ysun_name
};
