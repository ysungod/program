export default {
  name: 'ysun',
  data () {
    return {
      //上下轮播消息
      comTurnList: ["1、回家的时候丹甫股份的发", "2、按时间大港股份", "3、师大皇冠会掉个", "4、很多国家三国大时代"],

      loadMoreCount: 20,
      loadState: false,
      //选择框
      radioVItem: false,
      radioVList: [
        {content: "", id: "", active: false},
        {content: "", id: "", active: false},
        {content: "", id: "", active: false}
      ],
      checkBoxVList: [
        {content: "", id: "", active: false},
        {content: "", id: "", active: false},
        {content: "", id: "", active: false}
      ],
      actionsheet: false,
      actions: [
        {name: "本地上传", method() {console.log("本地上传");}}, 
        {name: "拍照上传", method() {console.log("拍照上传");}} 
      ],

      //选择器
      showPicker: false,
      pickerActions: [
        {
          name: "年",
          item: 0,
          array: ['2018', '2017', '2016', '2015', '2014', '2013', '2012', '2011', '2010']
        } 
      ],
      //时间选择器
      showDatePicker: false,
 
      //地址选择器 
      showAddrPicker: false,
      addrSlots: [
        {index: 0},
        {index: 0},
        {index: 0} 
      ]
    }
  },
  created () {


  },
  watch: {},
  mounted() { 
    this.$nextTick(() => {});
  },
  methods: { 
    getSlideCount(index) {
      if (plug.judgeValid(index)) {
        console.log(index);
      }
    },
    //无限上拉加载
    loadMore() {
      this.loadState = true;
      window.setTimeout(()=> {
        this.loadMoreCount += 10;
        this.loadState = false;
      }, 2000); 
    },
    doDatePicker() {
      this.showDatePicker = true; 
    }, 
    getDateValue(value) {
      console.log(value); 
    },
    doAddrPicker() { 
      this.showAddrPicker = true;
    },
    getAddrValue(value) {
      console.log(value); 
    },
    doPicker() {
      this.showPicker = true;
    },
    getPickerValue(value) {
      console.log(value); 
    },
    doActionSheet() {
      this.actionsheet = true;
    },
    doConfirm() {
      this.$root.comConfirmData.layer = 1;
      this.$root.comConfirmData.info = "你确认要关闭吗?";
      this.$root.comConfirmData.confirm = ()=> {
        this.$root.comConfirmData.layer = 0;
      }
    },
    doAlert() {
      this.$root.comAlertData.layer = 1;
      this.$root.comAlertData.info = "你的验证码是: 1756";
    },
    doToast() {
      this.$root.comToastData.layer = 1;
      this.$root.comToastData.info = "Toast演示";
    },
    doLoading() {
      this.$root.comLoadingData.layer = 1;
      window.setTimeout(()=> {
        this.$root.comLoadingData.layer = 0;
      }, 3000);
    },
    doProgress() {
      this.$root.comProgressData.layer = 1;
      window.setTimeout(()=> {
        this.$root.comProgressData.layer = 0; 
      }, 3000);
    },
    doPageLoading() {
      this.$root.comPageLoadingData.layer = 1;
      this.$root.comPageLoadingData.callback = ()=> {
        this.$root.comPageLoadingData.layer = 1;
        window.setTimeout(()=> {
          this.$root.comPageLoadingData.layer = 0;
        }, 2000); 
      };
      window.setTimeout(()=> {
        this.$root.comPageLoadingData.layer = 2; 
      }, 3000);
    },

    //选择框
    radioHItem() {
      this.radioVItem = !this.radioVItem;
    },
    radioHList(idx) {
      this.radioVList.forEach((value)=> {
        value.active = false; 
      });
      this.radioVList[idx].active = true; 
    },
    checkBoxHList(idx) {
      this.checkBoxVList[idx].active = !this.checkBoxVList[idx].active;
    }

  }
}