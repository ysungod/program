import Vue from 'vue/dist/vue.min.js'; 
import VueRouter from 'vue-router';
import ysun from 'ysun';

Vue.use(VueRouter);
Vue.use(ysun);
Vue.config.productionTip = false;

import App from './App.vue'; 

import router from "@/router/index.js";

//公共函数
import plugin from "./../plugin.js"; 
window['plug'] = plugin;   
Vue.prototype.plug = plugin;
plug.run();
//组件
import './../library/component.js';
 
new Vue({ 
  render: h => h(App),
  router,
  data () {
    return {
      name: "main.js",
      comPageLoadingData: {
        layer: 0,
        info: ""
      },
      comToastData: {
        layer: 0,
        info: ""
      },
      comLoadingData: {
        layer: 0,
        info: ""
      },
      comProgressData: {
        layer: 0,
        info: ""
      },
      comAlertData: {
        layer: 0, 
        title: "", 
        info: ""
      },
      comConfirmData: {
        layer: 0, 
        title: "", 
        info: ""
      } 
    }
  },
  created: function() {
    console.log(ysun.ysun_name);
  }
}).$mount('#app');  
