import VueRouter from 'vue-router';
  
//调试
const ysun = r => require.ensure([], () => r(require('@/components/ysun/index.vue')), 'ysun');

//登录注册
const account = r => require.ensure([], () => r(require('@/components/account/frame.vue')), 'account');
const account_login = r => require.ensure([], () => r(require('@/components/account/login.vue')), 'account_login');
const account_regcode = r => require.ensure([], () => r(require('@/components/account/regcode.vue')), 'account_regcode');
const account_register = r => require.ensure([], () => r(require('@/components/account/register.vue')), 'account_register');
const account_forcode = r => require.ensure([], () => r(require('@/components/account/forcode.vue')), 'account_forcode');
const account_forget = r => require.ensure([], () => r(require('@/components/account/forget.vue')), 'account_forget');

//Web APP主页
const home = r => require.ensure([], () => r(require('@/components/home/frame.vue')), 'home');
const home_news = r => require.ensure([], () => r(require('@/components/home/news.vue')), 'home_news');
const home_goods = r => require.ensure([], () => r(require('@/components/home/goods.vue')), 'home_goods');
const home_nearby = r => require.ensure([], () => r(require('@/components/home/nearby.vue')), 'home_nearby');
const home_service = r => require.ensure([], () => r(require('@/components/home/service.vue')), 'home_service');
const home_me = r => require.ensure([], () => r(require('@/components/home/me.vue')), 'home_me');

//IM通信
const im = r => require.ensure([], () => r(require('@/components/im/frame.vue')), 'im');
const im_dialog = r => require.ensure([], () => r(require('@/components/im/dialog.vue')), 'im_dialog');

//商品
const goods = r => require.ensure([], () => r(require('@/components/goods/frame.vue')), 'goods');
const goods_index = r => require.ensure([], () => r(require('@/components/goods/index.vue')), 'goods_index');
const goods_list = r => require.ensure([], () => r(require('@/components/goods/list.vue')), 'goods_list');
const goods_select = r => require.ensure([], () => r(require('@/components/goods/select.vue')), 'goods_select');

//服务
const service = r => require.ensure([], () => r(require('@/components/service/frame.vue')), 'service');
const service_index = r => require.ensure([], () => r(require('@/components/service/index.vue')), 'service_index');
const service_list = r => require.ensure([], () => r(require('@/components/service/list.vue')), 'service_list');
const service_select = r => require.ensure([], () => r(require('@/components/service/select.vue')), 'service_select');

const routes = [
  {path: '/', redirect: "/home/news"},

  {name: "ysun", path: '/ysun', component: ysun}, 

  {
    path: "/goods", component: goods,
    children: [ 
      {name: "goodsIndex", path: "index", meta: {index: 2}, component: goods_index},
      {name: "goodsList", path: "list", meta: {index: 1}, component: goods_list},
      {name: "goodsSelect", path: "select", meta: {index: 1}, component: goods_select}
    ] 
  },

  {
    path: "/service", component: service,
    children: [ 
      {name: "serviceIndex", path: "index", meta: {index: 2}, component: service_index},
      {name: "serviceList", path: "list", meta: {index: 1}, component: service_list},
      {name: "serviceSelect", path: "select", meta: {index: 1}, component: service_select}
    ] 
  },

  {
    path: "/account", component: account, 
    children: [  
      {name: "login", path: 'login', meta: {index: 1}, component: account_login},
      {name: "regcode", path: 'regcode', meta: {index: 2}, component: account_regcode},
      {name: "register", path: 'register', meta: {index: 3}, component: account_register},
      {name: "forcode", path: 'forcode', meta: {index: 2}, component: account_forcode},
      {name: "forget", path: 'forget', meta: {index: 3}, component: account_forget}
    ]
  }, 
  
  {
    path: '/home', redirect: "/home/news", component: home, 
    children: [
      {name: "homeNews", path: "news", meta: {index: 0}, component: home_news},
      {name: "homeGoods", path: "goods", meta: {index: 0}, component: home_goods},
      {name: "homeService", path: "service", meta: {index: 0}, component: home_service},
      {name: "homeNearby", path: "nearby", meta: {index: 0}, component: home_nearby},
      {name: "homeMe", path: "me", meta: {index: 0}, component: home_me}
    ]
  },

  {
    path: "/im", component: im,
    children: [
      {name: "imDialog", path: "dialog", meta: {index: 1}, component: im_dialog}
    ] 
  }

];

 
export default new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: routes 
});