import axios from 'axios';
import * as dom from "./library/dom.js";

export default {
  name: "公共函数",
  baseIm: "http://127.0.0.1:8011/",  //API接口路由
  regPhone: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/,

  run() {
    //console.log(this);

  },

  //引入dom.js操作DOM
  ...dom,

   
  //判断空字符串
  judgeString(content) {
    let arr = content || "";
    if (arr.length == 0) {
      return false;
    }
    let reg = arr.replace(/\s/g, "");
    if (reg.length == 0) {
      return false;
    }
    return true;
  },

  //判断值
  judgeValid(varible) {
    if (typeof varible == "number") {
      return true;
    }
    if (varible == undefined || varible == "undefined" || varible == null || varible == "null" || varible == "") {
      return false;
    } 
    return true;
  }, 

  //判断对象
  judgeObject(obj) {
    if (!this.judgeValid(obj)) {
      return false;
    }
    for (let i in obj) {
      return true;
    }
    return false;
  },

  
  /*
   *@DOM
   *-----------------------------------------------------------------------------
   *-----------------------------------------------------------------------------
   *-----------------------------------------------------------------------------
   */










  /*
  *@Funcc: ajajx交互数据
  *@Case
  plug.ajax({ 
    object: this,
    url: "",
    type: "get",
    data: {},
    login: (res)=> {},
    exception: (res)=> {},
    success: (res)=> {},
    error: (err)=> {}
  }); 
  */
  ajax(args) {
    if (!args) {
      console.log("请传入函数参数");
      return ;
    } 
    let self = args.object;

    let dataJSON = {
      method: "post",
      url: this.baseIm + (args.url ? args.url : ""),
      timeout: 10000,
      headers: {} 
    };
    if (this.judgeValid(args.type)) {
      dataJSON["method"] = args.type;
    }
    if (this.judgeObject(args.header)) {
      let headers = {};
      for (let key in args.header) {
        headers[key] = args.header[key];
      }
      dataJSON["headers"] = headers;
    }
    let reg = /^[0-9]*$/g;
    if (this.judgeValid(args.timeout) && reg.test(args.timeout)) {
      dataJSON.timeout = args.timeout;
    }

    if (dataJSON.method == "get") {
      dataJSON.params = args.data || {};
    } 
    if (dataJSON.method == "post") {  
      dataJSON.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      dataJSON.data = require("qs").stringify(args.data) || {};
    }  
    axios(dataJSON).then((res) => {
      self.$root.comProgressData.layer = 0; 
      self.$root.comLoadingData.layer = 0;

      let data = res.data;

      //异常
      if (data.state == 1) {
        if (args.exception) {
          args.exception(data);
          return ;
        }
        if (this.judgeValid(data.msg)) {
          self.$root.comToastData.layer = 1; 
          self.$root.comToastData.info = data.msg;
        } 
        return ; 
      }
      //未登录
      if (data.state == 2) {
        if (self.judgeValid(data.msg)) {
          self.$root.comToastData.layer = 1; 
          self.$root.comToastData.info = data.msg;
        }
        window.setTimeout(()=> {
          self.$root.comToastData.layer = 0; 
          if (args.login) {
            args.login(data);
          } 
        }, 3000);   
        return ;
      }
      //业务成功  
      if (args.success) {
        args.success(data);
      }
    }).catch((err) => {
      if (args.error) {
        args.error(err);
        return ;
      }

      self.$root.comProgressData.layer = 0; 
      self.$root.comLoadingData.layer = 0; 

      if (err.constructor === Error) {
        if (err.code == "ECONNABORTED") {
          self.$root.comToastData.info = "数据通信超时";
          self.$root.comToastData.layer = 1;
          return ; 
        }   

        if (this.judgeValid(err.response)) {
          if (err.response.status == 404) {
            self.$root.comToastData.info = "数据响应异常";
            self.$root.comToastData.layer = 1;
            return;  
          }
        }
        self.$root.comToastData.info = "服务器出现异常";
        self.$root.comToastData.layer = 1;
        return ;
      }
      self.$root.comToastData.info = "网络环境出现异常";
      self.$root.comToastData.layer = 1;
    });
  }
}