/**
  * ysungod v1.0.0
  * (c) 2018 Ysun
  * @license MIT
  */
(function(global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.VueRouter = factory());
})(this, function() { 'use strict';





});