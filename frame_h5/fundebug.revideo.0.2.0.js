var _0x4f3a = ["_compress", "val", "fundebug", "revideo", "monitorRedo", "redoIntervalId", "sequence", "singleFragmentMaxLength", "Invalid request option. invalid attribute name: ", "ID_PROP", "prototype", "removedFromParent", "childList", "anyCharacterDataChanged", "STAYED_OUT", "visitNode", "ENTERED", "REPARENTED", "added", "length", "treeChanges", "push", "computeMatchabilityChange", "nodeType", "mutations", "get", "has", "attributeChangedNodes", "attribute", "getOldPreviousSibling", "uid", "getAttributeOldValue", "EXITED", "match", "callback", "calcReordered", "some", "options", "characterDataOldValue", "Invalid request option. all has no options.", "elementAttributes", "createSummaries", "isSmallContent", "actionData", "toLowerCase", "Cannot call a class as a function", "key", "cache", "configurable", "__esModule", "previousElementSibling", "previousSibling", "default", "hasId", "Super expression must either be null or a function, not ", "resize", "top", "defaultEventCallback", "mousemoveSamplingFrequence", "parseFromString", "__proto__", "onEvents", "_events", "trace", "listener", "timestamp", "frames", "left", "cursorX", "height", "replace", "constructor", "Invalid typed array length", '"value" argument must not be a number', "'length' is out of bounds", "toString", "function", "ucs-2", "call", "readInt16BE", "writeUIntLE", "pow", "TYPED_ARRAY_SUPPORT", "writable", "vedio", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", "decompress"]; !
function(t, e) { !
	function(e) {
		for (; --e;) t.push(t.shift())
	} (++e)
} (_0x4f3a, 350);
var _0x2654 = function(e, t) {
	return _0x4f3a[e -= 0]
}; !
function(e, t) {
	"object" == typeof exports && "object" == typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? exports.revideo = t() : e.revideo = t()
} (this,
function() {
	return function(r) {
		function n(e) {
			if (i[e]) return i[e].exports;
			var t = i[e] = {
				exports: {},
				id: e,
				loaded: !1
			};
			return r[e].call(t.exports, t, t.exports, n),
			t.loaded = !0,
			t.exports
		}
		var i = {};
		return n.m = r,
		n.c = i,
		n.p = "",
		n(0)
	} ([function(e, t, r) {
		"use strict";
		document.addEventListener && document.addEventListener("DOMContentLoaded",
		function() {
			if (!document.all && !fundebug.silentVideo) {
				var e = r(1);
				if (e) {
					var t = new e("fundebug-revideo", {});
					t.init(),
					window[_0x2654("0x0")] && (window.fundebug[_0x2654("0x1")] = t)
				}
			}
		})
	},
	function(e, t, r) {
		"use strict";
		function n(e) {
			return e && e.__esModule ? e: {
				"default": e
			}
		}
		var i = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e.prototype, t),
				r && n(e, r),
				e
			}
		} (),
		o = n(r(2)),
		a = n(r(3)),
		h = n(r(12)),
		l = n(r(6)),
		f = n(r(13)),
		s = n(r(14)),
		u = n(r(25)),
		d = n(r(4)),
		p = n(r(5)),
		c = n(r(26)),
		v = n(r(27)),
		g = function() {
			function r(e, t) { (function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				})(this, r),
				this.version = "0.2.0",
				this.token = e,
				this.url = "http://localhost:3000/summary",
				this.singleFragmentMaxLength = 1e3,
				this.multipleFragmentMaxLength = 2e3,
				this.MAXESTLENGTH = 1e4,
				this.maxTimeSpan = 2e4,
				this.timeSpanCount = 0,
				this.lengthOverflowCheckTimeSpan = 5e3,
				this.options = t,
				this.recorder = null,
				this.redoIntervalId = null,
				this.observer = null,
				this.vedio = null,
				this.sequence = [],
				this.actions = []
			}
			return i(r, [{
				key: "send",
				value: function() {
					this.pushFragmentToSequence(0);
					var e = {
						token: this.token,
						sequence: this.sequence
					},
					t = JSON.stringify(e);
					c["default"].sendToServer(this.url, t)
				}
			},
			{
				key: "fetchSequence",
				value: function() {
					this.pushFragmentToSequence(0);
					var e = [];
					try {
						e = JSON.stringify(this.sequence)
					} catch(r) {}
					var t = "";
					try {
						t = v["default"].compressToUTF16(e)
					} catch(n) {}
					return t
				}
			},
			{
				key: "getSequence",
				value: function() {
					return this.sequence
				}
			},
			{
				key: "init",
				value: function() {
					this.initRecorder(),
					this.initAction(),
					this[_0x2654("0x2")]()
				}
			},
			{
				key: "initRecorder",
				value: function() {
					this.vedio = null,
					this.recorder && this.recorder.stopRecording(),
					this.recorder = new s["default"].Recorder({
						target: document.documentElement
					});
					var t = this;
					this.recorder.on("fun_recording",
					function(e) {
						t.vedio = t.recorder.getVedio()
					}),
					this.recorder.start()
				}
			},
			{
				key: "initAction",
				value: function() {
					this.actions = [],
					this.initMutationObserver(),
					this.initInput()
				}
			},
			{
				key: "initMutationObserver",
				value: function() {
					this.observer && this.observer.disconnect();
					var r = this;
					try {
						this.observer = new o["default"]({
							callback: function(e) {
								r.addInputListenterToNodeList(e);
								var t = a["default"].parseMutations(e);
								r.actions = r.actions.concat(t)
							},
							queries: [{
								all: !0
							}],
							oldPreviousSibling: !0
						})
					} catch(e) {
						return null
					}
				}
			},
			{
				key: "initInput",
				value: function() {
					this.injectAllInputIn(document)
				}
			},
			{
				key: "injectAllInputIn",
				value: function(u) {
					var c = this; ["input", "textarea", "select"].map(function(e) {
						function n(e, t, r, n) {
							var i = new h["default"](t, e.type, r),
							o = new p["default"],
							a = new p["default"],
							s = new f["default"](e, i, o, a),
							u = new l["default"](4, s);
							n.actions.push(u)
						}
						if (u && u.querySelectorAll) {
							var t = u.querySelectorAll(e),
							r = d["default"].fromList(t);
							0 !== r.length && (i = e, o = r, a = {},
							s = setInterval(function() {
								0 === (o = o.filter(function(e) {
									return e.funRemoved && delete a[e.funId],
									!e.funRemoved
								})).length && clearInterval(s),
								o.map(function(e) {
									if (!e || "password" !== e.type) {
										var t;
										t = "radio" === e.type || "checkbox" === e.type ? e.checked: e.value;
										var r = e.funId;
										if (r) {
											if (a[r] === t) return;
											a[e.funId] = t,
											n(e, i, t, c)
										} else e.funId = d["default"].uniqueID(),
										a[e.funId] = t,
										n(e, i, t, c)
									}
								})
							},
							100))
						}
						var i, o, a, s
					})
				}
			}, {
				key: "addInputListenterToNodeList",
				value: function(e) {
					var r = this;
					e.map(function(e) {
						var t = a["default"].extractCleanNodes(e.added); (e.added = t).map(function(e) {
							r.injectAllInputIn(e)
						})
					})
				}
			},
			{
				key: "monitorRedo",
				value: function() {
					this[_0x2654("0x3")] && window.clearInterval(this.redoIntervalId);
					var e = this;
					this.redoIntervalId = setInterval(function() {
						if (e.timeSpanCount++, 2 <= e.timeSpanCount) return e.timeSpanCount = 0,
						1 < e.sequence.length && e[_0x2654("0x4")].shift(),
						void e.pushFragmentToSequence(0); (e.vedio && e.vedio.getFramesLength()) + e.actions.length > e[_0x2654("0x5")] && e.pushFragmentToSequence(0)
					},
					this.lengthOverflowCheckTimeSpan)
				}
			},
			{
				key: "pushFragmentToSequence",
				value: function(e) {
					this.vedio = this.recorder.getVedio();
					var t = new u["default"](e, this.vedio, this.actions);
					this.sequence.push(t),
					this.checkLengthToshiftSequence(),
					this.init()
				}
			},
			{
				key: "checkLengthToshiftSequence",
				value: function() {
					var e = this.calculateTotalLength();
					e > this.multipleFragmentMaxLength && (1 < this.calculateSequenceLen() || e > this.MAXESTLENGTH) && this.sequence.shift()
				}
			},
			{
				key: "calculateTotalLength",
				value: function() {
					return this.sequence.reduce(function(e, t) {
						return e + t.getVedioFrameLength() + t.getActionsLength()
					},
					0)
				}
			},
			{
				key: "calculateSequenceLen",
				value: function() {
					return this.sequence.length
				}
			},
			{
				key: "clear",
				value: function() {
					window.clearInterval(this.redoIntervalId),
					this.sequence = [],
					this.vedio = null,
					this.actions = [],
					this.recorder = null,
					this.redoIntervalId = null
				}
			}]),
			r
		} ();
		e.exports = g
	},
	function(e, t) {
		function r(e) {
			return '"' + e.replace(/"/, '\\"') + '"'
		}
		function u(e) {
			if ("string" != typeof e) throw Error("Invalid request opion. attribute must be a non-zero length string.");
			if (! (e = e.trim())) throw Error("Invalid request opion. attribute must be a non-zero length string.");
			if (!e.match(m)) throw Error(_0x2654("0x6") + e);
			return e
		}
		function c(e) {
			if (!e.trim().length) throw Error("Invalid request option: elementAttributes must contain at least one attribute.");
			for (var t = {},
			r = {},
			n = e.split(/\s+/), i = 0; i < n.length; i++) {
				if (o = n[i]) {
					var o, a = (o = u(o)).toLowerCase();
					if (t[a]) throw Error("Invalid request option: observing multiple case variations of the same attribute is not supported.");
					r[o] = !0,
					t[a] = !0
				}
			}
			return Object.keys(r)
		}
		var n, i = this.__extends ||
		function(e, t) {
			function r() {
				this.constructor = e
			}
			for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
			r.prototype = t.prototype,
			e.prototype = new r
		};
		"undefined" != typeof WebKitMutationObserver ? n = WebKitMutationObserver: document.all || (n = MutationObserver);
		var d, o, h = function() {
			function r() {
				this.nodes = [],
				this.values = []
			}
			return r.prototype.isIndex = function(e) {
				return + e == e >>> 0
			},
			r.prototype.nodeId = function(e) {
				var t = e[r[_0x2654("0x7")]];
				return t || (t = e[r.ID_PROP] = r.nextId_++),
				t
			},
			r.prototype.set = function(e, t) {
				var r = this.nodeId(e);
				this.nodes[r] = e,
				this.values[r] = t
			},
			r.prototype.get = function(e) {
				var t = this.nodeId(e);
				return this.values[t]
			},
			r.prototype.has = function(e) {
				return this.nodeId(e) in this.nodes
			},
			r.prototype["delete"] = function(e) {
				var t = this.nodeId(e);
				delete this.nodes[t],
				this.values[t] = void 0
			},
			r.prototype.keys = function() {
				var e = [];
				for (var t in this.nodes) this.isIndex(t) && e.push(this.nodes[t]);
				return e
			},
			r.ID_PROP = "__mutation_summary_node_map_id__",
			r.nextId_ = 1,
			r
		} (); (o = d || (d = {}))[o.STAYED_OUT = 0] = "STAYED_OUT",
		o[o.ENTERED = 1] = "ENTERED",
		o[o.STAYED_IN = 2] = "STAYED_IN",
		o[o.REPARENTED = 3] = "REPARENTED",
		o[o.REORDERED = 4] = "REORDERED",
		o[o.EXITED = 5] = "EXITED";
		var s = function() {
			function e(e, t, r, n, i, o, a, s) {
				void 0 === t && (t = !1),
				void 0 === r && (r = !1),
				void 0 === n && (n = !1),
				void 0 === i && (i = null),
				void 0 === o && (o = !1),
				void 0 === a && (a = null),
				void 0 === s && (s = null),
				this.node = e,
				this.childList = t,
				this.attributes = r,
				this.characterData = n,
				this.oldParentNode = i,
				this.added = o,
				this.attributeOldValues = a,
				this.characterDataOldValue = s,
				this.isCaseInsensitive = this.node.nodeType === Node.ELEMENT_NODE && this.node instanceof HTMLElement && this.node.ownerDocument instanceof HTMLDocument
			}
			return e.prototype.getAttributeOldValue = function(e) {
				if (this.attributeOldValues) return this.isCaseInsensitive && (e = e.toLowerCase()),
				this.attributeOldValues[e]
			},
			e[_0x2654("0x8")].getAttributeNamesMutated = function() {
				var e = [];
				if (!this.attributeOldValues) return e;
				for (var t in this.attributeOldValues) e.push(t);
				return e
			},
			e.prototype.attributeMutated = function(e, t) {
				this.attributes = !0,
				this.attributeOldValues = this.attributeOldValues || {},
				e in this.attributeOldValues || (this.attributeOldValues[e] = t)
			},
			e.prototype.characterDataMutated = function(e) {
				this.characterData || (this.characterData = !0, this.characterDataOldValue = e)
			},
			e.prototype[_0x2654("0x9")] = function(e) {
				this.childList = !0,
				this.added || this.oldParentNode ? this.added = !1 : this.oldParentNode = e
			},
			e[_0x2654("0x8")].insertedIntoParent = function() {
				this.childList = !0,
				this.added = !0
			},
			e.prototype.getOldParent = function() {
				if (this[_0x2654("0xa")]) {
					if (this.oldParentNode) return this.oldParentNode;
					if (this.added) return null
				}
				return this.node.parentNode
			},
			e
		} (),
		a = function() {
			this.added = new h,
			this.removed = new h,
			this.maybeMoved = new h,
			this.oldPrevious = new h,
			this.moved = void 0
		},
		l = function(a) {
			function e(e, t) {
				a.call(this),
				this.rootNode = e,
				this.reachableCache = void 0,
				this.wasReachableCache = void 0,
				this.anyParentsChanged = !1,
				this.anyAttributesChanged = !1,
				this.anyCharacterDataChanged = !1;
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					switch (n.type) {
					case "childList":
						this.anyParentsChanged = !0;
						for (var i = 0; i < n.removedNodes.length; i++) {
							var o = n.removedNodes[i];
							this.getChange(o).removedFromParent(n.target)
						}
						for (i = 0; i < n.addedNodes.length; i++) {
							o = n.addedNodes[i];
							this.getChange(o).insertedIntoParent()
						}
						break;
					case "attributes":
						this.anyAttributesChanged = !0,
						this.getChange(n.target).attributeMutated(n.attributeName, n.oldValue);
						break;
					case "characterData":
						this[_0x2654("0xb")] = !0,
						this.getChange(n.target).characterDataMutated(n.oldValue)
					}
				}
			}
			return i(e, a),
			e.prototype.getChange = function(e) {
				var t = this.get(e);
				return t || (t = new s(e), this.set(e, t)),
				t
			},
			e.prototype.getOldParent = function(e) {
				var t = this.get(e);
				return t ? t.getOldParent() : e.parentNode
			},
			e.prototype.getIsReachable = function(e) {
				if (e === this.rootNode) return ! 0;
				if (!e) return ! 1;
				this.reachableCache = this.reachableCache || new h;
				var t = this.reachableCache.get(e);
				return void 0 === t && (t = this.getIsReachable(e.parentNode), this.reachableCache.set(e, t)),
				t
			},
			e.prototype.getWasReachable = function(e, t) {
				if (200 <= ++t) return ! 1;
				if (e === this.rootNode) return ! 0;
				if (!e) return ! 1;
				this.wasReachableCache = this.wasReachableCache || new h;
				var r = this.wasReachableCache.get(e);
				return void 0 === r && (r = this.getWasReachable(this.getOldParent(e), t), this.wasReachableCache.set(e, r)),
				r
			},
			e.prototype.reachabilityChange = function(e) {
				return this.getIsReachable(e) ? this.getWasReachable(e, 0) ? d.STAYED_IN: d.ENTERED: this.getWasReachable(e, 0) ? d.EXITED: d[_0x2654("0xc")]
			},
			e
		} (h),
		f = function() {
			function e(e, t, r, n, i) {
				this.rootNode = e,
				this.mutations = t,
				this.selectors = r,
				this.calcReordered = n,
				this.calcOldPreviousSibling = i,
				this.treeChanges = new l(e, t),
				this.entered = [],
				this.exited = [],
				this.stayedIn = new h,
				this.visited = new h,
				this.childListChangeMap = void 0,
				this.characterDataOnly = void 0,
				this.matchCache = void 0,
				this.processMutations()
			}
			return e.prototype.processMutations = function() {
				if (this.treeChanges.anyParentsChanged || this.treeChanges.anyAttributesChanged) for (var e = this.treeChanges.keys(), t = 0; t < e.length; t++) this.visitNode(e[t], void 0)
			},
			e.prototype[_0x2654("0xd")] = function(e, t) {
				if (!this.visited.has(e)) {
					this.visited.set(e, !0);
					var r = this.treeChanges.get(e),
					n = t;
					if ((r && r.childList || null == n) && (n = this.treeChanges.reachabilityChange(e)), n !== d.STAYED_OUT) {
						if (this.matchabilityChange(e), n === d[_0x2654("0xe")]) this.entered.push(e);
						else if (n === d.EXITED) this.exited.push(e),
						this.ensureHasOldPreviousSiblingIfNeeded(e);
						else if (n === d.STAYED_IN) {
							var i = d.STAYED_IN;
							r && r.childList && (r.oldParentNode !== e.parentNode ? (i = d[_0x2654("0xf")], this.ensureHasOldPreviousSiblingIfNeeded(e)) : this.calcReordered && this.wasReordered(e) && (i = d.REORDERED)),
							this.stayedIn.set(e, i)
						}
						if (n !== d.STAYED_IN) for (var o = e.firstChild; o; o = o.nextSibling) this.visitNode(o, n)
					}
				}
			},
			e.prototype.ensureHasOldPreviousSiblingIfNeeded = function(e) {
				if (this.calcOldPreviousSibling) {
					this.processChildlistChanges();
					var t = e.parentNode,
					r = this.treeChanges.get(e);
					r && r.oldParentNode && (t = r.oldParentNode);
					var n = this.childListChangeMap.get(t);
					n || (n = new a, this.childListChangeMap.set(t, n)),
					n.oldPrevious.has(e) || n.oldPrevious.set(e, e.previousSibling)
				}
			},
			e.prototype.getChanged = function(e, t, r) {
				this.selectors = t,
				this.characterDataOnly = r;
				for (var n = 0; n < this.entered.length; n++) {
					var i = this.entered[n]; (s = this.matchabilityChange(i)) !== d.ENTERED && s !== d.STAYED_IN || e[_0x2654("0x10")].push(i)
				}
				var o = this.stayedIn.keys();
				for (n = 0; n < o.length; n++) {
					i = o[n];
					if ((s = this.matchabilityChange(i)) === d.ENTERED) e.added.push(i);
					else if (s === d.EXITED) e.removed.push(i);
					else if (s === d.STAYED_IN && (e.reparented || e.reordered)) {
						var a = this.stayedIn.get(i);
						e.reparented && a === d.REPARENTED ? e.reparented.push(i) : e.reordered && a === d.REORDERED && e.reordered.push(i)
					}
				}
				for (n = 0; n < this.exited[_0x2654("0x11")]; n++) {
					var s;
					i = this.exited[n]; (s = this.matchabilityChange(i)) !== d.EXITED && s !== d.STAYED_IN || e.removed.push(i)
				}
			},
			e.prototype.getOldParentNode = function(e) {
				var t = this.treeChanges.get(e);
				if (t && t.childList) return t.oldParentNode ? t.oldParentNode: null;
				var r = this.treeChanges.reachabilityChange(e);
				if (r === d.STAYED_OUT || r === d.ENTERED) throw Error("getOldParentNode requested on invalid node.");
				return e.parentNode
			},
			e.prototype.getOldPreviousSibling = function(e) {
				var t = e.parentNode,
				r = this.treeChanges.get(e);
				r && r.oldParentNode && (t = r.oldParentNode);
				var n = this.childListChangeMap.get(t);
				if (!n) throw Error("getOldPreviousSibling requested on invalid node.");
				return n.oldPrevious.get(e)
			},
			e.prototype.getOldAttribute = function(e, t) {
				var r = this[_0x2654("0x12")].get(e);
				if (!r || !r.attributes) throw Error("getOldAttribute requested on invalid node.");
				var n = r.getAttributeOldValue(t);
				if (void 0 === n) throw Error("getOldAttribute requested for unchanged attribute name.");
				return n
			},
			e.prototype.attributeChangedNodes = function(e) {
				if (!this.treeChanges.anyAttributesChanged) return {};
				var t, r;
				if (e) {
					t = {},
					r = {};
					for (var n = 0; n < e.length; n++) {
						t[l = e[n]] = !0,
						r[l.toLowerCase()] = l
					}
				}
				var i = {},
				o = this.treeChanges.keys();
				for (n = 0; n < o.length; n++) {
					var a = o[n],
					s = this.treeChanges.get(a);
					if (s.attributes && d.STAYED_IN === this.treeChanges.reachabilityChange(a) && d.STAYED_IN === this.matchabilityChange(a)) for (var u = a,
					c = s.getAttributeNamesMutated(), h = 0; h < c[_0x2654("0x11")]; h++) {
						var l = c[h];
						if (!t || t[l] || s.isCaseInsensitive && r[l]) s.getAttributeOldValue(l) !== u.getAttribute(l) && (r && s.isCaseInsensitive && (l = r[l]), i[l] = i[l] || [], i[l].push(u))
					}
				}
				return i
			},
			e.prototype.getOldCharacterData = function(e) {
				var t = this.treeChanges.get(e);
				if (!t || !t.characterData) throw Error("getOldCharacterData requested on invalid node.");
				return t.characterDataOldValue
			},
			e[_0x2654("0x8")].getCharacterDataChanged = function() {
				if (!this.treeChanges.anyCharacterDataChanged) return [];
				for (var e = this.treeChanges.keys(), t = [], r = 0; r < e.length; r++) {
					var n = e[r];
					if (d.STAYED_IN === this.treeChanges.reachabilityChange(n)) {
						var i = this.treeChanges.get(n);
						i.characterData && n.textContent != i.characterDataOldValue && t[_0x2654("0x13")](n)
					}
				}
				return t
			},
			e.prototype[_0x2654("0x14")] = function(e, t) {
				this.matchCache || (this.matchCache = []),
				this.matchCache[e.uid] || (this.matchCache[e.uid] = new h);
				var r = this.matchCache[e.uid],
				n = r.get(t);
				return void 0 === n && (n = e.matchabilityChange(t, this.treeChanges.get(t)), r.set(t, n)),
				n
			},
			e.prototype.matchabilityChange = function(e) {
				var t = this;
				if (this.characterDataOnly) switch (e[_0x2654("0x15")]) {
				case Node.COMMENT_NODE:
				case Node.TEXT_NODE:
					return d.STAYED_IN;
				default:
					return d.STAYED_OUT
				}
				if (!this.selectors) return d.STAYED_IN;
				if (e.nodeType !== Node.ELEMENT_NODE) return d.STAYED_OUT;
				for (var r = e,
				n = this.selectors.map(function(e) {
					return t.computeMatchabilityChange(e, r)
				}), i = d.STAYED_OUT, o = 0; i !== d.STAYED_IN && o < n.length;) {
					switch (n[o]) {
					case d.STAYED_IN:
						i = d.STAYED_IN;
						break;
					case d.ENTERED:
						i = i === d.EXITED ? d.STAYED_IN: d.ENTERED;
						break;
					case d.EXITED:
						i = i === d.ENTERED ? d.STAYED_IN: d.EXITED
					}
					o++
				}
				return i
			},
			e.prototype.getChildlistChange = function(e) {
				var t = this.childListChangeMap.get(e);
				return t || (t = new a, this.childListChangeMap.set(e, t)),
				t
			},
			e.prototype.processChildlistChanges = function() {
				function e(e, t) { ! e || n.oldPrevious.has(e) || n.added.has(e) || n.maybeMoved.has(e) || t && (n.added.has(t) || n.maybeMoved.has(t)) || n.oldPrevious.set(e, t)
				}
				if (!this.childListChangeMap) {
					this.childListChangeMap = new h;
					for (var t = 0; t < this.mutations.length; t++) {
						var r = this[_0x2654("0x16")][t];
						if ("childList" == r.type && (this[_0x2654("0x12")].reachabilityChange(r.target) === d.STAYED_IN || this.calcOldPreviousSibling)) {
							for (var n = this.getChildlistChange(r.target), i = r.previousSibling, o = 0; o < r.removedNodes.length; o++) {
								e(a = r.removedNodes[o], i),
								n.added.has(a) ? n.added["delete"](a) : (n.removed.set(a, !0), n.maybeMoved["delete"](a)),
								i = a
							}
							e(r.nextSibling, i);
							for (o = 0; o < r.addedNodes.length; o++) {
								var a = r.addedNodes[o];
								n.removed.has(a) ? (n.removed["delete"](a), n.maybeMoved.set(a, !0)) : n[_0x2654("0x10")].set(a, !0)
							}
						}
					}
				}
			},
			e.prototype.wasReordered = function(e) {
				function n(t) {
					if (!t) return ! 1;
					if (!i.maybeMoved.has(t)) return ! 1;
					var r = i.moved.get(t);
					return void 0 !== r || (o.has(t) ? r = !0 : (o.set(t, !0), r = function(e) {
						if (s.has(e)) return s[_0x2654("0x17")](e);
						for (var t = e.previousSibling; t && (i.added.has(t) || n(t));) t = t.previousSibling;
						return s.set(e, t),
						t
					} (t) !==
					function e(t) {
						var r = a.get(t);
						if (void 0 !== r) return r;
						for (r = i.oldPrevious.get(t); r && (i.removed[_0x2654("0x18")](r) || n(r));) r = e(r);
						return void 0 === r && (r = t.previousSibling),
						a.set(t, r),
						r
					} (t)), o.has(t) ? (o["delete"](t), i.moved.set(t, r)) : r = i.moved[_0x2654("0x17")](t)),
					r
				}
				if (!this.treeChanges.anyParentsChanged) return ! 1;
				this.processChildlistChanges();
				var t = e.parentNode,
				r = this.treeChanges.get(e);
				r && r.oldParentNode && (t = r.oldParentNode);
				var i = this.childListChangeMap.get(t);
				if (!i) return ! 1;
				if (i.moved) return i.moved.get(e);
				i.moved = new h;
				var o = new h,
				a = new h,
				s = new h;
				return i.maybeMoved.keys().forEach(n),
				i.moved.get(e)
			},
			e
		} (),
		p = function() {
			function e(e, t) {
				var r = this;
				if (this.projection = e, this.added = [], this.removed = [], this.reparented = t.all || t.element || t.characterData ? [] : void 0, this.reordered = t.all ? [] : void 0, e.getChanged(this, t.elementFilter, t.characterData), t.all || t.attribute || t.attributeList) {
					var n = t.attribute ? [t.attribute] : t.attributeList,
					i = e[_0x2654("0x19")](n);
					t.attribute ? this.valueChanged = i[t[_0x2654("0x1a")]] || [] : (this.attributeChanged = i, t.attributeList && t.attributeList.forEach(function(e) {
						r.attributeChanged.hasOwnProperty(e) || (r.attributeChanged[e] = [])
					}))
				}
				if (t.all || t.characterData) {
					var o = e.getCharacterDataChanged();
					t.characterData ? this.valueChanged = o: this.characterDataChanged = o
				}
				this.reordered && (this.getOldPreviousSibling = e.getOldPreviousSibling.bind(e))
			}
			return e.prototype.getOldParentNode = function(e) {
				return this.projection.getOldParentNode(e)
			},
			e.prototype.getOldAttribute = function(e, t) {
				return this.projection.getOldAttribute(e, t)
			},
			e.prototype.getOldCharacterData = function(e) {
				return this.projection.getOldCharacterData(e)
			},
			e.prototype[_0x2654("0x1b")] = function(e) {
				return this.projection.getOldPreviousSibling(e)
			},
			e
		} (),
		v = /[a-zA-Z_]+/,
		g = /[a-zA-Z0-9_\-]+/,
		y = function() {
			function e() {}
			return e.prototype.matches = function(e) {
				if (null === e) return ! 1;
				if (void 0 === this.attrValue) return ! 0;
				if (!this.contains) return this.attrValue == e;
				for (var t = e.split(" "), r = 0; r < t.length; r++) if (this.attrValue === t[r]) return ! 0;
				return ! 1
			},
			e.prototype.toString = function() {
				return "class" === this.attrName && this.contains ? "." + this.attrValue: "id" !== this.attrName || this.contains ? this.contains ? "[" + this.attrName + "~=" + r(this.attrValue) + "]": "attrValue" in this ? "[" + this.attrName + "=" + r(this.attrValue) + "]": "[" + this.attrName + "]": "#" + this.attrValue
			},
			e
		} (),
		b = function() {
			function f() {
				this[_0x2654("0x1c")] = f.nextUid++,
				this.qualifiers = []
			}
			return Object.defineProperty(f.prototype, "caseInsensitiveTagName", {
				get: function() {
					return this.tagName.toUpperCase()
				},
				enumerable: !0,
				configurable: !0
			}),
			Object.defineProperty(f.prototype, "selectorString", {
				get: function() {
					return this.tagName + this.qualifiers.join("")
				},
				enumerable: !0,
				configurable: !0
			}),
			f.prototype.isMatching = function(e) {
				return e[f.matchesSelector](this.selectorString)
			},
			f.prototype.wasMatching = function(e, t, r) {
				if (!t || !t.attributes) return r;
				var n = t.isCaseInsensitive ? this.caseInsensitiveTagName: this.tagName;
				if ("*" !== n && n !== e.tagName) return ! 1;
				for (var i = [], o = !1, a = 0; a < this.qualifiers.length; a++) {
					var s = this.qualifiers[a],
					u = t[_0x2654("0x1d")](s.attrName);
					i.push(u),
					o = o || void 0 !== u
				}
				if (!o) return r;
				for (a = 0; a < this.qualifiers[_0x2654("0x11")]; a++) {
					s = this.qualifiers[a];
					if (void 0 === (u = i[a]) && (u = e.getAttribute(s.attrName)), !s.matches(u)) return ! 1
				}
				return ! 0
			},
			f.prototype.matchabilityChange = function(e, t) {
				var r = this.isMatching(e);
				return r ? this.wasMatching(e, t, r) ? d.STAYED_IN: d.ENTERED: this.wasMatching(e, t, r) ? d[_0x2654("0x1e")] : d.STAYED_OUT
			},
			f.parseSelectors = function(e) {
				function t() {
					n && (i && (n.qualifiers.push(i), i = void 0), a.push(n)),
					n = new f
				}
				function r() {
					i && n.qualifiers.push(i),
					i = new y
				}
				for (var n, i, o, a = [], s = /\s/, u = "Invalid or unsupported selector syntax.", c = 1, h = 0; h < e.length;) {
					var l = e[h++];
					switch (c) {
					case 1:
						if (l.match(v)) {
							t(),
							n.tagName = l,
							c = 2;
							break
						}
						if ("*" == l) {
							t(),
							n.tagName = "*",
							c = 3;
							break
						}
						if ("." == l) {
							t(),
							r(),
							n.tagName = "*",
							i.attrName = "class",
							i.contains = !0,
							c = 4;
							break
						}
						if ("#" == l) {
							t(),
							r(),
							n.tagName = "*",
							i.attrName = "id",
							c = 4;
							break
						}
						if ("[" == l) {
							t(),
							r(),
							n.tagName = "*",
							i.attrName = "",
							c = 6;
							break
						}
						if (l.match(s)) break;
						throw Error(u);
					case 2:
						if (l.match(g)) {
							n.tagName += l;
							break
						}
						if ("." == l) {
							r(),
							i.attrName = "class",
							i.contains = !0,
							c = 4;
							break
						}
						if ("#" == l) {
							r(),
							i.attrName = "id",
							c = 4;
							break
						}
						if ("[" == l) {
							r(),
							i.attrName = "",
							c = 6;
							break
						}
						if (l.match(s)) {
							c = 14;
							break
						}
						if ("," == l) {
							c = 1;
							break
						}
						throw Error(u);
					case 3:
						if ("." == l) {
							r(),
							i.attrName = "class",
							i.contains = !0,
							c = 4;
							break
						}
						if ("#" == l) {
							r(),
							i.attrName = "id",
							c = 4;
							break
						}
						if ("[" == l) {
							r(),
							i.attrName = "",
							c = 6;
							break
						}
						if (l.match(s)) {
							c = 14;
							break
						}
						if ("," == l) {
							c = 1;
							break
						}
						throw Error(u);
					case 4:
						if (l.match(v)) {
							i.attrValue = l,
							c = 5;
							break
						}
						throw Error(u);
					case 5:
						if (l.match(g)) {
							i.attrValue += l;
							break
						}
						if ("." == l) {
							r(),
							i.attrName = "class",
							i.contains = !0,
							c = 4;
							break
						}
						if ("#" == l) {
							r(),
							i.attrName = "id",
							c = 4;
							break
						}
						if ("[" == l) {
							r(),
							c = 6;
							break
						}
						if (l.match(s)) {
							c = 14;
							break
						}
						if ("," == l) {
							c = 1;
							break
						}
						throw Error(u);
					case 6:
						if (l.match(v)) {
							i.attrName = l,
							c = 7;
							break
						}
						if (l.match(s)) break;
						throw Error(u);
					case 7:
						if (l.match(g)) {
							i.attrName += l;
							break
						}
						if (l.match(s)) {
							c = 8;
							break
						}
						if ("~" == l) {
							i.contains = !0,
							c = 9;
							break
						}
						if ("=" == l) {
							i.attrValue = "",
							c = 11;
							break
						}
						if ("]" == l) {
							c = 3;
							break
						}
						throw Error(u);
					case 8:
						if ("~" == l) {
							i.contains = !0,
							c = 9;
							break
						}
						if ("=" == l) {
							i.attrValue = "",
							c = 11;
							break
						}
						if ("]" == l) {
							c = 3;
							break
						}
						if (l.match(s)) break;
						throw Error(u);
					case 9:
						if ("=" == l) {
							i.attrValue = "",
							c = 11;
							break
						}
						throw Error(u);
					case 10:
						if ("]" == l) {
							c = 3;
							break
						}
						if (l.match(s)) break;
						throw Error(u);
					case 11:
						if (l.match(s)) break;
						if ('"' == l || "'" == l) {
							o = l,
							c = 13;
							break
						}
						i.attrValue += l,
						c = 12;
						break;
					case 12:
						if (l[_0x2654("0x1f")](s)) {
							c = 10;
							break
						}
						if ("]" == l) {
							c = 3;
							break
						}
						if ("'" == l || '"' == l) throw Error(u);
						i.attrValue += l;
						break;
					case 13:
						if (l == o) {
							c = 10;
							break
						}
						i.attrValue += l;
						break;
					case 14:
						if (l.match(s)) break;
						if ("," == l) {
							c = 1;
							break
						}
						throw Error(u)
					}
				}
				switch (c) {
				case 1:
				case 2:
				case 3:
				case 5:
				case 14:
					t();
					break;
				default:
					throw Error(u)
				}
				if (!a.length) throw Error(u);
				return a
			},
			f.nextUid = 1,
			f.matchesSelector = "function" == typeof(e = document.createElement("div")).webkitMatchesSelector ? "webkitMatchesSelector": "function" == typeof e.mozMatchesSelector ? "mozMatchesSelector": "function" == typeof e.msMatchesSelector ? "msMatchesSelector": "matchesSelector",
			f;
			var e
		} (),
		m = /^([a-zA-Z:_]+[a-zA-Z0-9_\-:\.]*)$/,
		w = function() {
			function s(e) {
				if (void 0 === n) throw new Error("No Engine");
				var t = this;
				this.connected = !1,
				this.options = s.validateOptions(e),
				this.observerOptions = s.createObserverOptions(this.options.queries),
				this.root = this.options.rootNode,
				this.callback = this.options[_0x2654("0x20")],
				this.elementFilter = Array.prototype.concat.apply([], this.options.queries.map(function(e) {
					return e.elementFilter ? e.elementFilter: []
				})),
				this.elementFilter.length || (this.elementFilter = void 0),
				this[_0x2654("0x21")] = this.options.queries[_0x2654("0x22")](function(e) {
					return e.all
				}),
				this.queryValidators = [],
				s.createQueryValidator && (this.queryValidators = this[_0x2654("0x23")].queries.map(function(e) {
					return s.createQueryValidator(t.root, e)
				})),
				this.observer = new n(function(e) {
					t.observerCallback(e)
				}),
				this.reconnect()
			}
			return s.createObserverOptions = function(e) {
				function i(e) {
					if (!o.attributes || t) {
						if (o.attributes = !0, o.attributeOldValue = !0, !e) return void(t = void 0);
						t = t || {},
						e.forEach(function(e) {
							t[e] = !0,
							t[e.toLowerCase()] = !0
						})
					}
				}
				var t, o = {
					childList: !0,
					subtree: !0
				};
				return e.forEach(function(e) {
					if (e.characterData) return o.characterData = !0,
					void(o.characterDataOldValue = !0);
					if (e.all) return i(),
					o.characterData = !0,
					void(o[_0x2654("0x24")] = !0);
					if (e.attribute) i([e.attribute.trim()]);
					else {
						var t, r, n = (t = e.elementFilter, r = {},
						t.forEach(function(e) {
							e.qualifiers.forEach(function(e) {
								r[e.attrName] = !0
							})
						}), Object.keys(r)).concat(e.attributeList || []);
						n.length && i(n)
					}
				}),
				t && (o.attributeFilter = Object.keys(t)),
				o
			},
			s.validateOptions = function(e) {
				for (var t in e) if (! (t in s.optionKeys)) throw Error("Invalid option: " + t);
				if ("function" != typeof e.callback) throw Error("Invalid options: callback is required and must be a function");
				if (!e.queries || !e.queries.length) throw Error("Invalid options: queries must contain at least one query request object.");
				for (var r = {
					callback: e.callback,
					rootNode: e.rootNode || document,
					observeOwnChanges: !!e.observeOwnChanges,
					oldPreviousSibling: !!e.oldPreviousSibling,
					queries: []
				},
				n = 0; n < e.queries.length; n++) {
					var i = e.queries[n];
					if (i.all) {
						if (1 < Object.keys(i).length) throw Error(_0x2654("0x25"));
						r.queries.push({
							all: !0
						})
					} else if ("attribute" in i) {
						if ((a = {
							attribute: u(i.attribute)
						}).elementFilter = b.parseSelectors("*[" + a.attribute + "]"), 1 < Object.keys(i).length) throw Error("Invalid request option. attribute has no options.");
						r.queries.push(a)
					} else if ("element" in i) {
						var o = Object.keys(i).length,
						a = {
							element: i.element,
							elementFilter: b.parseSelectors(i.element)
						};
						if (i.hasOwnProperty(_0x2654("0x26")) && (a.attributeList = c(i.elementAttributes), o--), 1 < o) throw Error("Invalid request option. element only allows elementAttributes option.");
						r.queries.push(a)
					} else {
						if (!i.characterData) throw Error("Invalid request option. Unknown query request.");
						if (1 < Object.keys(i).length) throw Error("Invalid request option. characterData has no options.");
						r.queries.push({
							characterData: !0
						})
					}
				}
				return r
			},
			s.prototype.createSummaries = function(e) {
				if (!e || !e.length) return [];
				for (var t = new f(this.root, e, this.elementFilter, this.calcReordered, this.options.oldPreviousSibling), r = [], n = 0; n < this.options.queries.length; n++) r.push(new p(t, this.options.queries[n]));
				return r
			},
			s.prototype.checkpointQueryValidators = function() {
				this.queryValidators.forEach(function(e) {
					e && e.recordPreviousState()
				})
			},
			s.prototype.runQueryValidators = function(r) {
				this.queryValidators.forEach(function(e, t) {
					e && e.validate(r[t])
				})
			},
			s.prototype.changesToReport = function(e) {
				return e.some(function(t) {
					if (["added", "removed", "reordered", "reparented", "valueChanged", "characterDataChanged"].some(function(e) {
						return t[e] && t[e].length
					})) return ! 0;
					if (t.attributeChanged && Object.keys(t.attributeChanged).some(function(e) {
						return !! t.attributeChanged[e].length
					})) return ! 0;
					return ! 1
				})
			},
			s.prototype.observerCallback = function(e) {
				this.options.observeOwnChanges || this.observer.disconnect();
				var t = this[_0x2654("0x27")](e);
				this.runQueryValidators(t),
				this.options.observeOwnChanges && this.checkpointQueryValidators(),
				this.changesToReport(t) && this.callback(t),
				!this.options.observeOwnChanges && this.connected && (this.checkpointQueryValidators(), this.observer.observe(this.root, this.observerOptions))
			},
			s.prototype.reconnect = function() {
				if (this.connected) throw Error("Already connected");
				this.observer.observe(this.root, this.observerOptions),
				this.connected = !0,
				this.checkpointQueryValidators()
			},
			s.prototype.takeSummaries = function() {
				if (!this.connected) throw Error("Not connected");
				var e = this.createSummaries(this.observer.takeRecords());
				return this.changesToReport(e) ? e: void 0
			},
			s.prototype.disconnect = function() {
				var e = this.takeSummaries();
				return this.observer.disconnect(),
				this.connected = !1,
				e
			},
			s.NodeMap = h,
			s.parseElementFilter = b.parseSelectors,
			s.optionKeys = {
				callback: !0,
				queries: !0,
				rootNode: !0,
				oldPreviousSibling: !0,
				observeOwnChanges: !0
			},
			s
		} ();
		e.exports = w
	},
	function(e, t, r) {
		"use strict";
		function n(e) {
			return e && e.__esModule ? e: {
				"default": e
			}
		}
		function i(e) {
			return function(o) {
				var e = o.length;
				if (e <= 1) return o;
				o.map(function(e) {
					e.funContent = h(e)
				});
				var a = 0;
				o.map(function(e, t) {
					if (0 !== t) {
						var r = o[a].funContent,
						n = o[t - 1].funContent,
						i = e.funContent;
						return n === i || r === i ? void(a = t) : void(r && -1 != r.indexOf(i) ? e[_0x2654("0x28")] = !0 : (i && -1 != i.indexOf(r) && (o[a].isSmallContent = !0), a = t))
					}
				}),
				o = o.filter(function(e) {
					return 1 != e.isSmallContent
				}),
				e = o.length;
				for (var t = 0; t < e; t++) if (!o[t].isSmallContent) {
					var r = 0;
					e: for (var n = t + 1; n < e; n++) if (!o[n].isSmallContent) {
						var i = o[t].funContent,
						s = o[n].funContent;
						if (i !== s) {
							if (i && -1 != i.indexOf(s)) o[n].isSmallContent = !0;
							else if (s && -1 != s.indexOf(i)) break e;
							if (300 < ++r) break e
						}
					}
				}
				for (var u = [], c = 0; c < e; c++) o[c].isSmallContent || u.push(o[c]),
				o[c].isSmallContent = !1;
				return u
			} (e)
		}
		function l(n, e, t) {
			return 0 === t.length ? [] : (0 != e && (t = i(t)), 1 === e && t.map(function(e) {
				var t = n.getOldPreviousSibling(e);
				e.oldPreviousSibling = t;
				var r = n.getOldParentNode(e);
				e.oldParentNode = r,
				e.isRemovedNode = !0
			}),
			function(e, t, r) {
				for (var n = new p["default"], i = new p["default"], o = []; 0 < r.length;) {
					var a = r.shift(),
					s = new g["default"](t, a, n, i);
					if (s.getPinpoint() || 0 !== t) {
						var u = new v["default"](t, s);
						o.push(u)
					} else {
						var c = a.parentNode,
						h = new v["default"](1, new g["default"](1, c, n, i));
						r = f(c, r),
						r = [c].concat(r),
						o.push(h)
					}
				}
				return 1 === t && o.reverse(),
				o
			} (0, e, t))
		}
		function f(e, t) {
			for (var r = h(e), n = t.length, i = 0; i < n; i++) {
				var o = t[i].funContent;
				if (!r || -1 == r.indexOf(o)) break;
				t[i].isSmallContent = !0
			}
			return t.filter(function(e) {
				return 1 != e[_0x2654("0x28")]
			})
		}
		function h(e) {
			var t = e.outerHTML || e.textContent;
			return 8 === e.nodeType && (t = "\x3c!--" + t + "--\x3e"),
			t.replace(/&amp;/g, "&")
		}
		function d(t) {
			var o = new p["default"],
			a = new p["default"];
			if (s["default"].isEmptyObject(t)) return [];
			var r = [],
			e = function(i) {
				var e = t[i].map(function(e) {
					var t = new g["default"](2, e, o, a),
					r = function(e, t) {
						for (var r = t.attributes,
						n = "",
						i = r.length,
						o = 0; o < i; o++) {
							var a = r[o];
							if (a.name === e) {
								n = a.value;
								break
							}
						}
						return new u["default"](e, n)
					} (i, e),
					n = new c["default"](t, [r]);
					return new v["default"](2, n)
				});
				r = r.concat(e)
			};
			for (var n in t) e(n);
			return r
		}
		var s = n(r(4)),
		p = n(r(5)),
		v = n(r(6)),
		g = n(r(7)),
		u = n(r(10)),
		c = n(r(11));
		e.exports.parseMutations = function(e) {
			return e.map(function(e) {
				var t, r = e.removed; (t = r).map(function(e) {
					e.funRemoved = !0
				});
				var n = l(e, 1, r = t),
				i = l(e, 0, e[_0x2654("0x10")]);
				i = function(e) {
					var t = e.length;
					if (t <= 1) return e;
					e.map(function(e) {
						e.funContent = e[_0x2654("0x29")].selfSelector ? e.actionData.selfSelector.parentSelector + " > " + e.actionData.selfSelector.localSelector: "No-SelfSelector"
					});
					for (var r = 0; r < t; r++) if (!e[r].isSmallContent) {
						var n = 0;
						e: for (var i = r + 1; i < t; i++) if (!e[i].isSmallContent) {
							var o = e[r].funContent,
							a = e[i].funContent;
							if ("No-SelfSelector" === o || "No-SelfSelector" === a) break e;
							if (o && 0 === o.indexOf(a)) {
								e[r].isSmallContent = !0;
								break e
							}
							if (a && 0 === a.indexOf(o) && (e[i].isSmallContent = !0), 300 < ++n) break e
						}
					}
					for (var s = [], u = 0; u < t; u++) e[u].isSmallContent || s.push(e[u]),
					delete e[u].isSmallContent,
					delete e[u].funContent;
					return s
				} (i);
				var o, a, s, u = d(e.attributeChanged),
				c = e.characterDataChanged,
				h = (o = c, a = new p["default"], s = new p["default"], o.map(function(e) {
					var t = new g["default"](3, e, a, s);
					return new v["default"](3, t)
				}));
				return [].concat(n).concat(i).concat(u).concat(h)
			}).reduce(function(e, t) {
				return e.concat(t)
			},
			[])
		},
		e.exports.extractCleanNodes = i
	},
	function(e, t) {
		"use strict";
		Array.prototype.includes || Object.defineProperty(Array.prototype, "includes", {
			value: function(e, t) {
				if (null == this) throw new TypeError('"this" is null or not defined');
				var r, n, i = Object(this),
				o = i.length >>> 0;
				if (0 === o) return ! 1;
				for (var a = 0 | t,
				s = Math.max(0 <= a ? a: o - Math.abs(a), 0); s < o;) {
					if ((r = i[s]) === (n = e) || "number" == typeof r && "number" == typeof n && isNaN(r) && isNaN(n)) return ! 0;
					s++
				}
				return ! 1
			}
		});
		e.exports.isEmptyObject = function(e) {
			var t;
			for (t in e) if (e.hasOwnProperty(t)) return ! 1;
			return ! 0
		},
		e.exports.fromList = function(e) {
			for (var t = new Array(e.length), r = 0, n = e.length; r < n; r++) t[r] = e[r];
			return t
		},
		e.exports.uniqueID = function() {
			function e() {
				return Math.random().toString(16).slice( - 4)
			}
			return e() + e() + "-" + e() + "-" + e() + "-" + e() + "-" + e() + e() + e()
		},
		e.exports.extractNodeName = function(e) {
			return e ? e.nodeName[_0x2654("0x2a")]() : null
		}
	},
	function(e, t) {
		"use strict";
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var r = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n[_0x2654("0x2c")], n)
				}
			}
			return function(e, t, r) {
				return t && n(e.prototype, t),
				r && n(e, r),
				e
			}
		} (),
		n = function() {
			function e() { (function(e, t) {
					if (! (e instanceof t)) throw new TypeError(_0x2654("0x2b"))
				})(this, e),
				this.cache = {}
			}
			return r(e, [{
				key: "clearCache",
				value: function() {
					this.cache = {}
				}
			},
			{
				key: "hasId",
				value: function(e) {
					return null != this.cache[e]
				}
			},
			{
				key: "getValue",
				value: function(e) {
					return this.cache[e]
				}
			},
			{
				key: "push",
				value: function(e, t) {
					this[_0x2654("0x2d")][e] = t
				}
			},
			{
				key: "getCache",
				value: function() {
					return this.cache
				}
			}]),
			e
		} ();
		t["default"] = n
	},
	function(e, t) {
		"use strict";
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var r = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n[_0x2654("0x2e")] = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e.prototype, t),
				r && n(e, r),
				e
			}
		} (),
		n = function() {
			function n(e, t) { (function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				})(this, n),
				this.actionType = e,
				this.actionData = t;
				var r = new Date;
				this.timestamp = r.getTime()
			}
			return r(n, [{
				key: "show",
				value: function() {}
			}]),
			n
		} ();
		t["default"] = n
	},
	function(e, t, r) {
		"use strict";
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var n, i = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e.prototype, t),
				r && n(e, r),
				e
			}
		} (),
		o = r(8),
		h = (n = o) && n[_0x2654("0x2f")] ? n: {
			"default": n
		},
		a = function() {
			function c(e, t, r, n) { (function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				})(this, c),
				this.nodeType = t.nodeType,
				this.outerHTML = t.outerHTML || t.textContent;
				var i = 1 != e ? t.previousSibling: t.oldPreviousSibling,
				o = t.parentNode;
				o || (o = t.oldParentNode || i && i.parentNode);
				var a = {
					selfNode: t,
					parentNode: o,
					previousElementSibling: t.previousElementSibling,
					previousSibling: i,
					nextElementSibling: t.nextElementSibling,
					nextSibling: t.nextSibling
				};
				try {
					this.selfSelector = h["default"].calculateSelfSelector(a, r, n)
				} catch(s) {
					this.selfSelector = null
				}
				try {
					this.pinpoint = h["default"].calculatePinpoint(a, r, n)
				} catch(u) {
					this.pinpoint = null
				}
			}
			return i(c, [{
				key: "getPinpoint",
				value: function() {
					return this.pinpoint
				}
			},
			{
				key: "show",
				value: function() {
					this.pinpoint.show()
				}
			}]),
			c
		} ();
		t["default"] = a
	},
	function(e, t, r) {
		"use strict";
		function n(e) {
			return e && e.__esModule ? e: {
				"default": e
			}
		}
		function i(e, t, r) {
			var n = e.previousElementSibling || e.previousSibling;
			if (n) {
				var i = v["default"].extractNodeName(n);
				if ("#comment" === i) return s(e, n, t, r);
				if ("#text" === i) {
					n.textContent.trim();
					return s(e, n, t, r)
				}
				var o = c(e.parentNode, t, r) + " > " + h(n, r);
				return new p["default"](o, 3)
			}
			var a = c(e.parentNode, t, r);
			return new p["default"](a, 1)
		}
		function s(e, t, r, n) {
			return e.previousElementSibling = t[_0x2654("0x30")],
			e[_0x2654("0x31")] = f(t[_0x2654("0x31")]),
			i(e, r, n)
		}
		function u(e) {
			return e && (t = e) ? "" + t.nodeName.toLowerCase() : "";
			var t
		}
		function c(e, t, r) {
			if (!e) return "";
			var n = e.funId;
			if (n) {
				if (t.hasId(n)) return t.getValue(n)
			} else e.funId = v["default"].uniqueID();
			var i = h(e, r),
			o = e.nodeName;
			if ("BODY" === o || "HEAD" === o) return i;
			if (i) {
				var a = e.parentNode;
				a || (a = e.oldParentNode);
				var s = c(a, t, r),
				u = s ? s + " > " + i: i;
				return t.push(e.funId, u),
				u
			}
			throw new Error("current node has no selector..")
		}
		function h(e, t) {
			e.funLocalSelector || (e.funLocalSelector = u(e));
			var r = l(f(e), e.funLocalSelector, 0, t);
			if (e.funId || (e.funId = v["default"].uniqueID()), t.push(e.funId, {
				funLocalSelector: e.funLocalSelector,
				index: r
			}), g <= r) throw new Error("too deep for calculateElementLocalSelector");
			return e.funLocalSelector + ":eq(" + r + ")"
		}
		function l(e, t, r, n) {
			var i = 0;
			if (!e) return i;
			if (e.funId && n[_0x2654("0x33")](e.funId)) {
				var o = n.getValue(e.funId);
				if (d(o.funLocalSelector, t)) return o.index + 1
			}
			if (g <= r) return i;
			e.funId || (e.funId = v["default"].uniqueID()),
			e.funLocalSelector || (e.funLocalSelector = u(e));
			var a = f(e);
			return a && (i += l(a, t, ++r, n)),
			d(e.funLocalSelector, t) && (n.push(e.funId, {
				funLocalSelector: e.funLocalSelector,
				index: i
			}), i++),
			i
		}
		function f(e) {
			if (!e) return e;
			var t = e.previousElementSibling || e.previousSibling;
			return e.isRemovedNode && (t = e.oldPreviousSibling),
			t
		}
		function d(e, t) {
			var r = /[#\.]/,
			n = e.split(r),
			i = t.split(r);
			return 0 !== i.length && 0 !== n.length && i.map(function(e) {
				return n.includes(e)
			}).reduce(function(e, t) {
				return e && t
			},
			!0)
		}
		var p = n(r(9)),
		v = n(r(4)),
		g = 1e4;
		e.exports.calculatePinpoint = function(e, t, r) {
			return i(e, t, r)
		},
		e.exports.calculateSelfSelector = function(e, t, r) {
			return {
				parentSelector: c(e.parentNode, t, r),
				localSelector: function(e, t, r) {
					e.funLocalSelector || (e.funLocalSelector = u(e));
					var n = l(t, e.funLocalSelector, 0, r);
					if (e.funId || (e.funId = v[_0x2654("0x32")].uniqueID()), r.push(e.funId, {
						funLocalSelector: e.funLocalSelector,
						index: n
					}), g <= n) throw new Error("too deep for calculateElementLocalSelector");
					return e.funLocalSelector + ":eq(" + n + ")"
				} (e.selfNode, e.previousSibling, r)
			}
		}
	},
	function(e, t) {
		"use strict";
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var n = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e.prototype, t),
				r && n(e, r),
				e
			}
		} (),
		r = function() {
			function r(e, t) { (function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				})(this, r),
				this.selector = e,
				this.type = t
			}
			return n(r, [{
				key: "show",
				value: function() {}
			},
			{
				key: "getSelector",
				value: function() {
					return this.selector
				}
			},
			{
				key: "getType",
				value: function() {
					return this.type
				}
			}]),
			r
		} ();
		t["default"] = r
	},
	function(e, t) {
		"use strict";
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var n = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e[_0x2654("0x8")], t),
				r && n(e, r),
				e
			}
		} (),
		r = function() {
			function r(e, t) { (function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				})(this, r),
				this.name = e,
				this.value = t
			}
			return n(r, [{
				key: "getName",
				value: function() {
					return this.name
				}
			},
			{
				key: "getValue",
				value: function() {
					return this.value
				}
			},
			{
				key: "show",
				value: function() {}
			}]),
			r
		} ();
		t["default"] = r
	},
	function(e, t) {
		"use strict";
		Object.defineProperty(t, _0x2654("0x2f"), {
			value: !0
		});
		var n = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e[_0x2654("0x8")], t),
				r && n(e, r),
				e
			}
		} (),
		r = function() {
			function r(e, t) { (function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				})(this, r),
				this.redoableElement = e,
				this.changedAttributeList = t
			}
			return n(r, [{
				key: "setElement",
				value: function() {
					this.redoableElement = redoableElement
				}
			},
			{
				key: "setAttributeList",
				value: function() {
					this.changedAttributeList = changedAttributeList
				}
			},
			{
				key: "appendAttribute",
				value: function(e) {
					this.changedAttributeList = this.changedAttributeList.concat([e])
				}
			},
			{
				key: "show",
				value: function() {
					this.redoableElement.show(),
					this.changedAttributeList.map(function(e) {
						e.show()
					})
				}
			}]),
			r
		} ();
		t["default"] = r
	},
	function(e, t) {
		"use strict";
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var r = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e[_0x2654("0x8")], t),
				r && n(e, r),
				e
			}
		} (),
		n = function() {
			function n(e, t, r) { (function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				})(this, n),
				this.inputType = e,
				this.eleType = t,
				this.value = r
			}
			return r(n, [{
				key: "getEleType",
				value: function() {
					return this.eleType
				}
			},
			{
				key: "getInputType",
				value: function() {
					return this.inputType
				}
			},
			{
				key: "getValue",
				value: function() {
					return this.value
				}
			}]),
			n
		} ();
		t["default"] = n
	},
	function(e, t, r) {
		"use strict";
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var n, i = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e.prototype, t),
				r && n(e, r),
				e
			}
		} (),
		o = r(8),
		s = (n = o) && n.__esModule ? n: {
			"default": n
		},
		a = function() {
			function a(e, t, r, n) { !
				function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				} (this, a);
				var i = {
					selfNode: e,
					parentNode: e.parentNode,
					previousElementSibling: e.previousElementSibling,
					previousSibling: e[_0x2654("0x31")],
					nextElementSibling: e.nextElementSibling,
					nextSibling: e.nextSibling
				};
				try {
					this.selfSelector = s[_0x2654("0x32")].calculateSelfSelector(i, r, n)
				} catch(o) {
					this.selfSelector = null
				}
				this.input = t
			}
			return i(a, [{
				key: "getSelfSelector",
				value: function() {
					return this.selfSelector
				}
			},
			{
				key: "getInput",
				value: function() {
					return this.input
				}
			},
			{
				key: "show",
				value: function() {
					this.pinpoint.show()
				}
			}]),
			a
		} ();
		t["default"] = a
	},
	function(e, t, r) {
		"use strict";
		var n, i = r(15),
		o = (n = i) && n.__esModule ? n: {
			"default": n
		};
		e.exports.Recorder = o["default"]
	},
	function(e, t, r) {
		"use strict";
		function n(e) {
			return e && e.__esModule ? e: {
				"default": e
			}
		}
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var i = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e.prototype, t),
				r && n(e, r),
				e
			}
		} (),
		o = n(r(16)),
		s = n(r(18)),
		u = r(20),
		c = n(r(8)),
		h = n(r(5)),
		a = function(e) {
			function r() {
				var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {}; !
				function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				} (this, r);
				var t = function(e, t) {
					if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
					return ! t || "object" != typeof t && "function" != typeof t ? e: t
				} (this, (r.__proto__ || Object.getPrototypeOf(r)).call(this, e));
				return t.events = ["mousemove", "click", "scroll", _0x2654("0x35")],
				t.eventHandlers = [],
				t.vedio = null,
				t.isRecording = !1,
				t.mousemoveSamplingFrequence = 0,
				t.MaxMousemoveSamplingFrequence = 4,
				t.target instanceof window.HTMLHtmlElement && (t.listeners = {
					scroll: window,
					resize: window
				}),
				t.on("fun_recording_start",
				function() {
					t.isRecording = !0
				}),
				t.on("fun_recording_stop",
				function() {
					t.isRecording = !1
				}),
				t.on("fun_recording",
				function(e) {}),
				t
			}
			return function(e, t) {
				if ("function" != typeof t && null !== t) throw new TypeError(_0x2654("0x34") + typeof t);
				e.prototype = Object.create(t && t.prototype, {
					constructor: {
						value: e,
						enumerable: !1,
						writable: !0,
						configurable: !0
					}
				}),
				t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
			} (r, o["default"]),
			i(r, [{
				key: "getVedio",
				value: function() {
					return this.vedio
				}
			},
			{
				key: "getListener",
				value: function(e) {
					return this.listeners[e] || this.getTarget()
				}
			},
			{
				key: "start",
				value: function() {
					var i = this;
					this.init(),
					this.emit("fun_recording_start");
					for (var e = function(e) {
						var t = i.events[e],
						r = i.onEvents[t];
						r || (r = i.defaultEventCallback);
						var n = function(e) {
							i.onEventCallback(r, e)
						};
						i.eventHandlers.splice(e, 0, n),
						"scroll" === t ? i.getListener(t).addEventListener(t, n, !0) : i.getListener(t).addEventListener(t, n)
					},
					t = 0; t < this.events.length; t++) e(t)
				}
			},
			{
				key: "stopRecording",
				value: function() {
					for (var e = 0; e < this.events.length; e++) {
						var t = this.events[e],
						r = this.eventHandlers[e];
						r && this.getListener(t).removeEventListener(t, r)
					}
					return this.emit("fun_recording_stop"),
					this.vedio
				}
			},
			{
				key: "init",
				value: function() {
					var e = this.getTarget(),
					t = this.getTargetPosition(),
					r = new s["default"];
					r.top = t[_0x2654("0x36")],
					r.left = t.left;
					var n = e.cloneNode(!0).innerHTML,
					i = this.removeHideElement(n);
					i = "<!DOCTYPE html>" + i,
					r.scene = u.Base64.encode(i),
					r.rootOpts = {
						style: e.getAttribute("style"),
						"class": e.getAttribute("class")
					},
					r.scrollX = e.scrollLeft,
					r.scrollY = e.scrollTop;
					var o = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
					a = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
					return r.width = o,
					r.height = a,
					this.vedio = r,
					this.vedio
				}
			},
			{
				key: "getTargetPosition",
				value: function() {
					var e = this.getTarget().getBoundingClientRect(),
					t = document.documentElement,
					r = window.pageYOffset || t.scrollTop || 0,
					n = window.pageXOffset || t.scrollLeft || 0;
					return {
						top: e.top + r,
						left: e.left + n
					}
				}
			},
			{
				key: "onEventCallback",
				value: function(e, t) {
					this.emit("fun_recording", t),
					this.emit(t.type, t),
					e(this, this.vedio, t)
				}
			},
			{
				key: _0x2654("0x37"),
				value: function(e, t, r) {
					if ("mousemove" === r.type) {
						if (! (e[_0x2654("0x38")] >= e.MaxMousemoveSamplingFrequence)) return void e.mousemoveSamplingFrequence++;
						e.mousemoveSamplingFrequence = 0
					}
					var n = r.target,
					i = n.body,
					o = {
						scrollY: i ? i.parentNode.scrollTop || i.scrollTop: n.scrollTop,
						scrollX: i ? i.parentNode.scrollLeft || i.scrollLeft: n.scrollLeft,
						cursorX: r.pageX,
						cursorY: r.pageY,
						width: e.target.clientWidth,
						height: e.target.clientHeight,
						eventType: r.type
					};
					if ("scroll" === r.type) {
						var a = {
							selfNode: n,
							parentNode: n.parentNode,
							previousElementSibling: n.previousElementSibling,
							previousSibling: n.previousSibling,
							nextElementSibling: n.nextElementSibling,
							nextSibling: n.nextSibling
						};
						try {
							o.selfSelector = c["default"].calculateSelfSelector(a, new h["default"], new h["default"])
						} catch(s) {
							o.selfSelector = null
						}
					}
					t.pushNewFrame(o)
				}
			},
			{
				key: "removeHideElement",
				value: function(e) {
					var t = (new DOMParser)[_0x2654("0x39")](e, "text/html");
					try {
						for (var r = t.getElementsByClassName("_fun-hide"), n = [].slice.call(r), i = 0; i < n.length; i++) n[i].remove()
					} catch(o) {}
					return t.documentElement.innerHTML
				}
			}]),
			r
		} ();
		t["default"] = a
	},
	function(e, t, r) {
		"use strict";
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var n = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e.prototype, t),
				r && n(e, r),
				e
			}
		} (),
		i = r(17),
		o = function(e) {
			function r() {
				var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {}; !
				function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				} (this, r);
				var t = function(e, t) {
					if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
					return ! t || "object" != typeof t && "function" != typeof t ? e: t
				} (this, (r.__proto__ || Object.getPrototypeOf(r)).call(this));
				return e.target ? t.setTarget(e.target) : t.target = null,
				t[_0x2654("0x3b")] = e.onEvents || t.onEvents(),
				t
			}
			return function(e, t) {
				if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
				e.prototype = Object.create(t && t.prototype, {
					constructor: {
						value: e,
						enumerable: !1,
						writable: !0,
						configurable: !0
					}
				}),
				t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e[_0x2654("0x3a")] = t)
			} (r, i["EventEmitter"]),
			n(r, [{
				key: "onEvents",
				value: function() {
					return {}
				}
			},
			{
				key: "setTarget",
				value: function(e) {
					if (! (e instanceof window.HTMLElement)) throw new Error("Target不是HTMLElement类型");
					this.target = e
				}
			},
			{
				key: "getTarget",
				value: function() {
					if (!this.target) throw new Error("未定义要监听的Target");
					return this.target
				}
			}]),
			r
		} ();
		t["default"] = o
	},
	function(e, t) {
		function n() {
			this._events = this._events || {},
			this._maxListeners = this._maxListeners || void 0
		}
		function u(e) {
			return "function" == typeof e
		}
		function c(e) {
			return "object" == typeof e && null !== e
		}
		function h(e) {
			return void 0 === e
		} ((e.exports = n).EventEmitter = n).prototype._events = void 0,
		n.prototype._maxListeners = void 0,
		n.defaultMaxListeners = 10,
		n.prototype.setMaxListeners = function(e) {
			if ("number" != typeof e || e < 0 || isNaN(e)) throw TypeError("n must be a positive number");
			return this._maxListeners = e,
			this
		},
		n.prototype.emit = function(e) {
			var t, r, n, i, o, a;
			if (this._events || (this[_0x2654("0x3c")] = {}), "error" === e && (!this._events.error || c(this._events.error) && !this._events.error.length)) {
				if ((t = arguments[1]) instanceof Error) throw t;
				var s = new Error('Uncaught, unspecified "error" event. (' + t + ")");
				throw s.context = t,
				s
			}
			if (h(r = this._events[e])) return ! 1;
			if (u(r)) switch (arguments.length) {
			case 1:
				r.call(this);
				break;
			case 2:
				r.call(this, arguments[1]);
				break;
			case 3:
				r.call(this, arguments[1], arguments[2]);
				break;
			default:
				i = Array.prototype.slice.call(arguments, 1),
				r.apply(this, i)
			} else if (c(r)) for (i = Array.prototype.slice.call(arguments, 1), n = (a = r.slice()).length, o = 0; o < n; o++) a[o].apply(this, i);
			return ! 0
		},
		n.prototype.addListener = function(e, t) {
			var r;
			if (!u(t)) throw TypeError("listener must be a function");
			return this._events || (this._events = {}),
			this._events.newListener && this.emit("newListener", e, u(t.listener) ? t.listener: t),
			this._events[e] ? c(this._events[e]) ? this._events[e].push(t) : this._events[e] = [this._events[e], t] : this._events[e] = t,
			c(this[_0x2654("0x3c")][e]) && !this._events[e].warned && ((r = h(this._maxListeners) ? n.defaultMaxListeners: this._maxListeners) && 0 < r && this._events[e].length > r && (this._events[e].warned = !0, console.error("(node) warning: possible EventEmitter memory leak detected. %d listeners added. Use emitter.setMaxListeners() to increase limit.", this._events[e][_0x2654("0x11")]), "function" == typeof console.trace && console[_0x2654("0x3d")]())),
			this
		},
		n[_0x2654("0x8")].on = n[_0x2654("0x8")].addListener,
		n.prototype.once = function(e, t) {
			function r() {
				this.removeListener(e, r),
				n || (n = !0, t.apply(this, arguments))
			}
			if (!u(t)) throw TypeError("listener must be a function");
			var n = !1;
			return r.listener = t,
			this.on(e, r),
			this
		},
		n.prototype.removeListener = function(e, t) {
			var r, n, i, o;
			if (!u(t)) throw TypeError("listener must be a function");
			if (!this._events || !this._events[e]) return this;
			if (i = (r = this._events[e]).length, n = -1, r === t || u(r.listener) && r[_0x2654("0x3e")] === t) delete this._events[e],
			this._events.removeListener && this.emit("removeListener", e, t);
			else if (c(r)) {
				for (o = i; 0 < o--;) if (r[o] === t || r[o].listener && r[o][_0x2654("0x3e")] === t) {
					n = o;
					break
				}
				if (n < 0) return this;
				1 === r.length ? (r.length = 0, delete this._events[e]) : r.splice(n, 1),
				this._events.removeListener && this.emit("removeListener", e, t)
			}
			return this
		},
		n.prototype.removeAllListeners = function(e) {
			var t, r;
			if (!this._events) return this;
			if (!this._events.removeListener) return 0 === arguments.length ? this._events = {}: this._events[e] && delete this._events[e],
			this;
			if (0 === arguments.length) {
				for (t in this._events)"removeListener" !== t && this.removeAllListeners(t);
				return this.removeAllListeners("removeListener"),
				this._events = {},
				this
			}
			if (u(r = this._events[e])) this.removeListener(e, r);
			else if (r) for (; r.length;) this.removeListener(e, r[r.length - 1]);
			return delete this._events[e],
			this
		},
		n[_0x2654("0x8")].listeners = function(e) {
			return this._events && this._events[e] ? u(this._events[e]) ? [this._events[e]] : this._events[e].slice() : []
		},
		n.prototype.listenerCount = function(e) {
			if (this._events) {
				var t = this._events[e];
				if (u(t)) return 1;
				if (t) return t[_0x2654("0x11")]
			}
			return 0
		},
		n.listenerCount = function(e, t) {
			return e.listenerCount(t)
		}
	},
	function(e, t, r) {
		"use strict";
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var n, i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ?
		function(e) {
			return typeof e
		}: function(e) {
			return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol": typeof e
		},
		o = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e.prototype, t),
				r && n(e, r),
				e
			}
		} (),
		a = r(19),
		s = (n = a) && n.__esModule ? n: {
			"default": n
		},
		u = r(20),
		c = function() {
			function n() {
				var e = 0 < arguments[_0x2654("0x11")] && void 0 !== arguments[0] ? arguments[0] : {}; (function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				})(this, n),
				this.top = e.top || 0,
				this.left = e.left || 0,
				this.scrollX = e.scrollX || 0,
				this.scrollY = e.scrollY || 0,
				this.width = e.width || 0,
				this.height = e.height || 0,
				this.scene = e.scene || "",
				this.rootOpts = e.rootOpts || null;
				var t = new Date;
				this[_0x2654("0x3f")] = t.getTime();
				var r = e.frames || [];
				this.setFrames(r)
			}
			return o(n, [{
				key: "getFrames",
				value: function() {
					return this.frames
				}
			},
			{
				key: "getFramesLength",
				value: function() {
					return this.frames && this[_0x2654("0x40")][_0x2654("0x11")]
				}
			},
			{
				key: "setFrames",
				value: function(e) {
					this.frames = [];
					for (var t = 0; t < e.length; t++) this.pushNewFrame(e[t]);
					return this
				}
			},
			{
				key: "pushNewFrame",
				value: function() {
					var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {};
					if (null !== e && "object" === (void 0 === e ? "undefined": i(e)) && !(e = new s["default"](e)).timestamp) {
						var t = new Date;
						e.timestamp = t.getTime()
					}
					e && this.frames.push(e)
				}
			},
			{
				key: "toJSON",
				value: function() {
					return {
						top: this.top,
						left: this[_0x2654("0x41")],
						scrollX: this.scrollX,
						scrollY: this.scrollY,
						width: this.width,
						height: this.height,
						timestamp: this.timestamp,
						scene: this.scene,
						rootOpts: this.rootOpts,
						frames: this.frames
					}
				}
			},
			{
				key: "getDecodedScene",
				value: function() {
					return this.sceneDecoded || (this.sceneDecoded = u.Base64.decode(this.scene)),
					this.sceneDecoded
				}
			}]),
			n
		} ();
		t[_0x2654("0x32")] = c
	},
	function(e, t) {
		"use strict";
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var r = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n.writable = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e.prototype, t),
				r && n(e, r),
				e
			}
		} (),
		n = function() {
			function t() {
				var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {}; (function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				})(this, t),
				this.cursorX = e[_0x2654("0x42")] || 0,
				this.cursorY = e.cursorY || 0,
				this.scrollX = e.scrollX || 0,
				this.scrollY = e.scrollY || 0,
				this.height = e.height || 0,
				this.width = e.width || 0,
				this.timestamp = e.timestamp || 0,
				this.options = e.options || {},
				this.eventType = e.eventType || "",
				this.selfSelector = e.selfSelector || null
			}
			return r(t, [{
				key: "toJSON",
				value: function() {
					return {
						eventType: this.eventType,
						options: this.options,
						scrollX: this.scrollX,
						scrollY: this.scrollY,
						cursorX: this.cursorX,
						cursorY: this.cursorY,
						height: this[_0x2654("0x43")],
						width: this.width,
						timestamp: this.timestamp,
						selfSelector: this.selfSelector
					}
				}
			}]),
			t
		} ();
		t["default"] = n
	},
	function(x, E, S) {
		var C; (function(e) {
			var t;
			t = "undefined" != typeof self ? self: "undefined" != typeof window ? window: void 0 !== e ? e: this,
			x.exports = function(t) {
				"use strict";
				var r, n = t.Base64;
				if (void 0 !== x && x.exports) try {
					r = S(21).Buffer
				} catch(_) {}
				var i = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
				o = function(e) {
					for (var t = {},
					r = 0,
					n = e.length; r < n; r++) t[e.charAt(r)] = r;
					return t
				} (i),
				a = String.fromCharCode,
				s = function(e) {
					if (e.length < 2) {
						var t = e.charCodeAt(0);
						return t < 128 ? e: t < 2048 ? a(192 | t >>> 6) + a(128 | 63 & t) : a(224 | t >>> 12 & 15) + a(128 | t >>> 6 & 63) + a(128 | 63 & t)
					}
					var t = 65536 + 1024 * (e.charCodeAt(0) - 55296) + (e.charCodeAt(1) - 56320);
					return a(240 | t >>> 18 & 7) + a(128 | t >>> 12 & 63) + a(128 | t >>> 6 & 63) + a(128 | 63 & t)
				},
				u = /[\uD800-\uDBFF][\uDC00-\uDFFFF]|[^\x00-\x7F]/g,
				c = function(e) {
					return e[_0x2654("0x44")](u, s)
				},
				h = function(e) {
					var t = [0, 2, 1][e.length % 3],
					r = e.charCodeAt(0) << 16 | (1 < e.length ? e.charCodeAt(1) : 0) << 8 | (2 < e.length ? e.charCodeAt(2) : 0),
					n = [i.charAt(r >>> 18), i.charAt(r >>> 12 & 63), 2 <= t ? "=": i.charAt(r >>> 6 & 63), 1 <= t ? "=": i.charAt(63 & r)];
					return n.join("")
				},
				l = t.btoa ?
				function(e) {
					return t.btoa(e)
				}: function(e) {
					return e.replace(/[\s\S]{1,3}/g, h)
				},
				f = r ? r.from && r.from !== Uint8Array.from ?
				function(e) {
					return (e.constructor === r.constructor ? e: r.from(e)).toString("base64")
				}: function(e) {
					return (e.constructor === r.constructor ? e: new r(e)).toString("base64")
				}: function(e) {
					return l(c(e))
				},
				d = function(e, t) {
					return t ? f(String(e)).replace(/[+\/]/g,
					function(e) {
						return "+" == e ? "-": "_"
					}).replace(/=/g, "") : f(String(e))
				},
				p = new RegExp(["[À-ß][-¿]", "[à-ï][-¿]{2}", "[ð-÷][-¿]{3}"].join("|"), "g"),
				v = function(e) {
					switch (e.length) {
					case 4:
						var t = (7 & e.charCodeAt(0)) << 18 | (63 & e.charCodeAt(1)) << 12 | (63 & e.charCodeAt(2)) << 6 | 63 & e.charCodeAt(3),
						r = t - 65536;
						return a(55296 + (r >>> 10)) + a(56320 + (1023 & r));
					case 3:
						return a((15 & e.charCodeAt(0)) << 12 | (63 & e.charCodeAt(1)) << 6 | 63 & e.charCodeAt(2));
					default:
						return a((31 & e.charCodeAt(0)) << 6 | 63 & e.charCodeAt(1))
					}
				},
				g = function(e) {
					return e.replace(p, v)
				},
				y = function(e) {
					var t = e.length,
					r = t % 4,
					n = (0 < t ? o[e.charAt(0)] << 18 : 0) | (1 < t ? o[e.charAt(1)] << 12 : 0) | (2 < t ? o[e.charAt(2)] << 6 : 0) | (3 < t ? o[e.charAt(3)] : 0),
					i = [a(n >>> 16), a(n >>> 8 & 255), a(255 & n)];
					return i.length -= [0, 0, 2, 1][r],
					i.join("")
				},
				b = t.atob ?
				function(e) {
					return t.atob(e)
				}: function(e) {
					return e.replace(/[\s\S]{1,4}/g, y)
				},
				m = r ? r.from && r.from !== Uint8Array.from ?
				function(e) {
					return (e[_0x2654("0x45")] === r.constructor ? e: r.from(e, "base64")).toString()
				}: function(e) {
					return (e.constructor === r.constructor ? e: new r(e, "base64")).toString()
				}: function(e) {
					return g(b(e))
				},
				e = function(e) {
					return m(String(e).replace(/[-_]/g,
					function(e) {
						return "-" == e ? "+": "/"
					}).replace(/[^A-Za-z0-9\+\/]/g, ""))
				};
				if (t.Base64 = {
					VERSION: "2.4.0",
					atob: b,
					btoa: l,
					fromBase64: e,
					toBase64: d,
					utob: c,
					encode: d,
					encodeURI: function(e) {
						return d(e, !0)
					},
					btou: g,
					decode: e,
					noConflict: function() {
						var e = t.Base64;
						return t.Base64 = n,
						e
					}
				},
				"function" == typeof Object.defineProperty) {
					var w = function(e) {
						return {
							value: e,
							enumerable: !1,
							writable: !0,
							configurable: !0
						}
					};
					t.Base64.extendString = function() {
						Object.defineProperty(String.prototype, "fromBase64", w(function() {
							return e(this)
						})),
						Object.defineProperty(String.prototype, "toBase64", w(function(e) {
							return d(this, e)
						})),
						Object.defineProperty(String.prototype, "toBase64URI", w(function() {
							return d(this, !0)
						}))
					}
				}
				return t.Meteor && (Base64 = t.Base64),
				void 0 !== x && x.exports ? x.exports.Base64 = t.Base64: void 0 !== (C = function() {
					return t.Base64
				}.apply(E, [])) && (x.exports = C),
				{
					Base64: t.Base64
				}
			} (t)
		}).call(E,
		function() {
			return this
		} ())
	},
	function(e, M, t) { (function(e) {
			"use strict";
			function r() {
				return l.TYPED_ARRAY_SUPPORT ? 2147483647 : 1073741823
			}
			function s(e, t) {
				if (r() < t) throw new RangeError(_0x2654("0x46"));
				return l.TYPED_ARRAY_SUPPORT ? (e = new Uint8Array(t)).__proto__ = l.prototype: (null === e && (e = new l(t)), e.length = t),
				e
			}
			function l(e, t, r) {
				if (! (l.TYPED_ARRAY_SUPPORT || this instanceof l)) return new l(e, t, r);
				if ("number" == typeof e) {
					if ("string" == typeof t) throw new Error("If encoding is specified then the first argument must be a string");
					return i(this, e)
				}
				return n(this, e, t, r)
			}
			function n(e, t, r, n) {
				if ("number" == typeof t) throw new TypeError(_0x2654("0x47"));
				return "undefined" != typeof ArrayBuffer && t instanceof ArrayBuffer ?
				function(e, t, r, n) {
					if (t.byteLength, r < 0 || t.byteLength < r) throw new RangeError("'offset' is out of bounds");
					if (t.byteLength < r + (n || 0)) throw new RangeError(_0x2654("0x48"));
					return t = void 0 === r && void 0 === n ? new Uint8Array(t) : void 0 === n ? new Uint8Array(t, r) : new Uint8Array(t, r, n),
					l.TYPED_ARRAY_SUPPORT ? (e = t).__proto__ = l.prototype: e = o(e, t),
					e
				} (e, t, r, n) : "string" == typeof t ?
				function(e, t, r) {
					if ("string" == typeof r && "" !== r || (r = "utf8"), !l.isEncoding(r)) throw new TypeError('"encoding" must be a valid string encoding');
					var n = 0 | c(t, r),
					i = (e = s(e, n)).write(t, r);
					return i !== n && (e = e.slice(0, i)),
					e
				} (e, t, r) : function(e, t) {
					if (l.isBuffer(t)) {
						var r = 0 | a(t.length);
						return 0 === (e = s(e, r)).length || t.copy(e, 0, 0, r),
						e
					}
					if (t) {
						if ("undefined" != typeof ArrayBuffer && t.buffer instanceof ArrayBuffer || "length" in t) return "number" != typeof t.length || (n = t.length) != n ? s(e, 0) : o(e, t);
						if ("Buffer" === t.type && L(t.data)) return o(e, t.data)
					}
					var n;
					throw new TypeError("First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.")
				} (e, t)
			}
			function u(e) {
				if ("number" != typeof e) throw new TypeError('"size" argument must be a number');
				if (e < 0) throw new RangeError('"size" argument must not be negative')
			}
			function i(e, t) {
				if (u(t), e = s(e, t < 0 ? 0 : 0 | a(t)), !l.TYPED_ARRAY_SUPPORT) for (var r = 0; r < t; ++r) e[r] = 0;
				return e
			}
			function o(e, t) {
				var r = t.length < 0 ? 0 : 0 | a(t.length);
				e = s(e, r);
				for (var n = 0; n < r; n += 1) e[n] = 255 & t[n];
				return e
			}
			function a(e) {
				if (e >= r()) throw new RangeError("Attempt to allocate Buffer larger than maximum size: 0x" + r()[_0x2654("0x49")](16) + " bytes");
				return 0 | e
			}
			function c(e, t) {
				if (l.isBuffer(e)) return e.length;
				if ("undefined" != typeof ArrayBuffer && "function" == typeof ArrayBuffer.isView && (ArrayBuffer.isView(e) || e instanceof ArrayBuffer)) return e.byteLength;
				"string" != typeof e && (e = "" + e);
				var r = e[_0x2654("0x11")];
				if (0 === r) return 0;
				for (var n = !1;;) switch (t) {
				case "ascii":
				case "latin1":
				case "binary":
					return r;
				case "utf8":
				case "utf-8":
				case void 0:
					return O(e).length;
				case "ucs2":
				case "ucs-2":
				case "utf16le":
				case "utf-16le":
					return 2 * r;
				case "hex":
					return r >>> 1;
				case "base64":
					return k(e).length;
				default:
					if (n) return O(e).length;
					t = ("" + t).toLowerCase(),
					n = !0
				}
			}
			function h(e, t, r) {
				var n = e[t];
				e[t] = e[r],
				e[r] = n
			}
			function f(e, t, r, n, i) {
				if (0 === e.length) return - 1;
				if ("string" == typeof r ? (n = r, r = 0) : 2147483647 < r ? r = 2147483647 : r < -2147483648 && (r = -2147483648), r = +r, isNaN(r) && (r = i ? 0 : e.length - 1), r < 0 && (r = e.length + r), r >= e.length) {
					if (i) return - 1;
					r = e.length - 1
				} else if (r < 0) {
					if (!i) return - 1;
					r = 0
				}
				if ("string" == typeof t && (t = l.from(t, n)), l.isBuffer(t)) return 0 === t.length ? -1 : d(e, t, r, n, i);
				if ("number" == typeof t) return t &= 255,
				l.TYPED_ARRAY_SUPPORT && _0x2654("0x4a") == typeof Uint8Array.prototype.indexOf ? i ? Uint8Array.prototype.indexOf.call(e, t, r) : Uint8Array[_0x2654("0x8")].lastIndexOf.call(e, t, r) : d(e, [t], r, n, i);
				throw new TypeError("val must be string, number or Buffer")
			}
			function d(e, t, r, n, i) {
				function o(e, t) {
					return 1 === s ? e[t] : e.readUInt16BE(t * s)
				}
				var a, s = 1,
				u = e.length,
				c = t.length;
				if (void 0 !== n && ("ucs2" === (n = String(n).toLowerCase()) || "ucs-2" === n || "utf16le" === n || "utf-16le" === n)) {
					if (e.length < 2 || t.length < 2) return - 1;
					u /= s = 2,
					c /= 2,
					r /= 2
				}
				if (i) {
					var h = -1;
					for (a = r; a < u; a++) if (o(e, a) === o(t, -1 === h ? 0 : a - h)) {
						if ( - 1 === h && (h = a), a - h + 1 === c) return h * s
					} else - 1 !== h && (a -= a - h),
					h = -1
				} else for (u < r + c && (r = u - c), a = r; 0 <= a; a--) {
					for (var l = !0,
					f = 0; f < c; f++) if (o(e, a + f) !== o(t, f)) {
						l = !1;
						break
					}
					if (l) return a
				}
				return - 1
			}
			function v(e, t, r, n) {
				r = Number(r) || 0;
				var i = e.length - r;
				n ? i < (n = Number(n)) && (n = i) : n = i;
				var o = t[_0x2654("0x11")];
				if (o % 2 != 0) throw new TypeError("Invalid hex string");
				o / 2 < n && (n = o / 2);
				for (var a = 0; a < n; ++a) {
					var s = parseInt(t.substr(2 * a, 2), 16);
					if (isNaN(s)) return a;
					e[r + a] = s
				}
				return a
			}
			function g(e, t, r, n) {
				return N(function(e) {
					for (var t = [], r = 0; r < e.length; ++r) t.push(255 & e.charCodeAt(r));
					return t
				} (t), e, r, n)
			}
			function p(e, t, r) {
				r = Math.min(e.length, r);
				for (var n = [], i = t; i < r;) {
					var o, a, s, u, c = e[i],
					h = null,
					l = 239 < c ? 4 : 223 < c ? 3 : 191 < c ? 2 : 1;
					if (i + l <= r) switch (l) {
					case 1:
						c < 128 && (h = c);
						break;
					case 2:
						128 == (192 & (o = e[i + 1])) && (127 < (u = (31 & c) << 6 | 63 & o) && (h = u));
						break;
					case 3:
						o = e[i + 1],
						a = e[i + 2],
						128 == (192 & o) && 128 == (192 & a) && (2047 < (u = (15 & c) << 12 | (63 & o) << 6 | 63 & a) && (u < 55296 || 57343 < u) && (h = u));
						break;
					case 4:
						o = e[i + 1],
						a = e[i + 2],
						s = e[i + 3],
						128 == (192 & o) && 128 == (192 & a) && 128 == (192 & s) && (65535 < (u = (15 & c) << 18 | (63 & o) << 12 | (63 & a) << 6 | 63 & s) && u < 1114112 && (h = u))
					}
					null === h ? (h = 65533, l = 1) : 65535 < h && (h -= 65536, n.push(h >>> 10 & 1023 | 55296), h = 56320 | 1023 & h),
					n.push(h),
					i += l
				}
				return function(e) {
					var t = e.length;
					if (t <= R) return String.fromCharCode.apply(String, e);
					for (var r = "",
					n = 0; n < t;) r += String.fromCharCode.apply(String, e.slice(n, n += R));
					return r
				} (n)
			}
			function y(e, t, r) {
				var n = "";
				r = Math.min(e.length, r);
				for (var i = t; i < r; ++i) n += String.fromCharCode(127 & e[i]);
				return n
			}
			function b(e, t, r) {
				var n = "";
				r = Math.min(e.length, r);
				for (var i = t; i < r; ++i) n += String.fromCharCode(e[i]);
				return n
			}
			function m(e, t, r) {
				var n, i = e.length; (!t || t < 0) && (t = 0),
				(!r || r < 0 || i < r) && (r = i);
				for (var o = "",
				a = t; a < r; ++a) o += (n = e[a]) < 16 ? "0" + n.toString(16) : n.toString(16);
				return o
			}
			function w(e, t, r) {
				for (var n = e.slice(t, r), i = "", o = 0; o < n.length; o += 2) i += String.fromCharCode(n[o] + 256 * n[o + 1]);
				return i
			}
			function _(e, t, r) {
				if (e % 1 != 0 || e < 0) throw new RangeError("offset is not uint");
				if (r < e + t) throw new RangeError("Trying to access beyond buffer length")
			}
			function x(e, t, r, n, i, o) {
				if (!l.isBuffer(e)) throw new TypeError('"buffer" argument must be a Buffer instance');
				if (i < t || t < o) throw new RangeError('"value" argument is out of bounds');
				if (r + n > e.length) throw new RangeError("Index out of range")
			}
			function E(e, t, r, n) {
				t < 0 && (t = 65535 + t + 1);
				for (var i = 0,
				o = Math.min(e.length - r, 2); i < o; ++i) e[r + i] = (t & 255 << 8 * (n ? i: 1 - i)) >>> 8 * (n ? i: 1 - i)
			}
			function S(e, t, r, n) {
				t < 0 && (t = 4294967295 + t + 1);
				for (var i = 0,
				o = Math.min(e.length - r, 4); i < o; ++i) e[r + i] = t >>> 8 * (n ? i: 3 - i) & 255
			}
			function C(e, t, r, n, i, o) {
				if (r + n > e.length) throw new RangeError("Index out of range");
				if (r < 0) throw new RangeError("Index out of range")
			}
			function A(e, t, r, n, i) {
				return i || C(e, 0, r, 4),
				I.write(e, t, r, n, 23, 4),
				r + 4
			}
			function T(e, t, r, n, i) {
				return i || C(e, 0, r, 8),
				I.write(e, t, r, n, 52, 8),
				r + 8
			}
			function O(e, t) {
				t = t || 1 / 0;
				for (var r, n = e.length,
				i = null,
				o = [], a = 0; a < n; ++a) {
					if (55295 < (r = e.charCodeAt(a)) && r < 57344) {
						if (!i) {
							if (56319 < r) { - 1 < (t -= 3) && o.push(239, 191, 189);
								continue
							}
							if (a + 1 === n) { - 1 < (t -= 3) && o.push(239, 191, 189);
								continue
							}
							i = r;
							continue
						}
						if (r < 56320) { - 1 < (t -= 3) && o.push(239, 191, 189),
							i = r;
							continue
						}
						r = 65536 + (i - 55296 << 10 | r - 56320)
					} else i && -1 < (t -= 3) && o.push(239, 191, 189);
					if (i = null, r < 128) {
						if ((t -= 1) < 0) break;
						o.push(r)
					} else if (r < 2048) {
						if ((t -= 2) < 0) break;
						o.push(r >> 6 | 192, 63 & r | 128)
					} else if (r < 65536) {
						if ((t -= 3) < 0) break;
						o.push(r >> 12 | 224, r >> 6 & 63 | 128, 63 & r | 128)
					} else {
						if (! (r < 1114112)) throw new Error("Invalid code point");
						if ((t -= 4) < 0) break;
						o.push(r >> 18 | 240, r >> 12 & 63 | 128, r >> 6 & 63 | 128, 63 & r | 128)
					}
				}
				return o
			}
			function k(e) {
				return P.toByteArray(function(e) {
					if ((e = (t = e, t.trim ? t.trim() : t.replace(/^\s+|\s+$/g, "")).replace(D, "")).length < 2) return "";
					for (var t; e.length % 4 != 0;) e += "=";
					return e
				} (e))
			}
			function N(e, t, r, n) {
				for (var i = 0; i < n && !(i + r >= t.length || i >= e.length); ++i) t[i + r] = e[i];
				return i
			}
			var P = t(22),
			I = t(23),
			L = t(24);
			M.Buffer = l,
			M.SlowBuffer = function(e) {
				return + e != e && (e = 0),
				l.alloc( + e)
			},
			M.INSPECT_MAX_BYTES = 50,
			l.TYPED_ARRAY_SUPPORT = void 0 !== e.TYPED_ARRAY_SUPPORT ? e.TYPED_ARRAY_SUPPORT: function() {
				try {
					var e = new Uint8Array(1);
					return e.__proto__ = {
						__proto__: Uint8Array.prototype,
						foo: function() {
							return 42
						}
					},
					42 === e.foo() && "function" == typeof e.subarray && 0 === e.subarray(1, 1).byteLength
				} catch(t) {
					return ! 1
				}
			} (),
			M.kMaxLength = r(),
			l.poolSize = 8192,
			l._augment = function(e) {
				return e.__proto__ = l.prototype,
				e
			},
			l.from = function(e, t, r) {
				return n(null, e, t, r)
			},
			l.TYPED_ARRAY_SUPPORT && (l.prototype.__proto__ = Uint8Array.prototype, l.__proto__ = Uint8Array, "undefined" != typeof Symbol && Symbol.species && l[Symbol.species] === l && Object.defineProperty(l, Symbol.species, {
				value: null,
				configurable: !0
			})),
			l.alloc = function(e, t, r) {
				return n = null,
				o = t,
				a = r,
				u(i = e),
				i <= 0 ? s(n, i) : void 0 !== o ? "string" == typeof a ? s(n, i).fill(o, a) : s(n, i).fill(o) : s(n, i);
				var n, i, o, a
			}, l.allocUnsafe = function(e) {
				return i(null, e)
			},
			l.allocUnsafeSlow = function(e) {
				return i(null, e)
			},
			l.isBuffer = function(e) {
				return ! (null == e || !e._isBuffer)
			},
			l.compare = function(e, t) {
				if (!l.isBuffer(e) || !l.isBuffer(t)) throw new TypeError("Arguments must be Buffers");
				if (e === t) return 0;
				for (var r = e.length,
				n = t.length,
				i = 0,
				o = Math.min(r, n); i < o; ++i) if (e[i] !== t[i]) {
					r = e[i],
					n = t[i];
					break
				}
				return r < n ? -1 : n < r ? 1 : 0
			},
			l.isEncoding = function(e) {
				switch (String(e).toLowerCase()) {
				case "hex":
				case "utf8":
				case "utf-8":
				case "ascii":
				case "latin1":
				case "binary":
				case "base64":
				case "ucs2":
				case _0x2654("0x4b"):
				case "utf16le":
				case "utf-16le":
					return ! 0;
				default:
					return ! 1
				}
			},
			l.concat = function(e, t) {
				if (!L(e)) throw new TypeError('"list" argument must be an Array of Buffers');
				if (0 === e.length) return l.alloc(0);
				var r;
				if (void 0 === t) for (r = t = 0; r < e.length; ++r) t += e[r].length;
				var n = l.allocUnsafe(t),
				i = 0;
				for (r = 0; r < e.length; ++r) {
					var o = e[r];
					if (!l.isBuffer(o)) throw new TypeError('"list" argument must be an Array of Buffers');
					o.copy(n, i),
					i += o.length
				}
				return n
			},
			l.byteLength = c,
			l.prototype._isBuffer = !0,
			l.prototype.swap16 = function() {
				var e = this.length;
				if (e % 2 != 0) throw new RangeError("Buffer size must be a multiple of 16-bits");
				for (var t = 0; t < e; t += 2) h(this, t, t + 1);
				return this
			},
			l.prototype.swap32 = function() {
				var e = this.length;
				if (e % 4 != 0) throw new RangeError("Buffer size must be a multiple of 32-bits");
				for (var t = 0; t < e; t += 4) h(this, t, t + 3),
				h(this, t + 1, t + 2);
				return this
			},
			l.prototype.swap64 = function() {
				var e = this.length;
				if (e % 8 != 0) throw new RangeError("Buffer size must be a multiple of 64-bits");
				for (var t = 0; t < e; t += 8) h(this, t, t + 7),
				h(this, t + 1, t + 6),
				h(this, t + 2, t + 5),
				h(this, t + 3, t + 4);
				return this
			},
			l[_0x2654("0x8")].toString = function() {
				var e = 0 | this.length;
				return 0 === e ? "": 0 === arguments.length ? p(this, 0, e) : function(e, t, r) {
					var n, i, o, a = !1;
					if ((void 0 === t || t < 0) && (t = 0), t > this.length) return "";
					if ((void 0 === r || r > this.length) && (r = this.length), r <= 0) return "";
					if ((r >>>= 0) <= (t >>>= 0)) return "";
					for (e || (e = "utf8");;) switch (e) {
					case "hex":
						return m(this, t, r);
					case "utf8":
					case "utf-8":
						return p(this, t, r);
					case "ascii":
						return y(this, t, r);
					case "latin1":
					case "binary":
						return b(this, t, r);
					case "base64":
						return n = this,
						o = r,
						0 === (i = t) && o === n.length ? P.fromByteArray(n) : P.fromByteArray(n.slice(i, o));
					case "ucs2":
					case "ucs-2":
					case "utf16le":
					case "utf-16le":
						return w(this, t, r);
					default:
						if (a) throw new TypeError("Unknown encoding: " + e);
						e = (e + "").toLowerCase(),
						a = !0
					}
				}.apply(this, arguments)
			},
			l.prototype.equals = function(e) {
				if (!l.isBuffer(e)) throw new TypeError("Argument must be a Buffer");
				return this === e || 0 === l.compare(this, e)
			},
			l.prototype.inspect = function() {
				var e = "",
				t = M.INSPECT_MAX_BYTES;
				return 0 < this.length && (e = this.toString("hex", 0, t).match(/.{2}/g).join(" "), this.length > t && (e += " ... ")),
				"<Buffer " + e + ">"
			},
			l.prototype.compare = function(e, t, r, n, i) {
				if (!l.isBuffer(e)) throw new TypeError("Argument must be a Buffer");
				if (void 0 === t && (t = 0), void 0 === r && (r = e ? e.length: 0), void 0 === n && (n = 0), void 0 === i && (i = this.length), t < 0 || r > e.length || n < 0 || i > this.length) throw new RangeError("out of range index");
				if (i <= n && r <= t) return 0;
				if (i <= n) return - 1;
				if (r <= t) return 1;
				if (this === e) return 0;
				for (var o = (i >>>= 0) - (n >>>= 0), a = (r >>>= 0) - (t >>>= 0), s = Math.min(o, a), u = this.slice(n, i), c = e.slice(t, r), h = 0; h < s; ++h) if (u[h] !== c[h]) {
					o = u[h],
					a = c[h];
					break
				}
				return o < a ? -1 : a < o ? 1 : 0
			},
			l.prototype.includes = function(e, t, r) {
				return - 1 !== this.indexOf(e, t, r)
			},
			l.prototype.indexOf = function(e, t, r) {
				return f(this, e, t, r, !0)
			},
			l.prototype.lastIndexOf = function(e, t, r) {
				return f(this, e, t, r, !1)
			},
			l.prototype.write = function(e, t, r, n) {
				if (void 0 === t) n = "utf8",
				r = this.length,
				t = 0;
				else if (void 0 === r && "string" == typeof t) n = t,
				r = this.length,
				t = 0;
				else {
					if (!isFinite(t)) throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
					t |= 0,
					isFinite(r) ? (r |= 0, void 0 === n && (n = "utf8")) : (n = r, r = void 0)
				}
				var i, o, a, s, u, c, h, l, f, d = this.length - t;
				if ((void 0 === r || d < r) && (r = d), 0 < e.length && (r < 0 || t < 0) || t > this.length) throw new RangeError("Attempt to write outside buffer bounds");
				n || (n = "utf8");
				for (var p = !1;;) switch (n) {
				case "hex":
					return v(this, e, t, r);
				case "utf8":
				case "utf-8":
					return l = t,
					f = r,
					N(O(e, (h = this).length - l), h, l, f);
				case "ascii":
					return g(this, e, t, r);
				case "latin1":
				case "binary":
					return g(this, e, t, r);
				case "base64":
					return s = this,
					u = t,
					c = r,
					N(k(e), s, u, c);
				case "ucs2":
				case "ucs-2":
				case "utf16le":
				case "utf-16le":
					return o = t,
					a = r,
					N(function(e, t) {
						for (var r, n, i, o = [], a = 0; a < e.length && !((t -= 2) < 0); ++a) r = e.charCodeAt(a),
						n = r >> 8,
						i = r % 256,
						o.push(i),
						o.push(n);
						return o
					} (e, (i = this).length - o), i, o, a);
				default:
					if (p) throw new TypeError("Unknown encoding: " + n);
					n = ("" + n).toLowerCase(),
					p = !0
				}
			},
			l.prototype.toJSON = function() {
				return {
					type: "Buffer",
					data: Array.prototype.slice[_0x2654("0x4c")](this._arr || this, 0)
				}
			};
			var R = 4096;
			l[_0x2654("0x8")].slice = function(e, t) {
				var r, n = this.length;
				if ((e = ~~e) < 0 ? (e += n) < 0 && (e = 0) : n < e && (e = n), (t = void 0 === t ? n: ~~t) < 0 ? (t += n) < 0 && (t = 0) : n < t && (t = n), t < e && (t = e), l.TYPED_ARRAY_SUPPORT)(r = this.subarray(e, t)).__proto__ = l.prototype;
				else {
					var i = t - e;
					r = new l(i, void 0);
					for (var o = 0; o < i; ++o) r[o] = this[o + e]
				}
				return r
			},
			l.prototype.readUIntLE = function(e, t, r) {
				e |= 0,
				t |= 0,
				r || _(e, t, this.length);
				for (var n = this[e], i = 1, o = 0; ++o < t && (i *= 256);) n += this[e + o] * i;
				return n
			},
			l.prototype.readUIntBE = function(e, t, r) {
				e |= 0,
				t |= 0,
				r || _(e, t, this.length);
				for (var n = this[e + --t], i = 1; 0 < t && (i *= 256);) n += this[e + --t] * i;
				return n
			},
			l.prototype.readUInt8 = function(e, t) {
				return t || _(e, 1, this.length),
				this[e]
			},
			l.prototype.readUInt16LE = function(e, t) {
				return t || _(e, 2, this.length),
				this[e] | this[e + 1] << 8
			},
			l[_0x2654("0x8")].readUInt16BE = function(e, t) {
				return t || _(e, 2, this.length),
				this[e] << 8 | this[e + 1]
			},
			l[_0x2654("0x8")].readUInt32LE = function(e, t) {
				return t || _(e, 4, this.length),
				(this[e] | this[e + 1] << 8 | this[e + 2] << 16) + 16777216 * this[e + 3]
			},
			l.prototype.readUInt32BE = function(e, t) {
				return t || _(e, 4, this.length),
				16777216 * this[e] + (this[e + 1] << 16 | this[e + 2] << 8 | this[e + 3])
			},
			l.prototype.readIntLE = function(e, t, r) {
				e |= 0,
				t |= 0,
				r || _(e, t, this.length);
				for (var n = this[e], i = 1, o = 0; ++o < t && (i *= 256);) n += this[e + o] * i;
				return (i *= 128) <= n && (n -= Math.pow(2, 8 * t)),
				n
			},
			l.prototype.readIntBE = function(e, t, r) {
				e |= 0,
				t |= 0,
				r || _(e, t, this.length);
				for (var n = t,
				i = 1,
				o = this[e + --n]; 0 < n && (i *= 256);) o += this[e + --n] * i;
				return (i *= 128) <= o && (o -= Math.pow(2, 8 * t)),
				o
			},
			l.prototype.readInt8 = function(e, t) {
				return t || _(e, 1, this.length),
				128 & this[e] ? -1 * (255 - this[e] + 1) : this[e]
			},
			l.prototype.readInt16LE = function(e, t) {
				t || _(e, 2, this.length);
				var r = this[e] | this[e + 1] << 8;
				return 32768 & r ? 4294901760 | r: r
			},
			l.prototype[_0x2654("0x4d")] = function(e, t) {
				t || _(e, 2, this[_0x2654("0x11")]);
				var r = this[e + 1] | this[e] << 8;
				return 32768 & r ? 4294901760 | r: r
			},
			l.prototype.readInt32LE = function(e, t) {
				return t || _(e, 4, this.length),
				this[e] | this[e + 1] << 8 | this[e + 2] << 16 | this[e + 3] << 24
			},
			l.prototype.readInt32BE = function(e, t) {
				return t || _(e, 4, this.length),
				this[e] << 24 | this[e + 1] << 16 | this[e + 2] << 8 | this[e + 3]
			},
			l.prototype.readFloatLE = function(e, t) {
				return t || _(e, 4, this.length),
				I.read(this, e, !0, 23, 4)
			},
			l.prototype.readFloatBE = function(e, t) {
				return t || _(e, 4, this.length),
				I.read(this, e, !1, 23, 4)
			},
			l.prototype.readDoubleLE = function(e, t) {
				return t || _(e, 8, this.length),
				I.read(this, e, !0, 52, 8)
			},
			l.prototype.readDoubleBE = function(e, t) {
				return t || _(e, 8, this.length),
				I.read(this, e, !1, 52, 8)
			},
			l[_0x2654("0x8")][_0x2654("0x4e")] = function(e, t, r, n) { (e = +e, t |= 0, r |= 0, n) || x(this, e, t, r, Math.pow(2, 8 * r) - 1, 0);
				var i = 1,
				o = 0;
				for (this[t] = 255 & e; ++o < r && (i *= 256);) this[t + o] = e / i & 255;
				return t + r
			},
			l.prototype.writeUIntBE = function(e, t, r, n) { (e = +e, t |= 0, r |= 0, n) || x(this, e, t, r, Math.pow(2, 8 * r) - 1, 0);
				var i = r - 1,
				o = 1;
				for (this[t + i] = 255 & e; 0 <= --i && (o *= 256);) this[t + i] = e / o & 255;
				return t + r
			},
			l.prototype.writeUInt8 = function(e, t, r) {
				return e = +e,
				t |= 0,
				r || x(this, e, t, 1, 255, 0),
				l.TYPED_ARRAY_SUPPORT || (e = Math.floor(e)),
				this[t] = 255 & e,
				t + 1
			},
			l[_0x2654("0x8")].writeUInt16LE = function(e, t, r) {
				return e = +e,
				t |= 0,
				r || x(this, e, t, 2, 65535, 0),
				l.TYPED_ARRAY_SUPPORT ? (this[t] = 255 & e, this[t + 1] = e >>> 8) : E(this, e, t, !0),
				t + 2
			},
			l.prototype.writeUInt16BE = function(e, t, r) {
				return e = +e,
				t |= 0,
				r || x(this, e, t, 2, 65535, 0),
				l.TYPED_ARRAY_SUPPORT ? (this[t] = e >>> 8, this[t + 1] = 255 & e) : E(this, e, t, !1),
				t + 2
			},
			l.prototype.writeUInt32LE = function(e, t, r) {
				return e = +e,
				t |= 0,
				r || x(this, e, t, 4, 4294967295, 0),
				l.TYPED_ARRAY_SUPPORT ? (this[t + 3] = e >>> 24, this[t + 2] = e >>> 16, this[t + 1] = e >>> 8, this[t] = 255 & e) : S(this, e, t, !0),
				t + 4
			},
			l.prototype.writeUInt32BE = function(e, t, r) {
				return e = +e,
				t |= 0,
				r || x(this, e, t, 4, 4294967295, 0),
				l.TYPED_ARRAY_SUPPORT ? (this[t] = e >>> 24, this[t + 1] = e >>> 16, this[t + 2] = e >>> 8, this[t + 3] = 255 & e) : S(this, e, t, !1),
				t + 4
			},
			l.prototype.writeIntLE = function(e, t, r, n) {
				if (e = +e, t |= 0, !n) {
					var i = Math.pow(2, 8 * r - 1);
					x(this, e, t, r, i - 1, -i)
				}
				var o = 0,
				a = 1,
				s = 0;
				for (this[t] = 255 & e; ++o < r && (a *= 256);) e < 0 && 0 === s && 0 !== this[t + o - 1] && (s = 1),
				this[t + o] = (e / a >> 0) - s & 255;
				return t + r
			},
			l.prototype.writeIntBE = function(e, t, r, n) {
				if (e = +e, t |= 0, !n) {
					var i = Math[_0x2654("0x4f")](2, 8 * r - 1);
					x(this, e, t, r, i - 1, -i)
				}
				var o = r - 1,
				a = 1,
				s = 0;
				for (this[t + o] = 255 & e; 0 <= --o && (a *= 256);) e < 0 && 0 === s && 0 !== this[t + o + 1] && (s = 1),
				this[t + o] = (e / a >> 0) - s & 255;
				return t + r
			},
			l.prototype.writeInt8 = function(e, t, r) {
				return e = +e,
				t |= 0,
				r || x(this, e, t, 1, 127, -128),
				l.TYPED_ARRAY_SUPPORT || (e = Math.floor(e)),
				e < 0 && (e = 255 + e + 1),
				this[t] = 255 & e,
				t + 1
			},
			l[_0x2654("0x8")].writeInt16LE = function(e, t, r) {
				return e = +e,
				t |= 0,
				r || x(this, e, t, 2, 32767, -32768),
				l.TYPED_ARRAY_SUPPORT ? (this[t] = 255 & e, this[t + 1] = e >>> 8) : E(this, e, t, !0),
				t + 2
			},
			l.prototype.writeInt16BE = function(e, t, r) {
				return e = +e,
				t |= 0,
				r || x(this, e, t, 2, 32767, -32768),
				l[_0x2654("0x50")] ? (this[t] = e >>> 8, this[t + 1] = 255 & e) : E(this, e, t, !1),
				t + 2
			},
			l.prototype.writeInt32LE = function(e, t, r) {
				return e = +e,
				t |= 0,
				r || x(this, e, t, 4, 2147483647, -2147483648),
				l.TYPED_ARRAY_SUPPORT ? (this[t] = 255 & e, this[t + 1] = e >>> 8, this[t + 2] = e >>> 16, this[t + 3] = e >>> 24) : S(this, e, t, !0),
				t + 4
			},
			l.prototype.writeInt32BE = function(e, t, r) {
				return e = +e,
				t |= 0,
				r || x(this, e, t, 4, 2147483647, -2147483648),
				e < 0 && (e = 4294967295 + e + 1),
				l.TYPED_ARRAY_SUPPORT ? (this[t] = e >>> 24, this[t + 1] = e >>> 16, this[t + 2] = e >>> 8, this[t + 3] = 255 & e) : S(this, e, t, !1),
				t + 4
			},
			l.prototype.writeFloatLE = function(e, t, r) {
				return A(this, e, t, !0, r)
			},
			l.prototype.writeFloatBE = function(e, t, r) {
				return A(this, e, t, !1, r)
			},
			l.prototype.writeDoubleLE = function(e, t, r) {
				return T(this, e, t, !0, r)
			},
			l.prototype.writeDoubleBE = function(e, t, r) {
				return T(this, e, t, !1, r)
			},
			l.prototype.copy = function(e, t, r, n) {
				if (r || (r = 0), n || 0 === n || (n = this.length), t >= e.length && (t = e.length), t || (t = 0), 0 < n && n < r && (n = r), n === r) return 0;
				if (0 === e.length || 0 === this.length) return 0;
				if (t < 0) throw new RangeError("targetStart out of bounds");
				if (r < 0 || r >= this.length) throw new RangeError("sourceStart out of bounds");
				if (n < 0) throw new RangeError("sourceEnd out of bounds");
				n > this.length && (n = this.length),
				e.length - t < n - r && (n = e.length - t + r);
				var i, o = n - r;
				if (this === e && r < t && t < n) for (i = o - 1; 0 <= i; --i) e[i + t] = this[i + r];
				else if (o < 1e3 || !l.TYPED_ARRAY_SUPPORT) for (i = 0; i < o; ++i) e[i + t] = this[i + r];
				else Uint8Array[_0x2654("0x8")].set.call(e, this.subarray(r, r + o), t);
				return o
			},
			l.prototype.fill = function(e, t, r, n) {
				if ("string" == typeof e) {
					if ("string" == typeof t ? (n = t, t = 0, r = this.length) : "string" == typeof r && (n = r, r = this.length), 1 === e.length) {
						var i = e.charCodeAt(0);
						i < 256 && (e = i)
					}
					if (void 0 !== n && "string" != typeof n) throw new TypeError("encoding must be a string");
					if ("string" == typeof n && !l.isEncoding(n)) throw new TypeError("Unknown encoding: " + n)
				} else "number" == typeof e && (e &= 255);
				if (t < 0 || this.length < t || this.length < r) throw new RangeError("Out of range index");
				if (r <= t) return this;
				var o;
				if (t >>>= 0, r = void 0 === r ? this.length: r >>> 0, e || (e = 0), "number" == typeof e) for (o = t; o < r; ++o) this[o] = e;
				else {
					var a = l.isBuffer(e) ? e: O(new l(e, n).toString()),
					s = a.length;
					for (o = 0; o < r - t; ++o) this[o + t] = a[o % s]
				}
				return this
			};
			var D = /[^+\/0-9A-Za-z-_]/g
		}).call(M,
		function() {
			return this
		} ())
	},
	function(e, t) {
		"use strict";
		function h(e) {
			var t = e.length;
			if (0 < t % 4) throw new Error("Invalid string. Length must be a multiple of 4");
			var r = e.indexOf("=");
			return - 1 === r && (r = t),
			[r, r === t ? 0 : 4 - r % 4]
		}
		function s(e, t, r) {
			for (var n, i = [], o = t; o < r; o += 3) n = (e[o] << 16 & 16711680) + (e[o + 1] << 8 & 65280) + (255 & e[o + 2]),
			i.push(u[(a = n) >> 18 & 63] + u[a >> 12 & 63] + u[a >> 6 & 63] + u[63 & a]);
			var a;
			return i.join("")
		}
		t.byteLength = function(e) {
			var t = h(e),
			r = t[0],
			n = t[1];
			return 3 * (r + n) / 4 - n
		},
		t.toByteArray = function(e) {
			for (var t, r = h(e), n = r[0], i = r[1], o = new f(3 * (n + (c = i)) / 4 - c), a = 0, s = 0 < i ? n - 4 : n, u = 0; u < s; u += 4) t = l[e.charCodeAt(u)] << 18 | l[e.charCodeAt(u + 1)] << 12 | l[e.charCodeAt(u + 2)] << 6 | l[e.charCodeAt(u + 3)],
			o[a++] = t >> 16 & 255,
			o[a++] = t >> 8 & 255,
			o[a++] = 255 & t;
			var c;
			return 2 === i && (t = l[e.charCodeAt(u)] << 2 | l[e.charCodeAt(u + 1)] >> 4, o[a++] = 255 & t),
			1 === i && (t = l[e.charCodeAt(u)] << 10 | l[e.charCodeAt(u + 1)] << 4 | l[e.charCodeAt(u + 2)] >> 2, o[a++] = t >> 8 & 255, o[a++] = 255 & t),
			o
		},
		t.fromByteArray = function(e) {
			for (var t, r = e.length,
			n = r % 3,
			i = [], o = 0, a = r - n; o < a; o += 16383) i.push(s(e, o, a < o + 16383 ? a: o + 16383));
			return 1 === n ? (t = e[r - 1], i.push(u[t >> 2] + u[t << 4 & 63] + "==")) : 2 === n && (t = (e[r - 2] << 8) + e[r - 1], i.push(u[t >> 10] + u[t >> 4 & 63] + u[t << 2 & 63] + "=")),
			i.join("")
		};
		for (var u = [], l = [], f = "undefined" != typeof Uint8Array ? Uint8Array: Array, r = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", n = 0, i = r.length; n < i; ++n) u[n] = r[n],
		l[r.charCodeAt(n)] = n;
		l["-".charCodeAt(0)] = 62,
		l["_".charCodeAt(0)] = 63
	},
	function(e, t) {
		t.read = function(e, t, r, n, i) {
			var o, a, s = 8 * i - n - 1,
			u = (1 << s) - 1,
			c = u >> 1,
			h = -7,
			l = r ? i - 1 : 0,
			f = r ? -1 : 1,
			d = e[t + l];
			for (l += f, o = d & (1 << -h) - 1, d >>= -h, h += s; 0 < h; o = 256 * o + e[t + l], l += f, h -= 8);
			for (a = o & (1 << -h) - 1, o >>= -h, h += n; 0 < h; a = 256 * a + e[t + l], l += f, h -= 8);
			if (0 === o) o = 1 - c;
			else {
				if (o === u) return a ? NaN: 1 / 0 * (d ? -1 : 1);
				a += Math.pow(2, n),
				o -= c
			}
			return (d ? -1 : 1) * a * Math.pow(2, o - n)
		},
		t.write = function(e, t, r, n, i, o) {
			var a, s, u, c = 8 * o - i - 1,
			h = (1 << c) - 1,
			l = h >> 1,
			f = 23 === i ? Math[_0x2654("0x4f")](2, -24) - Math.pow(2, -77) : 0,
			d = n ? 0 : o - 1,
			p = n ? 1 : -1,
			v = t < 0 || 0 === t && 1 / t < 0 ? 1 : 0;
			for (t = Math.abs(t), isNaN(t) || t === 1 / 0 ? (s = isNaN(t) ? 1 : 0, a = h) : (a = Math.floor(Math.log(t) / Math.LN2), t * (u = Math.pow(2, -a)) < 1 && (a--, u *= 2), 2 <= (t += 1 <= a + l ? f / u: f * Math.pow(2, 1 - l)) * u && (a++, u /= 2), h <= a + l ? (s = 0, a = h) : 1 <= a + l ? (s = (t * u - 1) * Math.pow(2, i), a += l) : (s = t * Math.pow(2, l - 1) * Math.pow(2, i), a = 0)); 8 <= i; e[r + d] = 255 & s, d += p, s /= 256, i -= 8);
			for (a = a << i | s, c += i; 0 < c; e[r + d] = 255 & a, d += p, a /= 256, c -= 8);
			e[r + d - p] |= 128 * v
		}
	},
	function(e, t) {
		var r = {}.toString;
		e.exports = Array.isArray ||
		function(e) {
			return "[object Array]" == r.call(e)
		}
	},
	function(e, t) {
		"use strict";
		Object.defineProperty(t, "__esModule", {
			value: !0
		});
		var r = function() {
			function n(e, t) {
				for (var r = 0; r < t.length; r++) {
					var n = t[r];
					n.enumerable = n.enumerable || !1,
					n.configurable = !0,
					"value" in n && (n[_0x2654("0x51")] = !0),
					Object.defineProperty(e, n.key, n)
				}
			}
			return function(e, t, r) {
				return t && n(e[_0x2654("0x8")], t),
				r && n(e, r),
				e
			}
		} (),
		n = function() {
			function n(e, t, r) { (function(e, t) {
					if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
				})(this, n),
				this.type = e,
				this.vedio = t,
				this.actions = r
			}
			return r(n, [{
				key: "getType",
				value: function() {
					return this.type
				}
			},
			{
				key: "getVedio",
				value: function() {
					return this[_0x2654("0x52")]
				}
			},
			{
				key: "getVedioFrameLength",
				value: function() {
					return this.vedio ? this.vedio.getFramesLength() : 0
				}
			},
			{
				key: "getActions",
				value: function() {
					return this.actions
				}
			},
			{
				key: "getActionsLength",
				value: function() {
					return this.actions.length
				}
			}]),
			n
		} ();
		t["default"] = n
	},
	function(e, t) {
		"use strict";
		e.exports.sendToServer = function(e, t) {
			var r = new XMLHttpRequest;
			r.open("POST", e),
			r.setRequestHeader("Content-Type", "application/json"),
			r.onreadystatechange = function() {
				r.readyState == XMLHttpRequest.DONE && r.status
			},
			r.send(t)
		}
	},
	function(e, t, r) {
		var n, i = function() {
			function r(e, t) {
				if (!o[e]) {
					o[e] = {};
					for (var r = 0; r < e.length; r++) o[e][e.charAt(r)] = r
				}
				return o[e][t]
			}
			var y = String.fromCharCode,
			n = _0x2654("0x53"),
			i = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$",
			o = {},
			a = {
				compressToBase64: function(e) {
					if (null == e) return "";
					var t = a._compress(e, 6,
					function(e) {
						return n.charAt(e)
					});
					switch (t.length % 4) {
					default:
					case 0:
						return t;
					case 1:
						return t + "===";
					case 2:
						return t + "==";
					case 3:
						return t + "="
					}
				},
				decompressFromBase64: function(t) {
					return null == t ? "": "" == t ? null: a._decompress(t.length, 32,
					function(e) {
						return r(n, t.charAt(e))
					})
				},
				compressToUTF16: function(e) {
					return null == e ? "": a._compress(e, 15,
					function(e) {
						return y(e + 32)
					}) + " "
				},
				decompressFromUTF16: function(t) {
					return null == t ? "": "" == t ? null: a._decompress(t.length, 16384,
					function(e) {
						return t.charCodeAt(e) - 32
					})
				},
				compressToUint8Array: function(e) {
					for (var t = a.compress(e), r = new Uint8Array(2 * t.length), n = 0, i = t.length; n < i; n++) {
						var o = t.charCodeAt(n);
						r[2 * n] = o >>> 8,
						r[2 * n + 1] = o % 256
					}
					return r
				},
				decompressFromUint8Array: function(e) {
					if (null == e) return a.decompress(e);
					for (var t = new Array(e.length / 2), r = 0, n = t.length; r < n; r++) t[r] = 256 * e[2 * r] + e[2 * r + 1];
					var i = [];
					return t.forEach(function(e) {
						i.push(y(e))
					}),
					a[_0x2654("0x54")](i.join(""))
				},
				compressToEncodedURIComponent: function(e) {
					return null == e ? "": a._compress(e, 6,
					function(e) {
						return i.charAt(e)
					})
				},
				decompressFromEncodedURIComponent: function(t) {
					return null == t ? "": "" == t ? null: (t = t[_0x2654("0x44")](/ /g, "+"), a._decompress(t.length, 32,
					function(e) {
						return r(i, t.charAt(e))
					}))
				},
				compress: function(e) {
					return a[_0x2654("0x55")](e, 16,
					function(e) {
						return y(e)
					})
				},
				_compress: function(e, t, r) {
					if (null == e) return "";
					var n, i, o, a = {},
					s = {},
					u = "",
					c = "",
					h = "",
					l = 2,
					f = 3,
					d = 2,
					p = [],
					v = 0,
					g = 0;
					for (o = 0; o < e.length; o += 1) if (u = e.charAt(o), Object.prototype.hasOwnProperty.call(a, u) || (a[u] = f++, s[u] = !0), c = h + u, Object.prototype.hasOwnProperty.call(a, c)) h = c;
					else {
						if (Object.prototype.hasOwnProperty.call(s, h)) {
							if (h.charCodeAt(0) < 256) {
								for (n = 0; n < d; n++) v <<= 1,
								g == t - 1 ? (g = 0, p.push(r(v)), v = 0) : g++;
								for (i = h.charCodeAt(0), n = 0; n < 8; n++) v = v << 1 | 1 & i,
								g == t - 1 ? (g = 0, p.push(r(v)), v = 0) : g++,
								i >>= 1
							} else {
								for (i = 1, n = 0; n < d; n++) v = v << 1 | i,
								g == t - 1 ? (g = 0, p.push(r(v)), v = 0) : g++,
								i = 0;
								for (i = h.charCodeAt(0), n = 0; n < 16; n++) v = v << 1 | 1 & i,
								g == t - 1 ? (g = 0, p.push(r(v)), v = 0) : g++,
								i >>= 1
							}
							0 == --l && (l = Math.pow(2, d), d++),
							delete s[h]
						} else for (i = a[h], n = 0; n < d; n++) v = v << 1 | 1 & i,
						g == t - 1 ? (g = 0, p.push(r(v)), v = 0) : g++,
						i >>= 1;
						0 == --l && (l = Math.pow(2, d), d++),
						a[c] = f++,
						h = String(u)
					}
					if ("" !== h) {
						if (Object.prototype.hasOwnProperty.call(s, h)) {
							if (h.charCodeAt(0) < 256) {
								for (n = 0; n < d; n++) v <<= 1,
								g == t - 1 ? (g = 0, p.push(r(v)), v = 0) : g++;
								for (i = h.charCodeAt(0), n = 0; n < 8; n++) v = v << 1 | 1 & i,
								g == t - 1 ? (g = 0, p[_0x2654("0x13")](r(v)), v = 0) : g++,
								i >>= 1
							} else {
								for (i = 1, n = 0; n < d; n++) v = v << 1 | i,
								g == t - 1 ? (g = 0, p.push(r(v)), v = 0) : g++,
								i = 0;
								for (i = h.charCodeAt(0), n = 0; n < 16; n++) v = v << 1 | 1 & i,
								g == t - 1 ? (g = 0, p.push(r(v)), v = 0) : g++,
								i >>= 1
							}
							0 == --l && (l = Math.pow(2, d), d++),
							delete s[h]
						} else for (i = a[h], n = 0; n < d; n++) v = v << 1 | 1 & i,
						g == t - 1 ? (g = 0, p.push(r(v)), v = 0) : g++,
						i >>= 1;
						0 == --l && (l = Math.pow(2, d), d++)
					}
					for (i = 2, n = 0; n < d; n++) v = v << 1 | 1 & i,
					g == t - 1 ? (g = 0, p.push(r(v)), v = 0) : g++,
					i >>= 1;
					for (;;) {
						if (v <<= 1, g == t - 1) {
							p.push(r(v));
							break
						}
						g++
					}
					return p.join("")
				},
				decompress: function(t) {
					return null == t ? "": "" == t ? null: a._decompress(t.length, 32768,
					function(e) {
						return t.charCodeAt(e)
					})
				},
				_decompress: function(e, t, r) {
					var n, i, o, a, s, u, c, h = [],
					l = 4,
					f = 4,
					d = 3,
					p = "",
					v = [],
					g = {
						val: r(0),
						position: t,
						index: 1
					};
					for (n = 0; n < 3; n += 1) h[n] = n;
					for (o = 0, s = Math.pow(2, 2), u = 1; u != s;) a = g.val & g.position,
					g.position >>= 1,
					0 == g.position && (g.position = t, g.val = r(g.index++)),
					o |= (0 < a ? 1 : 0) * u,
					u <<= 1;
					switch (o) {
					case 0:
						for (o = 0, s = Math.pow(2, 8), u = 1; u != s;) a = g.val & g.position,
						g.position >>= 1,
						0 == g.position && (g.position = t, g.val = r(g.index++)),
						o |= (0 < a ? 1 : 0) * u,
						u <<= 1;
						c = y(o);
						break;
					case 1:
						for (o = 0, s = Math.pow(2, 16), u = 1; u != s;) a = g[_0x2654("0x56")] & g.position,
						g.position >>= 1,
						0 == g.position && (g.position = t, g.val = r(g.index++)),
						o |= (0 < a ? 1 : 0) * u,
						u <<= 1;
						c = y(o);
						break;
					case 2:
						return ""
					}
					for (i = h[3] = c, v.push(c);;) {
						if (g.index > e) return "";
						for (o = 0, s = Math.pow(2, d), u = 1; u != s;) a = g.val & g.position,
						g.position >>= 1,
						0 == g.position && (g.position = t, g.val = r(g.index++)),
						o |= (0 < a ? 1 : 0) * u,
						u <<= 1;
						switch (c = o) {
						case 0:
							for (o = 0, s = Math.pow(2, 8), u = 1; u != s;) a = g.val & g.position,
							g.position >>= 1,
							0 == g.position && (g.position = t, g.val = r(g.index++)),
							o |= (0 < a ? 1 : 0) * u,
							u <<= 1;
							h[f++] = y(o),
							c = f - 1,
							l--;
							break;
						case 1:
							for (o = 0, s = Math.pow(2, 16), u = 1; u != s;) a = g.val & g.position,
							g.position >>= 1,
							0 == g.position && (g.position = t, g.val = r(g.index++)),
							o |= (0 < a ? 1 : 0) * u,
							u <<= 1;
							h[f++] = y(o),
							c = f - 1,
							l--;
							break;
						case 2:
							return v.join("")
						}
						if (0 == l && (l = Math.pow(2, d), d++), h[c]) p = h[c];
						else {
							if (c !== f) return null;
							p = i + i.charAt(0)
						}
						v.push(p),
						h[f++] = i + p.charAt(0),
						i = p,
						0 == --l && (l = Math.pow(2, d), d++)
					}
				}
			};
			return a
		} ();
		void 0 === (n = function() {
			return i
		}.call(t, r, t, e)) || (e.exports = n)
	}])
});