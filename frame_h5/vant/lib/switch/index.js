"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports["default"] = void 0;

var _babelHelperVueJsxMergeProps = _interopRequireDefault(require("@vue/babel-helper-vue-jsx-merge-props"));

var _utils = require("../utils");

var _loading = _interopRequireDefault(require("../loading"));

var _shared = require("./shared");

var _functional = require("../utils/functional");

var _use = (0, _utils.use)('switch'),
    sfc = _use[0],
    bem = _use[1];

function Switch(h, props, slots, ctx) {
  var value = props.value,
      loading = props.loading,
      disabled = props.disabled,
      activeValue = props.activeValue,
      inactiveValue = props.inactiveValue;
  var style = {
    fontSize: props.size,
    backgroundColor: value ? props.activeColor : props.inactiveColor
  };

  var onClick = function onClick() {
    if (!disabled && !loading) {
      var newValue = value === activeValue ? inactiveValue : activeValue;
      (0, _functional.emit)(ctx, 'input', newValue);
      (0, _functional.emit)(ctx, 'change', newValue);
    }
  };

  return h("div", (0, _babelHelperVueJsxMergeProps["default"])([{
    "class": bem({
      on: value === activeValue,
      disabled: disabled
    }),
    "style": style,
    "on": {
      "click": onClick
    }
  }, (0, _functional.inherit)(ctx)]), [h("div", {
    "class": bem('node')
  }, [loading && h(_loading["default"], {
    "class": bem('loading')
  })])]);
}

Switch.props = _shared.switchProps;

var _default = sfc(Switch);

exports["default"] = _default;