"use strict";

exports.__esModule = true;
exports.isSrc = isSrc;

/**
 * Is image source
 */
function isSrc(url) {
  return /^(https?:)?\/\/|data:image/.test(url);
}