"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports["default"] = void 0;

var _babelHelperVueJsxMergeProps = _interopRequireDefault(require("@vue/babel-helper-vue-jsx-merge-props"));

var _utils = require("../utils");

var _functional = require("../utils/functional");

var _info = _interopRequireDefault(require("../info"));

var _src = require("../utils/validate/src");

var _use = (0, _utils.use)('icon'),
    sfc = _use[0];

function Icon(h, props, slots, ctx) {
  var urlIcon = (0, _src.isSrc)(props.name);
  return h(props.tag, (0, _babelHelperVueJsxMergeProps["default"])([{
    "class": [props.classPrefix, urlIcon ? 'van-icon--image' : props.classPrefix + "-" + props.name],
    "style": {
      color: props.color,
      fontSize: props.size
    }
  }, (0, _functional.inherit)(ctx, true)]), [slots["default"] && slots["default"](), urlIcon && h("img", {
    "attrs": {
      "src": props.name
    }
  }), h(_info["default"], {
    "attrs": {
      "info": props.info
    }
  })]);
}

Icon.props = {
  name: String,
  size: String,
  color: String,
  info: [String, Number],
  tag: {
    type: String,
    "default": 'i'
  },
  classPrefix: {
    type: String,
    "default": 'van-icon'
  }
};

var _default = sfc(Icon);

exports["default"] = _default;