## 更新日志

## [v1.6.16](https://github.com/youzan/vant/tree/v1.6.16) (2019-04-26)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.15...v1.6.16)

**Breaking changes**

- 如果Toast先弹出过，因为z-indx计算，会被后使用的popup组件遮住，如果在popup里面需要使用Toast的话，就会被遮住了，这该如何解决？ [\#3145](https://github.com/youzan/vant/issues/3145)
- SubmitBar 的price属性如何显示成整数 [\#3144](https://github.com/youzan/vant/issues/3144)
- Card 组件问题 [\#3126](https://github.com/youzan/vant/issues/3126)

**Bug Fixes**

- \[Popup弹出层\] 组件报错 [\#3142](https://github.com/youzan/vant/issues/3142)

**Issue**

- Swipe组件，传入width [\#3220](https://github.com/youzan/vant/issues/3220)
- Stepper 步进器 [\#3217](https://github.com/youzan/vant/issues/3217)
- Tab 标签页 优化 [\#3216](https://github.com/youzan/vant/issues/3216)
- 是酱紫的，俺执行yarn build:lib，然后看到生成有lib和es文件目录，请问lib目录和es目录哪个才是真正vant运行时需要的文件夹？还是都需要呢？还有打包时间蛮长的哦。谢谢。 [\#3211](https://github.com/youzan/vant/issues/3211)
- 1.6.15版本 Dialog组件before-close回调无法触发 [\#3210](https://github.com/youzan/vant/issues/3210)
- tree select 能否添加一个多选呢? [\#3205](https://github.com/youzan/vant/issues/3205)
- SwipeItem的click事件无效 [\#3201](https://github.com/youzan/vant/issues/3201)
- swiper源码宽高获取错误 [\#3200](https://github.com/youzan/vant/issues/3200)
- Uploader chrome 模拟器上可以多选图片并且能调用after-read的方法，手机上选择单图是可以执行after-read方法的，但是选择多图就不执行after-read方法 [\#3199](https://github.com/youzan/vant/issues/3199)
- ES [\#3198](https://github.com/youzan/vant/issues/3198)
- button组件自定义 [\#3197](https://github.com/youzan/vant/issues/3197)
- 希望tabs组件增加一个监控左右滑动切换事件 [\#3192](https://github.com/youzan/vant/issues/3192)
- van-tabbar可以支持大于4个以上可左右滑动吗？ [\#3191](https://github.com/youzan/vant/issues/3191)
- Popup嵌套CouponList点击上层蒙版，蒙版消失，couponlist不消失 [\#3190](https://github.com/youzan/vant/issues/3190)
- ios 机型 用input输入框很难选中 [\#3189](https://github.com/youzan/vant/issues/3189)
- 国际化问题 [\#3188](https://github.com/youzan/vant/issues/3188)
- 使用safe-area-inset-bottom="true"会导致console错误 [\#3187](https://github.com/youzan/vant/issues/3187)
- ios机型NumberKeyboard输入错位 [\#3186](https://github.com/youzan/vant/issues/3186)
- tabs的title-active-color不生效 [\#3185](https://github.com/youzan/vant/issues/3185)
- 希望有个form验证组件，结合async-validator [\#3183](https://github.com/youzan/vant/issues/3183)
- 希望给search组件加上focus方法 [\#3181](https://github.com/youzan/vant/issues/3181)
- \*\*Environment\*\* Circle 环形进度条在手机上的适配问题，两个环形会分离。 [\#3170](https://github.com/youzan/vant/issues/3170)
- Tabs使用的bug [\#3164](https://github.com/youzan/vant/issues/3164)
- 按钮的box-shadow在IOS问题  [\#3159](https://github.com/youzan/vant/issues/3159)
- list 组件与 PullRefresh 组件结合使用时会一直执行 onLoad 方法 [\#3158](https://github.com/youzan/vant/issues/3158)
- picker只能点击，不能滑动 [\#3121](https://github.com/youzan/vant/issues/3121)
- 关于建议van-address-edit组件的address-info中省市区回显参数的说明 [\#3106](https://github.com/youzan/vant/issues/3106)

**Improvements**

- \[bugfix\] Area: change event values incorrect [\#3219](https://github.com/youzan/vant/pull/3219) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] ContactList: not trigger select event when click radio icon [\#3218](https://github.com/youzan/vant/pull/3218) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] AddressList: not trigger select event when click radio icon [\#3214](https://github.com/youzan/vant/pull/3214) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] Row: align bottom not work [\#3209](https://github.com/youzan/vant/pull/3209) ([chenjiahan](https://github.com/chenjiahan))
- \[Doc\] English link to MIT License [\#3208](https://github.com/youzan/vant/pull/3208) ([vd3v](https://github.com/vd3v))
- \[bugfix\] Slider: fix slider drag and `value` props change at the same time can not drag success [\#3206](https://github.com/youzan/vant/pull/3206) ([cookfront](https://github.com/cookfront))
- \[bugfix\] Field: add hack for iOS 12 scroll position [\#3204](https://github.com/youzan/vant/pull/3204) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Icon: update new icon & hot icon [\#3203](https://github.com/youzan/vant/pull/3203) ([chenjiahan](https://github.com/chenjiahan))
- \[Doc\] update PULL\_REQUEST\_TEMPLATE.md [\#3196](https://github.com/youzan/vant/pull/3196) ([chenjiahan](https://github.com/chenjiahan))
- \[Doc\] remove outdated contribution guide [\#3195](https://github.com/youzan/vant/pull/3195) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] NumberKeyboard: add title-left slot [\#3194](https://github.com/youzan/vant/pull/3194) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] Step: add active-icon slot [\#3193](https://github.com/youzan/vant/pull/3193) ([chenjiahan](https://github.com/chenjiahan))

## [v1.6.15](https://github.com/youzan/vant/tree/v1.6.15) (2019-04-19)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.14...v1.6.15)

**Breaking changes**

-  \[Bug Report\] ImagePreview 图片预览 图片放大的时候比原图模糊 [\#3116](https://github.com/youzan/vant/issues/3116)

**Bug Fixes**

- Dialog添加getContainer属性，返回上个界面，再次进入不生效 [\#3101](https://github.com/youzan/vant/issues/3101)

**Issue**

- stepper 步进器 可不可以出一个跟 field组合的样式  [\#3177](https://github.com/youzan/vant/issues/3177)
- van-field中type设置为textarea，并且设了autosize，高度自适应无效 [\#3176](https://github.com/youzan/vant/issues/3176)
- van-uploader 有没有 取消选择事件？ [\#3175](https://github.com/youzan/vant/issues/3175)
- 请问一下List组件滚动的问题 [\#3165](https://github.com/youzan/vant/issues/3165)
- Actionsheet希望有纯JS的使用方式 [\#3163](https://github.com/youzan/vant/issues/3163)
- 【bug】Tag 组件在微信内部浏览器打开后，下边框不显示，亲测好几款手机了 [\#3162](https://github.com/youzan/vant/issues/3162)
- 编辑地址兼容性问题 [\#3161](https://github.com/youzan/vant/issues/3161)
- List 组件不能触发@load事件 [\#3160](https://github.com/youzan/vant/issues/3160)
- 提个临时方案，针对Picker这类组件用到行内样式的rem兼容问题 [\#3156](https://github.com/youzan/vant/issues/3156)
- Sku组件里面添加message输入框，点击输入框的时候，页面不会自动上弹 [\#3146](https://github.com/youzan/vant/issues/3146)
-  Stepper如何限定最小数值 [\#3143](https://github.com/youzan/vant/issues/3143)
- Dialog组件在iPhone7\(iOS 12.1.4\)微信中按钮可点击区域与显示区域不一致 [\#3141](https://github.com/youzan/vant/issues/3141)
- 在安卓4.0，IOS9.0上自带浏览器打不开官方文档 [\#3139](https://github.com/youzan/vant/issues/3139)
- List 组件上拉加载更多自动执行更多方法？ [\#3138](https://github.com/youzan/vant/issues/3138)
- NoticeBar会影响性能，导致页面卡顿 [\#3137](https://github.com/youzan/vant/issues/3137)
- switch 组件 [\#3132](https://github.com/youzan/vant/issues/3132)
- picker 不能rem适配 [\#3128](https://github.com/youzan/vant/issues/3128)
- 启用本地字体图标后，会先尝试加载网络图标造成加载延迟 [\#3112](https://github.com/youzan/vant/issues/3112)
- 关于popup和Actionsheet 弹出层 中 再次弹出选择组件 遮罩显示问题 无法覆盖全屏 [\#3109](https://github.com/youzan/vant/issues/3109)

**Improvements**

- \[improvement\] Icon: update share、edit icon [\#3180](https://github.com/youzan/vant/pull/3180) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] GoodsAction: add safe-area-inset-bottom prop [\#3174](https://github.com/youzan/vant/pull/3174) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] NumberKeyboard: add safe-area-inset-bottom prop [\#3173](https://github.com/youzan/vant/pull/3173) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] SubmitBar: add safe-area-inset-bottom prop [\#3172](https://github.com/youzan/vant/pull/3172) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] Actionsheet: add safe-area-inset-bottom prop [\#3171](https://github.com/youzan/vant/pull/3171) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Swipe: add less vars [\#3169](https://github.com/youzan/vant/pull/3169) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Field: add less vars [\#3168](https://github.com/youzan/vant/pull/3168) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Card: add less vars [\#3167](https://github.com/youzan/vant/pull/3167) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] ImagePreview: add index slot [\#3157](https://github.com/youzan/vant/pull/3157) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] ImagePreview: add change event [\#3155](https://github.com/youzan/vant/pull/3155) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] ImagePreview: support component call [\#3154](https://github.com/youzan/vant/pull/3154) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] Toast: should add z-index if previous toast has not disappeared [\#3153](https://github.com/youzan/vant/pull/3153) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] Card: should not render empty bottom div [\#3152](https://github.com/youzan/vant/pull/3152) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] SubmitBar: add decimal-length prop [\#3151](https://github.com/youzan/vant/pull/3151) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] Popup: may cause event uncancelable warning [\#3150](https://github.com/youzan/vant/pull/3150) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Tabbar: add iPhone X layout feature for fixed tabbar [\#3149](https://github.com/youzan/vant/pull/3149) ([TimonPeng](https://github.com/TimonPeng))
- \[improvement\] Rate: readonly和disabled的时候touchMove不应该做任何事 [\#3148](https://github.com/youzan/vant/pull/3148) ([Litor](https://github.com/Litor))
- \[Doc\] AddressEdit: update doc [\#3147](https://github.com/youzan/vant/pull/3147) ([chenjiahan](https://github.com/chenjiahan))

## [v1.6.14](https://github.com/youzan/vant/tree/v1.6.14) (2019-04-13)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.13...v1.6.14)

**Breaking changes**

- `uploader`组件`before-read`和`after-read`中能否将组件的`ref`传回来呢？ [\#3091](https://github.com/youzan/vant/issues/3091)
- 组件的动画能否考虑使用translate减少对项目样式的侵入 [\#3079](https://github.com/youzan/vant/issues/3079)
- stepper 数值较大时，宽度不能自适应 [\#3052](https://github.com/youzan/vant/issues/3052)

**Bug Fixes**

- tabs 选中项丢失 [\#3073](https://github.com/youzan/vant/issues/3073)
- \[Bug Report\] tag在部分手机如vivo x9上，边框显示不全 [\#3042](https://github.com/youzan/vant/issues/3042)

**Issue**

- 动态添加field,v-model失效 [\#3130](https://github.com/youzan/vant/issues/3130)
- 【ios11】better-scroll内使用swiper导致页面闪烁 [\#3129](https://github.com/youzan/vant/issues/3129)
- 徽标组件info设置为0的时候可以不显示吗 [\#3127](https://github.com/youzan/vant/issues/3127)
- Swipe轮播组件 通过 ref 获取swipe 实例并调用实例方法时报错 [\#3120](https://github.com/youzan/vant/issues/3120)
- 无法获取数据 [\#3119](https://github.com/youzan/vant/issues/3119)
- 页面中有多条van-cell时，会导致页面滑动失去惯性 [\#3118](https://github.com/youzan/vant/issues/3118)
- Stepper步进器default-value属性 无法绑定动态值 [\#3117](https://github.com/youzan/vant/issues/3117)
- van-checkbox的Checkbox Event中click事件点击复选框时无效。 [\#3114](https://github.com/youzan/vant/issues/3114)
- tag组件 移动端文字没有垂直居中 [\#3113](https://github.com/youzan/vant/issues/3113)
- popup组件的实例化导致其他组件的touchmove事件失败 [\#3110](https://github.com/youzan/vant/issues/3110)
- vant从1.5.2升级到1.6.13后报错 [\#3108](https://github.com/youzan/vant/issues/3108)
- van-swipe轮播的时候页面抖动 [\#3105](https://github.com/youzan/vant/issues/3105)
- van-picker @change 方法如何传递选中值和自定义参数? [\#3102](https://github.com/youzan/vant/issues/3102)
- stepper [\#3081](https://github.com/youzan/vant/issues/3081)
- Popup 弹出层  使用input时候异常关闭 [\#3070](https://github.com/youzan/vant/issues/3070)

**Improvements**

- \[bugfix\] Step: step 销毁之前从父 steps 中移除 [\#3140](https://github.com/youzan/vant/pull/3140) ([akebe](https://github.com/akebe))
- \[improvement\] Panel: add less vars [\#3136](https://github.com/youzan/vant/pull/3136) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Rate: add less vars [\#3135](https://github.com/youzan/vant/pull/3135) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] Rate: add allow-half prop [\#3134](https://github.com/youzan/vant/pull/3134) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] ImagePreview: add max-zoom、min-zoom prop [\#3133](https://github.com/youzan/vant/pull/3133) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Badge: add less vars [\#3131](https://github.com/youzan/vant/pull/3131) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Slider: add less vars [\#3125](https://github.com/youzan/vant/pull/3125) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Tabbar: add less vars [\#3124](https://github.com/youzan/vant/pull/3124) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Dialog: add less vars [\#3123](https://github.com/youzan/vant/pull/3123) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Cell: add less vars [\#3122](https://github.com/youzan/vant/pull/3122) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] Toast: render error when use getContainer [\#3115](https://github.com/youzan/vant/pull/3115) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] Dialog: can't show after being removed from document [\#3111](https://github.com/youzan/vant/pull/3111) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] Dialog: add confirmButtonText、cancelButtonText prop [\#3107](https://github.com/youzan/vant/pull/3107) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] optimize utils [\#3104](https://github.com/youzan/vant/pull/3104) ([chenjiahan](https://github.com/chenjiahan))
- Update deep-assign.ts [\#3103](https://github.com/youzan/vant/pull/3103) ([xiejun-net](https://github.com/xiejun-net))

## [v1.6.13](https://github.com/youzan/vant/tree/v1.6.13) (2019-04-06)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.12...v1.6.13)

**Bug Fixes**

- \<swipe-cell /\> 处于 open 状态时，页面无法上下滑动 [\#3047](https://github.com/youzan/vant/issues/3047)

**Issue**

- fiex建议 [\#3092](https://github.com/youzan/vant/issues/3092)
- Vue 里面 Card thumb-url 能否改为支持 router-link 的 to 属性 [\#3090](https://github.com/youzan/vant/issues/3090)
- TypeError: Cannot read property 'push' of undefined [\#3089](https://github.com/youzan/vant/issues/3089)
- iphone微信浏览器内弹出系统键盘后，页面上推后弹出的Dialog.alert无法关闭！ [\#3088](https://github.com/youzan/vant/issues/3088)
- Circle 组件在rem方案下，样式在ip4,ip5,android机型上会错乱 [\#3086](https://github.com/youzan/vant/issues/3086)
- area组件 小程序 columns-num设置2和1都不起效，显示内容为空 [\#3085](https://github.com/youzan/vant/issues/3085)
- van-datetime-picker日期时间组件选中后的时间怎么格式化 [\#3084](https://github.com/youzan/vant/issues/3084)
- 自定义Sku 商品规格弹层时，各个粉组件的props都是一样的，数据不对应 [\#3083](https://github.com/youzan/vant/issues/3083)
- progress 进度条的进度可以做成拖动改变进度的吗？ [\#3080](https://github.com/youzan/vant/issues/3080)
- 输入框尾部图标无法自定义（无法显示） [\#3075](https://github.com/youzan/vant/issues/3075)
- vant 更新之后样式问题  [\#3074](https://github.com/youzan/vant/issues/3074)
- \[Feature Request\]DatetimePicker 时间选择器添加年\(year\)类型 [\#3072](https://github.com/youzan/vant/issues/3072)
- Dialog can't exist both Advanced Usage and Function Usage [\#3071](https://github.com/youzan/vant/issues/3071)
- popup与picker结合单列数据平铺 [\#3069](https://github.com/youzan/vant/issues/3069)
- pc微信van-swipe-cell 的滑动和点击事件冲突 [\#3067](https://github.com/youzan/vant/issues/3067)
- Tabbar 标签属性fixed="false"无效 [\#3066](https://github.com/youzan/vant/issues/3066)
- Tabbar 标签栏路由跳转到新页面中再次使用Tabbar无效果 [\#3065](https://github.com/youzan/vant/issues/3065)
- dialog中加field在ios下会存在位移问题 [\#3063](https://github.com/youzan/vant/issues/3063)
- 移动端浏览器自带的右划返回上一页和swipe组件的滑动事件冲突 [\#3062](https://github.com/youzan/vant/issues/3062)
-  iOS 系统点击反馈问题 [\#3060](https://github.com/youzan/vant/issues/3060)
- 数字键盘 [\#3059](https://github.com/youzan/vant/issues/3059)
- Stepper 步进器，小数输入问题 [\#3035](https://github.com/youzan/vant/issues/3035)
- Tab控件line的位置和宽度出错 [\#3034](https://github.com/youzan/vant/issues/3034)
- 部分机型出现组件无法操作问题的解决方案 [\#3015](https://github.com/youzan/vant/issues/3015)

**Improvements**

- \[Doc\] Stepper: optimize async demo [\#3098](https://github.com/youzan/vant/pull/3098) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] Tab: should set line position when activated [\#3097](https://github.com/youzan/vant/pull/3097) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] Uploader: add name prop [\#3096](https://github.com/youzan/vant/pull/3096) ([chenjiahan](https://github.com/chenjiahan))
- update dependencies [\#3095](https://github.com/youzan/vant/pull/3095) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] CellGroup: add less vars [\#3094](https://github.com/youzan/vant/pull/3094) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Button: add less vars [\#3093](https://github.com/youzan/vant/pull/3093) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Tag: add less vars [\#3087](https://github.com/youzan/vant/pull/3087) ([chenjiahan](https://github.com/chenjiahan))
- \[Docs\]Fix errors in the documentation [\#3082](https://github.com/youzan/vant/pull/3082) ([Saberization](https://github.com/Saberization))
- \[new feature\] Slider: add vertical prop [\#3078](https://github.com/youzan/vant/pull/3078) ([chenjiahan](https://github.com/chenjiahan))
- \[Doc\] update mobile index page [\#3077](https://github.com/youzan/vant/pull/3077) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] Stepper: add input-width prop [\#3076](https://github.com/youzan/vant/pull/3076) ([chenjiahan](https://github.com/chenjiahan))
- \[Doc\] update dialog document [\#3068](https://github.com/youzan/vant/pull/3068) ([chenjiahan](https://github.com/chenjiahan))

## [v1.6.12](https://github.com/youzan/vant/tree/v1.6.12) (2019-03-28)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.11...v1.6.12)

**Breaking changes**

- shall we support `error-message-align` prop for Field component [\#3013](https://github.com/youzan/vant/issues/3013)
- Actionsheet 上拉菜单设置cancel-text，自定义内容不渲染 [\#3011](https://github.com/youzan/vant/issues/3011)

**Bug Fixes**

- 两次调用$dialog.alert生成的van-overlay节点位置不一致 [\#3033](https://github.com/youzan/vant/issues/3033)

**Issue**

- Uploader选择照片时，不小心设置了默认从文件管理选择，如何更改 [\#3061](https://github.com/youzan/vant/issues/3061)
- 丰富Card 卡片的自定义项 [\#3055](https://github.com/youzan/vant/issues/3055)
- DatetimePicker 控件滑动卡住 [\#3054](https://github.com/youzan/vant/issues/3054)
- vant ui的日历为什么不是从底部弹出 [\#3053](https://github.com/youzan/vant/issues/3053)
- Checkbox不能使用动态v-model [\#3051](https://github.com/youzan/vant/issues/3051)
- vant-list组件load事件无限触发 [\#3050](https://github.com/youzan/vant/issues/3050)
- DatetimePicker组件设置最大或最小时间后，浏览器会卡死 [\#3046](https://github.com/youzan/vant/issues/3046)
- van-uploader组件设置只拍照不选择本地图片无效问题 [\#3043](https://github.com/youzan/vant/issues/3043)

**Improvements**

- \[bugfix\] Picker: render error when set data async [\#3064](https://github.com/youzan/vant/pull/3064) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] Button: 修改方形细边框按钮边框半径 [\#3058](https://github.com/youzan/vant/pull/3058) ([Mikasa33](https://github.com/Mikasa33))
- \[Docs\]Fix the error in the documentation [\#3057](https://github.com/youzan/vant/pull/3057) ([Saberization](https://github.com/Saberization))
- \[bugfix\] Swipe: unable to scroll page when opened [\#3056](https://github.com/youzan/vant/pull/3056) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Actionsheet: add less vars [\#3049](https://github.com/youzan/vant/pull/3049) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] SwipeCell: fix typo and adjust `swipeLeaveTransition` method's parameter values [\#3048](https://github.com/youzan/vant/pull/3048) ([ystarlongzi](https://github.com/ystarlongzi))

## [v1.6.11](https://github.com/youzan/vant/tree/v1.6.11) (2019-03-22)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.10...v1.6.11)

**Issue**

- vant-list遍历数据 第一个子节点样式不生效 [\#3038](https://github.com/youzan/vant/issues/3038)
- Toast轻提示，设置setDefaultOptions不生效 [\#3036](https://github.com/youzan/vant/issues/3036)
- Icon组件应该需要增加一个苹果图标一个安卓图标吧 [\#3032](https://github.com/youzan/vant/issues/3032)
- 使用rem适配,在SwipeCell中存在问题 [\#3020](https://github.com/youzan/vant/issues/3020)
- IOS 上 dialog 高级用法 样式会被虚拟键盘 顶乱 [\#3019](https://github.com/youzan/vant/issues/3019)
- van-area  确认点击不了 [\#3018](https://github.com/youzan/vant/issues/3018)
- van-area 选择城市后点击确认 光标在上边 [\#3017](https://github.com/youzan/vant/issues/3017)
- switch组件样式错误 [\#3014](https://github.com/youzan/vant/issues/3014)
- 华为手机点击tab等无响应 [\#3010](https://github.com/youzan/vant/issues/3010)

**Improvements**

- \[new feature\] Sku: set selectedNum when inited [\#3045](https://github.com/youzan/vant/pull/3045) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Icon: update shop icons [\#3044](https://github.com/youzan/vant/pull/3044) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] Dialog: overlay incorrect locate when use getContainer [\#3041](https://github.com/youzan/vant/pull/3041) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] Dialog: add getContainer option [\#3040](https://github.com/youzan/vant/pull/3040) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] Button: add touchstart event [\#3039](https://github.com/youzan/vant/pull/3039) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] Search: should inherit class & style [\#3037](https://github.com/youzan/vant/pull/3037) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] Button: add hairline prop [\#3031](https://github.com/youzan/vant/pull/3031) ([chenjiahan](https://github.com/chenjiahan))
- \[bug fix\] swipe: 修改swipe字体模糊问题 [\#3030](https://github.com/youzan/vant/pull/3030) ([Zeks1990](https://github.com/Zeks1990))
- \[improvement\] optimize jest fn called judgement [\#3029](https://github.com/youzan/vant/pull/3029) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Search: tsx [\#3028](https://github.com/youzan/vant/pull/3028) ([chenjiahan](https://github.com/chenjiahan))
- \[Doc\] add design resource [\#3027](https://github.com/youzan/vant/pull/3027) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] optimize active color [\#3026](https://github.com/youzan/vant/pull/3026) ([chenjiahan](https://github.com/chenjiahan))
- \[bugfix\] Toast: should not render overlay when cleared [\#3025](https://github.com/youzan/vant/pull/3025) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] test utils ts [\#3024](https://github.com/youzan/vant/pull/3024) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] i18n config ts [\#3023](https://github.com/youzan/vant/pull/3023) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] popup mixin ts [\#3022](https://github.com/youzan/vant/pull/3022) ([chenjiahan](https://github.com/chenjiahan))
- \[improvement\] Actionsheet: support use slot and cancel-text at same time [\#3021](https://github.com/youzan/vant/pull/3021) ([chenjiahan](https://github.com/chenjiahan))
- \[new feature\] Field: add error-message-align prop [\#3016](https://github.com/youzan/vant/pull/3016) ([huangming1994](https://github.com/huangming1994))
- \[improvement\] bump vue@2.6.10 [\#3012](https://github.com/youzan/vant/pull/3012) ([chenjiahan](https://github.com/chenjiahan))

## [v1.6.10](https://github.com/youzan/vant/tree/v1.6.10) (2019-03-17)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.9...v1.6.10)

## [v1.6.9](https://github.com/youzan/vant/tree/v1.6.9) (2019-03-11)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.8...v1.6.9)

## [v1.6.8](https://github.com/youzan/vant/tree/v1.6.8) (2019-03-02)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.7...v1.6.8)

## [v1.6.7](https://github.com/youzan/vant/tree/v1.6.7) (2019-02-26)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.6...v1.6.7)

## [v1.6.6](https://github.com/youzan/vant/tree/v1.6.6) (2019-02-24)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.5...v1.6.6)

## [v1.6.5](https://github.com/youzan/vant/tree/v1.6.5) (2019-02-17)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.4...v1.6.5)

## [v1.6.4](https://github.com/youzan/vant/tree/v1.6.4) (2019-02-14)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.3...v1.6.4)

## [v1.6.3](https://github.com/youzan/vant/tree/v1.6.3) (2019-02-14)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.2...v1.6.3)

## [v1.6.2](https://github.com/youzan/vant/tree/v1.6.2) (2019-02-10)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.1...v1.6.2)

## [v1.6.1](https://github.com/youzan/vant/tree/v1.6.1) (2019-02-05)
[Full Changelog](https://github.com/youzan/vant/compare/v1.6.0...v1.6.1)

## [v1.6.0](https://github.com/youzan/vant/tree/v1.6.0) (2019-02-04)
[Full Changelog](https://github.com/youzan/vant/compare/v1.5.7...v1.6.0)

## [v1.5.7](https://github.com/youzan/vant/tree/v1.5.7) (2019-01-24)
[Full Changelog](https://github.com/youzan/vant/compare/v1.5.6...v1.5.7)

## [v1.5.6](https://github.com/youzan/vant/tree/v1.5.6) (2019-01-22)
[Full Changelog](https://github.com/youzan/vant/compare/v1.5.5...v1.5.6)

## [v1.5.5](https://github.com/youzan/vant/tree/v1.5.5) (2019-01-21)
[Full Changelog](https://github.com/youzan/vant/compare/v1.5.4...v1.5.5)

## [v1.5.4](https://github.com/youzan/vant/tree/v1.5.4) (2019-01-20)
[Full Changelog](https://github.com/youzan/vant/compare/v1.5.3...v1.5.4)

## [v1.5.3](https://github.com/youzan/vant/tree/v1.5.3) (2019-01-20)
[Full Changelog](https://github.com/youzan/vant/compare/v1.5.2...v1.5.3)

## [v1.5.2](https://github.com/youzan/vant/tree/v1.5.2) (2019-01-10)
[Full Changelog](https://github.com/youzan/vant/compare/v1.5.2-beta...v1.5.2)

## [v1.5.2-beta](https://github.com/youzan/vant/tree/v1.5.2-beta) (2019-01-09)
[Full Changelog](https://github.com/youzan/vant/compare/v1.5.1...v1.5.2-beta)

## [v1.5.1](https://github.com/youzan/vant/tree/v1.5.1) (2019-01-01)
[Full Changelog](https://github.com/youzan/vant/compare/v1.5.0...v1.5.1)

## [v1.5.0](https://github.com/youzan/vant/tree/v1.5.0) (2018-12-22)
[Full Changelog](https://github.com/youzan/vant/compare/v1.5.0-beta2...v1.5.0)

## [v1.5.0-beta2](https://github.com/youzan/vant/tree/v1.5.0-beta2) (2018-12-18)
[Full Changelog](https://github.com/youzan/vant/compare/v1.5.0-beta...v1.5.0-beta2)

## [v1.5.0-beta](https://github.com/youzan/vant/tree/v1.5.0-beta) (2018-12-17)
[Full Changelog](https://github.com/youzan/vant/compare/v1.4.8...v1.5.0-beta)

## [v1.4.8](https://github.com/youzan/vant/tree/v1.4.8) (2018-12-04)
[Full Changelog](https://github.com/youzan/vant/compare/v1.4.7...v1.4.8)

## [v1.4.7](https://github.com/youzan/vant/tree/v1.4.7) (2018-11-27)
[Full Changelog](https://github.com/youzan/vant/compare/v1.4.5...v1.4.7)

## [v1.4.5](https://github.com/youzan/vant/tree/v1.4.5) (2018-11-23)
[Full Changelog](https://github.com/youzan/vant/compare/v1.4.4...v1.4.5)

## [v1.4.4](https://github.com/youzan/vant/tree/v1.4.4) (2018-11-18)
[Full Changelog](https://github.com/youzan/vant/compare/v1.4.3...v1.4.4)

## [v1.4.3](https://github.com/youzan/vant/tree/v1.4.3) (2018-11-10)
[Full Changelog](https://github.com/youzan/vant/compare/v1.4.2...v1.4.3)

## [v1.4.2](https://github.com/youzan/vant/tree/v1.4.2) (2018-11-05)
[Full Changelog](https://github.com/youzan/vant/compare/v1.4.1...v1.4.2)

## [v1.4.1](https://github.com/youzan/vant/tree/v1.4.1) (2018-11-04)
[Full Changelog](https://github.com/youzan/vant/compare/v1.4.0...v1.4.1)

## [v1.4.0](https://github.com/youzan/vant/tree/v1.4.0) (2018-11-04)
[Full Changelog](https://github.com/youzan/vant/compare/v1.3.10...v1.4.0)

## [v1.3.10](https://github.com/youzan/vant/tree/v1.3.10) (2018-10-29)
[Full Changelog](https://github.com/youzan/vant/compare/v1.3.9...v1.3.10)

## [v1.3.9](https://github.com/youzan/vant/tree/v1.3.9) (2018-10-24)
[Full Changelog](https://github.com/youzan/vant/compare/v1.3.8...v1.3.9)

## [v1.3.8](https://github.com/youzan/vant/tree/v1.3.8) (2018-10-17)
[Full Changelog](https://github.com/youzan/vant/compare/v1.3.7...v1.3.8)

## [v1.3.7](https://github.com/youzan/vant/tree/v1.3.7) (2018-10-12)
[Full Changelog](https://github.com/youzan/vant/compare/v1.3.6...v1.3.7)

## [v1.3.6](https://github.com/youzan/vant/tree/v1.3.6) (2018-10-11)
[Full Changelog](https://github.com/youzan/vant/compare/v1.3.5...v1.3.6)

## [v1.3.5](https://github.com/youzan/vant/tree/v1.3.5) (2018-10-10)
[Full Changelog](https://github.com/youzan/vant/compare/v1.3.4...v1.3.5)

## [v1.3.4](https://github.com/youzan/vant/tree/v1.3.4) (2018-10-02)
[Full Changelog](https://github.com/youzan/vant/compare/v1.3.3...v1.3.4)

## [v1.3.3](https://github.com/youzan/vant/tree/v1.3.3) (2018-09-23)
[Full Changelog](https://github.com/youzan/vant/compare/v1.3.2...v1.3.3)

## [v1.3.2](https://github.com/youzan/vant/tree/v1.3.2) (2018-09-14)
[Full Changelog](https://github.com/youzan/vant/compare/v1.3.1...v1.3.2)

## [v1.3.1](https://github.com/youzan/vant/tree/v1.3.1) (2018-09-07)
[Full Changelog](https://github.com/youzan/vant/compare/v1.3.0...v1.3.1)

## [v1.3.0](https://github.com/youzan/vant/tree/v1.3.0) (2018-08-31)
[Full Changelog](https://github.com/youzan/vant/compare/v1.2.1...v1.3.0)

## [v1.2.1](https://github.com/youzan/vant/tree/v1.2.1) (2018-08-24)
[Full Changelog](https://github.com/youzan/vant/compare/v1.2.0...v1.2.1)

## [v1.2.0](https://github.com/youzan/vant/tree/v1.2.0) (2018-08-20)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.16...v1.2.0)

## [v1.1.16](https://github.com/youzan/vant/tree/v1.1.16) (2018-08-10)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.15...v1.1.16)

## [v1.1.15](https://github.com/youzan/vant/tree/v1.1.15) (2018-08-03)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.14...v1.1.15)

## [v1.1.14](https://github.com/youzan/vant/tree/v1.1.14) (2018-07-19)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.13...v1.1.14)

## [v1.1.13](https://github.com/youzan/vant/tree/v1.1.13) (2018-07-13)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.12...v1.1.13)

## [v1.1.12](https://github.com/youzan/vant/tree/v1.1.12) (2018-07-06)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.11...v1.1.12)

## [v1.1.11](https://github.com/youzan/vant/tree/v1.1.11) (2018-07-04)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.10...v1.1.11)

## [v1.1.10](https://github.com/youzan/vant/tree/v1.1.10) (2018-06-28)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.9...v1.1.10)

## [v1.1.9](https://github.com/youzan/vant/tree/v1.1.9) (2018-06-22)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.8...v1.1.9)

## [v1.1.8](https://github.com/youzan/vant/tree/v1.1.8) (2018-06-14)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.7...v1.1.8)

## [v1.1.7](https://github.com/youzan/vant/tree/v1.1.7) (2018-06-06)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.6...v1.1.7)

## [v1.1.6](https://github.com/youzan/vant/tree/v1.1.6) (2018-06-01)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.5...v1.1.6)

## [v1.1.5](https://github.com/youzan/vant/tree/v1.1.5) (2018-05-24)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.4...v1.1.5)

## [v1.1.4](https://github.com/youzan/vant/tree/v1.1.4) (2018-05-18)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.3...v1.1.4)

## [v1.1.3](https://github.com/youzan/vant/tree/v1.1.3) (2018-05-12)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.2...v1.1.3)

## [v1.1.2](https://github.com/youzan/vant/tree/v1.1.2) (2018-05-08)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.1...v1.1.2)

## [v1.1.1](https://github.com/youzan/vant/tree/v1.1.1) (2018-05-04)
[Full Changelog](https://github.com/youzan/vant/compare/v1.1.0...v1.1.1)

## [v1.1.0](https://github.com/youzan/vant/tree/v1.1.0) (2018-04-25)
[Full Changelog](https://github.com/youzan/vant/compare/v1.0.8...v1.1.0)

## [v1.0.8](https://github.com/youzan/vant/tree/v1.0.8) (2018-04-20)
[Full Changelog](https://github.com/youzan/vant/compare/v1.0.7...v1.0.8)

## [v1.0.7](https://github.com/youzan/vant/tree/v1.0.7) (2018-04-17)
[Full Changelog](https://github.com/youzan/vant/compare/v1.0.6...v1.0.7)

## [v1.0.6](https://github.com/youzan/vant/tree/v1.0.6) (2018-04-17)
[Full Changelog](https://github.com/youzan/vant/compare/v1.0.5...v1.0.6)

## [v1.0.5](https://github.com/youzan/vant/tree/v1.0.5) (2018-04-13)
[Full Changelog](https://github.com/youzan/vant/compare/v1.0.4...v1.0.5)

## [v1.0.4](https://github.com/youzan/vant/tree/v1.0.4) (2018-04-10)
[Full Changelog](https://github.com/youzan/vant/compare/v1.0.3...v1.0.4)

## [v1.0.3](https://github.com/youzan/vant/tree/v1.0.3) (2018-03-26)
[Full Changelog](https://github.com/youzan/vant/compare/v1.0.2...v1.0.3)

## [v1.0.2](https://github.com/youzan/vant/tree/v1.0.2) (2018-03-22)
[Full Changelog](https://github.com/youzan/vant/compare/v1.0.1...v1.0.2)

## [v1.0.1](https://github.com/youzan/vant/tree/v1.0.1) (2018-03-19)
[Full Changelog](https://github.com/youzan/vant/compare/v1.0.0...v1.0.1)



\* *This Change Log was automatically generated by [github_changelog_generator](https://github.com/skywinder/Github-Changelog-Generator)*