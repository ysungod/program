module.exports = { 
  //设置打包黑名单，即不打包这些文件
  externals: {
    'vue': 'Vue',
    'vue-router': 'VueRouter',
    'element-ui': 'ELEMENT',
  }
}